﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Infrastructure.Dataes;
using NUnit.Framework;
using System.Collections.Generic;

namespace ConsoleRestaurant.Tests.Infrastructure
{
    public class AllergyTest
    {
        [Test]
        public void FindCustomerAllergiesInDish_CustomerBuyDishAndDishHaveAllergyIngredient_ListAllergy()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateBaseFakeDatabase(manager, true);

            var allergyPotatoes = new Ingredient
            {
                Name = "Potatoes"
            };

            Customer customerBernard = new Customer
            {
                FullName = "Bernard Unfortunate",
                Budget = 30,
                Allergies = new List<Ingredient>() { allergyPotatoes }
            };

            var dish = GetTestDish();
            var allergy = new Allergy();

            //when
            var listAllargy = allergy.FindCustomerAllergiesInDish(customerBernard, dish);

            //then
            Assert.Greater(listAllargy.Length, 0);
        }

        [Test]
        public void FindCustomerAllergiesInDish_CustomerBuyDishAndDishHaveAllergyIngredient_EmptyListAllergy()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateBaseFakeDatabase(manager, true);

            var allergyPotatoes = new Ingredient
            {
                Name = "Tomatoes"
            };

            Customer customerBernard = new Customer
            {
                FullName = "Bernard Unfortunate",
                Budget = 30,
                Allergies = new List<Ingredient>() { allergyPotatoes }
            };

            var dish = GetTestDish();

            var allergy = new Allergy();

            //when
            var listAllargy = allergy.FindCustomerAllergiesInDish(customerBernard, dish);

            //then
            Assert.AreEqual(0, listAllargy.Length);
        }


        [Test]
        public void FindAllergy_CheckDishOnAllergyIngredient_AllergyPositiveResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateBaseFakeDatabase(manager, true);

            var allergyIngredient = new Ingredient
            {
                Name = "Potatoes"
            };

            var dish = GetTestDish();

            var allergy = new Allergy();

            //when
            var dishHasAllargy = allergy.FindAllergy(dish, allergyIngredient);

            //then
            Assert.IsTrue(dishHasAllargy);
        }

        [Test]
        public void FindAllergy_CheckDishOnAllergyIngredient_AllergyNegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateBaseFakeDatabase(manager, true);

            var allergyIngredient = new Ingredient
            {
                Name = "Tomatoes"
            };

            var dish = GetTestDish();

            var allergy = new Allergy();

            //when
            var dishHasAllargy = allergy.FindAllergy(dish, allergyIngredient);

            //then
            Assert.IsFalse(dishHasAllargy);
        }

        [Test]
        public void GetReportAllergy_AllergyStateLimitAsKeep_PositiveResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true);

            var allergy = new Allergy(manager);

            var report = new BuyReport();

            report.PriceOfDish = 32.5;             //25 - random number       //25 + 7.5 (30% ProfitMargin)
            report.TransactionTaxAmount = 6.5;                                //39 * 0.2 = 7.8 (20% TransactionTax)
            report.Customer = manager.GetCustomer("Bernard Unfortunate");
            report.Dish = manager.GetDish("Fries");

            List<string> allergies = new List<string>() { "Potatoes" };

            //var expectedBudget = 21.88;
            //var expectedAllergyState = AllergyState.KEEP;
            var expectedTransactionTaxAmount = 0;
            var expectedPriceOfDish = 8.12;                                   //32.5 * 0.25 = 8.12

            //when
            var reportAllergy = allergy.GetReportAllergy(report, allergies.ToArray());

            //then
            Assert.IsTrue(reportAllergy.CommandSucceeded);
            Assert.IsFalse(reportAllergy.IsBankruptcy);
            Assert.IsTrue(report.Message.EndsWith($"[{expectedPriceOfDish} (Tax: {expectedTransactionTaxAmount})] => fail, can't buy, allergic to: Potatoes"));
        }

        private static Dish GetTestDish()
        {
            var iRabbit = new Ingredient
            {
                Name = "Rabbit",
                Price = 30
            };

            var iOil = new Ingredient
            {
                Name = "Oil",
                Price = 10
            };

            var iPotatoes = new Ingredient
            {
                Name = "Potatoes",
                Price = 5
            };

            var dFatRabbit = new Dish
            {
                Name = "Fat rabbit",
                Ingredients = new List<Ingredient>() { iRabbit, iOil }
            };

            var dComplexLunch = new Dish
            {
                Name = "Complex lunch",
                Dishes = new List<Dish>() { dFatRabbit },
                Ingredients = new List<Ingredient>() { iPotatoes }
            };

            return dComplexLunch;
        }

        private void GenerateBaseFakeDatabase(ManagetRepositoryMock manager, bool isCommandEnabled)
        {
            //======================================================== config
            CommandConfig commandConfig = new CommandConfig()
            {
                Audit = true,
                Budget = true,
                Buy = isCommandEnabled,
                Order = true,
                Table = true
            };

            TaxConfig taxConfig = new TaxConfig()
            {
                DailyTax = 20,
                ProfitMargin = 30,
                TransactionTax = 20
            };

            var warehouseConfig = new WarehouseConfig()
            {
                MaxDishType = 4,
                MaxIngredientType = 8,
                TotalMaximum = 15
            };

            CouponConfig couponConfig = new CouponConfig()
            {
                EveryThirdDiscount = 10
            };

            AllergyConfig allergyConfig = new AllergyConfig()
            {
                DishesWithAllergies = "30"
            };

            TimeCostConfig timeCostConfig = new TimeCostConfig()
            {
                BudgetCost = 250
            };

            var appsetting = new Appsetting()
            {
                Allergy = allergyConfig,
                Command = commandConfig,
                Coupon = couponConfig,
                Tax = taxConfig,
                TimeCost = timeCostConfig,
                Warehouse = warehouseConfig,
                MainRestaurantName = "Gurman"
            };

            AppConfig.Settings = appsetting;
        }

        private void GenerateFakeDatabase(ManagetRepositoryMock manager, bool isCommandEnabled)
        {
            //======================================================== config
            GenerateBaseFakeDatabase(manager, isCommandEnabled);

            //======================================================== restaurant
            Restaurant restaurant = new Restaurant
            {
                Name = AppConfig.Settings.MainRestaurantName,
                Budget = 30
            };

            manager.AddRestaurant(restaurant);

            //======================================================== ingredient
            var ingredientPotatoes = new Ingredient
            {
                Name = "Potatoes"
            };

            manager.AddIngredient(ingredientPotatoes);

            //======================================================== dish
            Dish dishFries = new Dish
            {
                Name = "Fries",
                Ingredients = new List<Ingredient>() { ingredientPotatoes }
            };

            manager.AddDish(dishFries);

            //======================================================== customer
            Customer customerBernard = new Customer
            {
                FullName = "Bernard Unfortunate",
                Budget = 30,
                Allergies = new List<Ingredient>() { ingredientPotatoes }
            };

            manager.AddCustomer(customerBernard);
        }
    }
}
