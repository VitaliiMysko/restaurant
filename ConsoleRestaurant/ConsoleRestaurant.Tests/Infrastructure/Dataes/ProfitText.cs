﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Infrastructure.Dataes;
using NUnit.Framework;

namespace ConsoleRestaurant.Tests.Infrastructure
{
    public class ProfitText
    {

        [Test]
        public void FixedInitialBudget_GetInitialBudget_PositiveResult()
        {
            //given
            var manager = new ManagetRepositoryMock();

            Restaurant restaurant = new Restaurant
            {
                Name = "Gurman",
                Budget = 30
            };

            manager.AddRestaurant(restaurant);

            var expectedInitialBudget = 30;

            var profit = new Profit(restaurant.Name);

            //when
            profit.FixedInitialBudget(manager);

            //then
            Assert.AreEqual(expectedInitialBudget, profit.InitialBudget);
        }

        [Test]
        public void FixedEndBudgetWithoutTip_GetEndBudgetAndBudgetHasTip_PositiveResult()
        {
            //given
            var manager = new ManagetRepositoryMock();

            AppConfig.Settings = new Appsetting()
            {
                Tax = new TaxConfig
                {
                    TipsTax = 5
                }
            };

            Restaurant restaurant = new Restaurant
            {
                Name = "Gurman",
                Budget = 69,
                Tip = 20
            };

            manager.AddRestaurant(restaurant);

            var expectedEndBudget = 50;

            var profit = new Profit(restaurant.Name);

            //when
            profit.FixedEndBudgetWithoutTip(manager);

            //then
            Assert.AreEqual(expectedEndBudget, profit.EndBudget);
        }

        [Test]
        public void GetProfit_ProfitOfRestaurant_PositiveResult()
        {
            //given
            var manager = new ManagetRepositoryMock();

            Restaurant restaurant = new Restaurant
            {
                Name = "Gurman",
                Budget = 30
            };

            manager.AddRestaurant(restaurant);

            var profit = new Profit(restaurant.Name);

            profit.FixedInitialBudget(manager);
            restaurant.Budget += 20;
            manager.UpdateRestaurant(restaurant);
            profit.FixedEndBudgetWithoutTip(manager);

            var expectedprofitOfRestaurant = 20;

            //when
            var profitOfRestaurant = profit.GetProfit();
            //then
            Assert.AreEqual(expectedprofitOfRestaurant, profitOfRestaurant);
        }

    }
}
