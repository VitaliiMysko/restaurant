﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Infrastructure.Dataes;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace ConsoleRestaurant.Tests.Infrastructure
{
    public class WarehouseTest
    {
        [TestCase("Potatoes", 5)]
        [TestCase("Lemon", 0)]
        public void GetQuantity_SomeIngredient_QuantityOfIngredientOnWarehouse(string nameIngredient, int expectedQuantity)
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager);

            var ingredient = manager.GetIngredient(nameIngredient);

            var command = new Warehouse(manager);

            //when
            var quantity = command.GetQuantity(ingredient);

            //then
            Assert.AreEqual(expectedQuantity, quantity);
        }

        [TestCase("Fries", 3)]
        [TestCase("Smashed Potatoes", 0)]
        public void GetQuantity_SomeDish_QuantityOfDishOnWarehouse(string nameDish, int expectedQuantity)
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager);

            var dish = manager.GetDish(nameDish);

            var command = new Warehouse(manager);

            //when
            var quantity = command.GetQuantity(dish);

            //then
            Assert.AreEqual(expectedQuantity, quantity);
        }

        [TestCase("Potatoes", 5, true)]
        [TestCase("Potatoes", 10, false)]
        [TestCase("Lemon", 1, false)]
        public void IsEnough_SomeIngredient_Bool(string nameIngredient, int needQuantity, bool expectedResult)
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager);

            var ingredient = manager.GetIngredient(nameIngredient);

            var command = new Warehouse(manager);

            //when
            var result = command.IsEnough(ingredient, needQuantity);

            //then
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase("Fries", 3, true)]
        [TestCase("Fries", 5, false)]
        [TestCase("Smashed Potatoes", 1, false)]
        public void IsEnough_SomeDish_Bool(string nameDish, int needQuantity, bool expectedResult)
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager);

            var dish = manager.GetDish(nameDish);

            var command = new Warehouse(manager);

            //when
            var result = command.IsEnough(dish, needQuantity);

            //then
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase("Potatoes", 2, 7, 0)]
        [TestCase("Potatoes", 5, 7, 3)]
        [TestCase("Lemon", 10, 2, 8)]
        public void Put_SomeIngredient_QuantityOfIngrediendGetMore(string nameIngredient, int quantity, int expectedQuantity, int expectedWasteQuantity)
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager);

            var ingredient = manager.GetIngredient(nameIngredient);

            var command = new Warehouse(manager);

            //when
            command.Put(ingredient, quantity);

            //then
            var warehouseIngredient = manager.GetWarehouseIngredient(nameIngredient);
            
            var wastedIngredient = manager.GetWasteIngredient(nameIngredient);
            var wastedQuantity = wastedIngredient != null ? wastedIngredient.Quantity : 0;

            Assert.AreEqual(expectedQuantity, warehouseIngredient.Quantity);
            Assert.AreEqual(expectedWasteQuantity, wastedQuantity);
        }

        [TestCase("Fries", 1, 4, 0)]
        [TestCase("Fries", 3, 4, 2)]
        [TestCase("Smashed Potatoes", 12, 2, 10)]
        public void Put_SomeDish_QuantityOfDishGetMore(string nameDish, int quantity, int expectedQuantity, int expectedWasteQuantity)
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager);

            var dish = manager.GetDish(nameDish);

            var command = new Warehouse(manager);

            //when
            command.Put(dish, quantity);

            //then
            var warehouseDish = manager.GetWarehouseDish(nameDish);

            var wastedDish = manager.GetWasteDish(nameDish);
            var wastedQuantity = wastedDish != null ? wastedDish.Quantity : 0;

            Assert.AreEqual(expectedQuantity, warehouseDish.Quantity);
            Assert.AreEqual(expectedWasteQuantity, wastedQuantity);
        }

        [TestCase("Potatoes", 2, 3)]
        public void Take_SomeIngredient_QuantityOfIngrediendGetLess(string nameIngredient, int quantity, int expectedQuantity)
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager);

            var ingredient = manager.GetIngredient(nameIngredient);

            var command = new Warehouse(manager);

            //when
            command.Take(ingredient, quantity);

            //then
            var warehouseIngredient = manager.GetWarehouseIngredient(nameIngredient);

            Assert.AreEqual(expectedQuantity, warehouseIngredient.Quantity);
        }

        [TestCase("Potatoes", 7)]
        [TestCase("Lemon", 2)]
        public void Take_SomeIngredientThatNotEnough_ExceptionThrown(string nameIngredient, int quantity)
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager);

            var ingredient = manager.GetIngredient(nameIngredient);

            var command = new Warehouse(manager);

            //when

            //then
            Assert.Throws<Exception>(() => command.Take(ingredient, quantity));
        }

        [TestCase("Fries", 2)]
        public void Take_SomeDish_QuantityOfDishGetMore(string nameDish, int expectedQuantity)
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager);

            var dish = manager.GetDish(nameDish);

            var command = new Warehouse(manager);

            //when
            command.Take(dish);

            //then
            var warehouseDish = manager.GetWarehouseDish(nameDish);

            Assert.AreEqual(expectedQuantity, warehouseDish.Quantity);
        }

        [TestCase("Omega Sauce")]
        public void Take_SomeDishAndNeedIngredientsNotEnough_ExceptionThrown(string nameDish)
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager);

            var dish = manager.GetDish(nameDish);

            var command = new Warehouse(manager);

            //when

            //then
            Assert.Throws<Exception>(() => command.Take(dish));
        }

        [Test]
        public void Take_SeveralDishes_QuantityOfDishAndIngredientsGetMore()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager);

            var dishIrishFish = manager.GetDish("Irish Fish");
            var dishFries = manager.GetDish("Fries");

            List<Dish> dishes = new List<Dish>() { dishIrishFish, dishFries };

            var expectedWarehouseDishFriesQuantity = 1;
            var expectedWarehousedIngredientPotatoesQuantity = 4;
            var expectedWarehousedIngredientTunaQuantity = 4;

            var command = new Warehouse(manager);

            //when
            command.Take(dishes);

            //then
            var warehouseDishFries = manager.GetWarehouseDish("Fries");
            var warehouseIngredientPotatoes = manager.GetWarehouseIngredient("Potatoes");
            var warehouseIngredientTuna = manager.GetWarehouseIngredient("Tuna");

            Assert.AreEqual(expectedWarehouseDishFriesQuantity, warehouseDishFries.Quantity);
            Assert.AreEqual(expectedWarehousedIngredientPotatoesQuantity, warehouseIngredientPotatoes.Quantity);
            Assert.AreEqual(expectedWarehousedIngredientTunaQuantity, warehouseIngredientTuna.Quantity);
        }

        [Test]
        public void Take_SeveralDishes_ExceptionThrown()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager);

            var dishIrishFish = manager.GetDish("Irish Fish");
            var dishFries = manager.GetDish("Omega Sauce");

            List<Dish> dishes = new List<Dish>() { dishIrishFish, dishFries };

            var command = new Warehouse(manager);

            //when

            //then
            Assert.Throws<Exception>(() => command.Take(dishes));
        }

        [TestCase("Irish Fish", true)]
        [TestCase("Omega Sauce", false)]
        public void CheckWarehouse_SomeDish_Bool(string nameDish, bool expectedResult)
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager);

            var dish = manager.GetDish(nameDish);

            var command = new Warehouse(manager);

            //when
            var result = command.CheckWarehouse(dish);

            //then
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase("Irish Fish", "Fries", true)]
        [TestCase("Omega Sauce", "Fries", false)]
        public void CheckWarehouse_SeveralDishes_Bool(string nameDish1, string nameDish2, bool expectedResult)
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager);

            var dish1 = manager.GetDish(nameDish1);
            var dish2 = manager.GetDish(nameDish2);

            List<Dish> dishes = new List<Dish>() { dish1, dish2 };

            var command = new Warehouse(manager);

            //when
            var result = command.CheckWarehouse(dishes);

            //then
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase(true, true, "Fries: 3, Potatoes: 5, Tuna: 5, Water: 5")]
        [TestCase(true, false, "Fries: 3")]
        [TestCase(false, true, "Potatoes: 5, Tuna: 5, Water: 5")]
        public void GetAudit_AllHarehouse_String(bool ShowDishes, bool ShowIngredients, string expectedResult)
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager);

            var command = new Warehouse(manager);

            //when
            var result = command.GetAudit(ShowDishes, ShowIngredients);

            //then
            Assert.AreEqual(expectedResult, result);
        }

        private static void GenerateFakeDatabase(ManagetRepositoryMock manager)
        {
            //======================================================== config
            var warehouseConfig = new WarehouseConfig()
            {
                MaxDishType = 4,
                MaxIngredientType = 8,
                TotalMaximum = 20
            };

            AppConfig.Settings.Warehouse = warehouseConfig;

            //======================================================== ingredient
            Ingredient ingredientLemon = new Ingredient
            {
                Name = "Lemon",
                Price = 3
            };

            Ingredient ingredientWater = new Ingredient
            {
                Name = "Water",
                Price = 1
            };

            Ingredient ingredientPotatoes = new Ingredient
            {
                Name = "Potatoes",
                Price = 3
            };

            Ingredient ingredientTuna = new Ingredient
            {
                Name = "Tuna",
                Price = 25
            };
            manager.AddIngredient(ingredientLemon);
            manager.AddIngredient(ingredientPotatoes);
            manager.AddIngredient(ingredientTuna);

            //======================================================== dish
            Dish dishSmashedPotatoes = new Dish
            {
                Name = "Smashed Potatoes",
                Ingredients = new List<Ingredient>() { ingredientPotatoes },
            };

            Dish dishFries = new Dish
            {
                Name = "Fries",
                Ingredients = new List<Ingredient>() { ingredientPotatoes }
            };

            Dish dishIrishFish = new Dish
            {
                Name = "Irish Fish",
                Dishes = new List<Dish>() { dishSmashedPotatoes, dishFries },
                Ingredients = new List<Ingredient>() { ingredientTuna }
            };

            Dish dishOmegaSauce = new Dish
            {
                Name = "Omega Sauce",
                Ingredients = new List<Ingredient>() { ingredientLemon, ingredientWater }
            };

            manager.AddDish(dishFries);
            manager.AddDish(dishSmashedPotatoes);
            manager.AddDish(dishIrishFish);
            manager.AddDish(dishOmegaSauce);

            //======================================================== warehouse dish
            WarehouseDish warehouseDishFries = new WarehouseDish
            {
                Dish = dishFries,
                Quantity = 3
            };
            manager.AddWarehouseDish(warehouseDishFries);

            //======================================================== warehouse ingredient
            WarehouseIngredient warehouseIngredientPotatoes = new WarehouseIngredient
            {
                Ingredient = ingredientPotatoes,
                Quantity = 5
            };

            WarehouseIngredient warehouseIngredientTuna = new WarehouseIngredient
            {
                Ingredient = ingredientTuna,
                Quantity = 5
            };

            WarehouseIngredient warehouseIngredientWater = new WarehouseIngredient
            {
                Ingredient = ingredientWater,
                Quantity = 5
            };

            manager.AddWarehouseIngredient(warehouseIngredientPotatoes);
            manager.AddWarehouseIngredient(warehouseIngredientTuna);
            manager.AddWarehouseIngredient(warehouseIngredientWater);
        }

    }
}
