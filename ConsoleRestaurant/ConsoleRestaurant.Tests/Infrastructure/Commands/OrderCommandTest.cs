﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Infrastructure.Commands;
using NUnit.Framework;
using System.Collections.Generic;

namespace ConsoleRestaurant.Tests.Infrastructure
{
    public class OrderCommandTest
    {
        [Test]
        public void Execute_CommandIsNotEnabledToUse_NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateBaseFakeDatabase(manager, false);

            var dataFakeArr = new string[] { "", "1" };
            var command = new OrderCommand(dataFakeArr, manager, 0, 0);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsTrue(report.Message.EndsWith(" disabled"));
        }

        [Test]
        public void Execute_PassedDataHasWrong_NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateBaseFakeDatabase(manager, true);

            var dataFakeArr = new string[] { "" };
            var command = new OrderCommand(dataFakeArr, manager, 0, 0);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsTrue(report.Message.EndsWith("Record is not correct"));
        }

        [Test]
        public void Execute_AmountIsNotCorrect_NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateBaseFakeDatabase(manager, true);

            var dataFakeArr = new string[] { "", "12a" };
            var command = new OrderCommand(dataFakeArr, manager, 0, 0);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsTrue(report.Message.EndsWith("is not correct"));
        }

        [Test]
        public void Execute_PriceOfIngredientNothing__NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateBaseFakeDatabase(manager, true);

            Ingredient ingredient = new Ingredient
            {
                Name = "Potatoes",
                Price = 0
            };

            manager.AddIngredient(ingredient);

            string[] dataFakeArr = { "Potatoes", "5" };
            var command = new OrderCommand(dataFakeArr, manager, 0, 0);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsTrue(report.Message.EndsWith("= zero"));
        }

        [TestCase("Potatoes", 5, 5, 0, 76)]
        [TestCase("Potatoes", 10, 8, 2, 52)]
        [TestCase("Potatoes", 15, 8, 7, 28)]
        public void Execute_CorrectOrderAndOrderStateIsIngredients_PositiveResult(string nameIngredient, int quantity, int expectedQuantity, int expectedWasteQuantity, int expectedBudget)
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateBaseFakeDatabase(manager, true);

            Restaurant restaurant = new Restaurant
            {
                Name = AppConfig.Settings.MainRestaurantName,
                Budget = 100
            };

            Ingredient ingredient = new Ingredient
            {
                Name = nameIngredient,
                Price = 4
            };

            manager.AddRestaurant(restaurant);
            manager.AddIngredient(ingredient);

            string[] dataFakeArr = { nameIngredient, quantity.ToString() };
            var command = new OrderCommand(dataFakeArr, manager, 0, 0);

            //when
            var report = command.Execute();

            //then
            var warehouseIngredientNew = manager.GetWarehouseIngredient(nameIngredient);
            var restaurantNew = manager.GetRestaurant(AppConfig.Settings.MainRestaurantName);

            var wastedIngredient = manager.GetWasteIngredient(nameIngredient);
            var wastedQuantity = wastedIngredient != null ? wastedIngredient.Quantity : 0;


            Assert.IsTrue(report.CommandSucceeded);
            Assert.AreEqual(expectedBudget, restaurantNew.Budget);
            Assert.AreEqual(expectedQuantity, warehouseIngredientNew.Quantity);
            Assert.AreEqual(expectedWasteQuantity, wastedQuantity);
        }

        [TestCase("Fries", "3", 4, 2, 89.2)]
        [TestCase("Irish Fish", "3", 2, 1, -11.6)]
        public void Execute_CorrectOrderAndOrderStateIsDishesAndIsEnoughBudgetOfRestaurant_PositiveResult(string nameDish, string quantity, int expectedQuantity, int expectedWasteQuantity, double expectedBudget)
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true);

            var orderConfig = new OrderConfig()
            {
                Allow = "Dishes"
            };

            AppConfig.Settings.Order = orderConfig;


            string[] dataFakeArr = { nameDish, quantity.ToString() };
            var command = new OrderCommand(dataFakeArr, manager, 0, 0);

            //when
            var report = command.Execute();

            //then
            var warehouseDishNew = manager.GetWarehouseDish(nameDish);
            var restaurantNew = manager.GetRestaurant(AppConfig.Settings.MainRestaurantName);

            var wastedDish = manager.GetWasteDish(nameDish);
            var wastedQuantity = wastedDish != null ? wastedDish.Quantity : 0;


            Assert.IsTrue(report.CommandSucceeded);
            Assert.AreEqual(expectedBudget, restaurantNew.Budget);
            Assert.AreEqual(expectedQuantity, warehouseDishNew.Quantity);
            Assert.AreEqual(expectedWasteQuantity, wastedQuantity);
        }

        private void GenerateBaseFakeDatabase(ManagetRepositoryMock manager, bool isCommandEnabled)
        {
            //======================================================== config
            CommandConfig commandConfig = new CommandConfig()
            {
                Audit = true,
                Budget = true,
                Buy = true,
                Order = isCommandEnabled,
                Table = true
            };

            TaxConfig taxConfig = new TaxConfig()
            {
                DailyTax = 20,
                ProfitMargin = 30,
                TransactionTax = 20
            };

            var warehouseConfig = new WarehouseConfig()
            {
                MaxDishType = 4,
                MaxIngredientType = 8,
                TotalMaximum = 15
            };

            var orderConfig = new OrderConfig()
            {
                Allow = "Ingredients"
            };

            TimeCostConfig timeCostConfig = new TimeCostConfig()
            {
                BudgetCost = 250
            };

            var appsetting = new Appsetting()
            {
                Command = commandConfig,
                Tax = taxConfig,
                TimeCost = timeCostConfig,
                MainRestaurantName = "Gurman",
                Order = orderConfig,
                Warehouse = warehouseConfig
            };

            AppConfig.Settings = appsetting;
        }

        private void GenerateFakeDatabase(ManagetRepositoryMock manager, bool isCommandEnabled, bool isBankrupt = false)
        {
            //======================================================== config
            GenerateBaseFakeDatabase(manager, isCommandEnabled);

            //======================================================== restaurant
            Restaurant restaurant = new Restaurant
            {
                Name = AppConfig.Settings.MainRestaurantName,
                Budget = isBankrupt ? -100 : 100
            };

            manager.AddRestaurant(restaurant);

            //======================================================== ingredient
            Ingredient ingredientPotatoes = new Ingredient
            {
                Name = "Potatoes",
                Price = 3
            };

            Ingredient ingredientTuna = new Ingredient
            {
                Name = "Tuna",
                Price = 25
            };

            manager.AddIngredient(ingredientPotatoes);
            manager.AddIngredient(ingredientTuna);

            //======================================================== dish
            Dish dishSmashedPotatoes = new Dish
            {
                Name = "Smashed Potatoes",
                Ingredients = new List<Ingredient>() { ingredientPotatoes },
            };

            Dish dishFries = new Dish
            {
                Name = "Fries",
                Ingredients = new List<Ingredient>() { ingredientPotatoes }
            };

            Dish dishIrishFish = new Dish
            {
                Name = "Irish Fish",
                Dishes = new List<Dish>() { dishSmashedPotatoes, dishFries },
                Ingredients = new List<Ingredient>() { ingredientTuna }
            };

            manager.AddDish(dishFries);
            manager.AddDish(dishSmashedPotatoes);
            manager.AddDish(dishIrishFish);

            //======================================================== warehouse dish
            WarehouseDish warehouseDishFries = new WarehouseDish
            {
                Dish = dishFries,
                Quantity = 3
            };

            manager.AddWarehouseDish(warehouseDishFries);

            //======================================================== warehouse ingredient
            WarehouseIngredient warehouseIngredientPotatoes = new WarehouseIngredient
            {
                Ingredient = ingredientPotatoes,
                Quantity = 5
            };

            manager.AddWarehouseIngredient(warehouseIngredientPotatoes);


            WarehouseIngredient warehouseIngredientTuna = new WarehouseIngredient
            {
                Ingredient = ingredientTuna,
                Quantity = 5
            };

            manager.AddWarehouseIngredient(warehouseIngredientTuna);

            //======================================================== customer

            //======================================================== activity of buying
        }

    }
}
