﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Infrastructure.Commands;
using NUnit.Framework;
using System.Collections.Generic;

namespace ConsoleRestaurant.Tests.Infrastructure
{
    public class TableCommandTest
    {
        [Test]
        public void Execute_CommandIsNotEnabledToUse_NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateBaseFakeDatabase(manager, false);

            var dataFakeArr = new string[] { "", "", "", "" };
            var command = new TableCommand(dataFakeArr, manager);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsTrue(report.Message.EndsWith(" disabled"));
        }

        [Test]
        public void Execute_PassedDataHasWrong_NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateBaseFakeDatabase(manager, true);

            var dataFakeArr = new string[] { "Adam Smith", "Irish Fish" };
            var command = new TableCommand(dataFakeArr, manager);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsTrue(report.Message.EndsWith("Record is not correct"));
        }

        [Test]
        public void Execute_TheNumbersOfCustomersDoNotEqualTheNumbersOfDishes_NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateBaseFakeDatabase(manager, true);

            string[] dataFakeArr = { "Bernard Unfortunate", "Adam Smith", "Irish Fish" };

            var customer1 = new Customer
            {
                FullName = "Adam Smith"
            };

            var customer2 = new Customer
            {
                FullName = "Bernard Unfortunate"
            };

            manager.AddCustomer(customer1);
            manager.AddCustomer(customer2);

            var dish = new Dish
            {
                Name = "Irish Fish"
            };

            manager.AddDish(dish);

            var command = new TableCommand(dataFakeArr, manager);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsTrue(report.Message.EndsWith("The numbers of customers do not equal the numbers of dishes"));
        }

        [Test]
        public void Execute_OnePersonCanAppearOnlyOnceAtTheTable_NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();

            var customer = new Customer();
            customer.FullName = "Adam Smith";
            manager.AddCustomer(customer);

            GenerateBaseFakeDatabase(manager, true);

            string[] dataFakeArr = { "Adam Smith", "Adam Smith", "Irish Fish", "Omega Sauce" };
            var command = new TableCommand(dataFakeArr, manager);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
        }

        [Test]
        public void Execute_WarehouseHasNoEnoughIngredients_NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, false, true, true, true, false, true);

            //======================================================== customer

            Customer customerJulie = new Customer
            {
                FullName = "Julie Mirage",
                Budget = 200
            };

            manager.AddCustomer(customerJulie);

            var warehouseIngredientTuna = manager.GetWarehouseIngredient("Tuna");
            warehouseIngredientTuna.Quantity = 2;
            manager.AddWarehouseIngredient(warehouseIngredientTuna);

            string[] dataFakeArr = { "Bernard Unfortunate", "Adam Smith", "Julie Mirage", "Irish Fish", "Irish Fish", "Irish Fish" };
            var command = new TableCommand(dataFakeArr, manager);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsTrue(report.Message.EndsWith("Error! Table is fail. Not enough ingredients or dishes on the warehouse"));
        }

        [Test]
        public void Execute_CustomersHaveAllergyAndBudgetOfRestaurantAllreadyIsBankrupt_NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true, true, true, false, true, true, true);

            string[] dataFakeArr = { "Bernard Unfortunate", "Adam Smith", "Irish Fish", "Irish Fish" };
            var command = new TableCommand(dataFakeArr, manager);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsTrue(report.IsBankruptcy);
        }

        [Test]
        public void Execute_CorrectTable_PositiveResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true, true, true, true, false, true);

            var expectedBudget = 122.24;        //3(potatoes)+3(potatoes)+25(tuna) = 31 * (1 + 0.3) = 40.3;  40.3 * (1 - 0.2) =  32.24;  90 + 32.24 = 122.24
            var expectedQuantityWarehouseDishFries = 1;             //3-2=1
            var expectedQuantityWarehouseIngredientPotatoes = 5;    //7-2=3
            var expectedQuantityWarehouseIngredientTuna = 3;        //5-2=3
            var expectedBudgetOfCustomerAdam = 59.7;                //100 - 40.3 = 59.7

            string[] dataFakeArr = { "Bernard Unfortunate", "Adam Smith", "Irish Fish", "Irish Fish" };
            var command = new TableCommand(dataFakeArr, manager);

            //when
            var report = command.Execute();

            //then
            var restaurantNew = manager.GetRestaurant(AppConfig.Settings.MainRestaurantName);
            var warehouseDishFriesNew = manager.GetWarehouseDish("Fries");
            var warehouseIngredientPotatoesNew = manager.GetWarehouseIngredient("Potatoes");
            var warehouseIngredientTunaNew = manager.GetWarehouseIngredient("Tuna");
            var customerAdamNew = manager.GetCustomer("Adam Smith");

            Assert.IsTrue(report.CommandSucceeded);
            Assert.AreEqual(expectedBudget, restaurantNew.Budget);
            Assert.AreEqual(expectedQuantityWarehouseDishFries, warehouseDishFriesNew.Quantity);
            Assert.AreEqual(expectedQuantityWarehouseIngredientPotatoes, warehouseIngredientPotatoesNew.Quantity);
            Assert.AreEqual(expectedQuantityWarehouseIngredientTuna, warehouseIngredientTunaNew.Quantity);
            Assert.AreEqual(expectedBudgetOfCustomerAdam, customerAdamNew.Budget);
        }

        [Test]
        public void Execute_SomeCustomerHasNoEnoughMoney_NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, false, true, false, true, false, true);

            string[] dataFakeArr = { "Bernard Unfortunate", "Adam Smith", "Irish Fish", "Irish Fish" };
            var command = new TableCommand(dataFakeArr, manager);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsNotNull(report.ExtraReports.Find((item) => item.Message.EndsWith(" => fail, not enough money")));
        }

        [Test]
        public void Execute_AllCustomersHasEnoughMoneyAndEveryOneHasNoAllergyAndPooledTrueWeRunItAsSimpleTable_PositiveResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, false, true, true, true, false, true);

            //======================================================== customer

            //Ok, customer has enough money
            Customer customerJulie = new Customer
            {
                FullName = "Julie Mirage",
                Budget = 200
            };

            manager.AddCustomer(customerJulie);

            //has not enough money
            Customer customerChristian = new Customer
            {
                FullName = "Christian Donnovan",
                Budget = 75
            };

            manager.AddCustomer(customerChristian);

            //has not enough money
            Customer customerElon = new Customer
            {
                FullName = "Elon Carousel",
                Budget = 50
            };

            manager.AddCustomer(customerElon);

            string[] dataFakeArr = { "Pooled", "Bernard Unfortunate", "Adam Smith", "Julie Mirage", "Christian Donnovan", "Elon Carousel", "Irish Fish", "Irish Fish", "Irish Fish", "Irish Fish", "Irish Fish" };
            var command = new TableCommand(dataFakeArr, manager);

            //3(potatoes)+3(potatoes)+25(tuna) = 31 * (1 + 0.3) = 40.3;  40.3 * (1 - 0.2) =  32.24;
            var expectedBudget = 251.2;                     //90 + 32.24 + 32.24 + 32.24 + 32.24 + 32.24 = 251.2
            var expectedBudgetOfCustomerBernard = 19.7;
            var expectedBudgetOfCustomerAdam = 59.7;
            var expectedBudgetOfCustomerJulie = 159.7;
            var expectedBudgetOfCustomerChristian = 34.7;
            var expectedBudgetOfCustomerElon = 9.7;


            //when
            var report = command.Execute();

            //then
            var restaurantNew = manager.GetRestaurant(AppConfig.Settings.MainRestaurantName);
            var warehouseDishFriesNew = manager.GetWarehouseDish("Fries");
            var warehouseIngredientPotatoesNew = manager.GetWarehouseIngredient("Potatoes");
            var warehouseIngredientTunaNew = manager.GetWarehouseIngredient("Tuna");
            var customerBernardNew = manager.GetCustomer("Bernard Unfortunate");
            var customerAdamNew = manager.GetCustomer("Adam Smith");
            var customerJulieNew = manager.GetCustomer("Julie Mirage");
            var customerChristianNew = manager.GetCustomer("Christian Donnovan");
            var customerElonNew = manager.GetCustomer("Elon Carousel");

            Assert.IsTrue(report.CommandSucceeded);
            Assert.AreEqual(expectedBudget, restaurantNew.Budget);
            Assert.IsNull(warehouseDishFriesNew);
            Assert.IsNull(warehouseIngredientPotatoesNew);
            Assert.IsNull(warehouseIngredientTunaNew);
            Assert.AreEqual(expectedBudgetOfCustomerBernard, customerBernardNew.Budget);
            Assert.AreEqual(expectedBudgetOfCustomerAdam, customerAdamNew.Budget);
            Assert.AreEqual(expectedBudgetOfCustomerJulie, customerJulieNew.Budget);
            Assert.AreEqual(expectedBudgetOfCustomerChristian, customerChristianNew.Budget);
            Assert.AreEqual(expectedBudgetOfCustomerElon, customerElonNew.Budget);
        }

        [Test]
        public void Execute_SomeCustomersHasNoEnoughMoneySomeCustomersHasEnoughMoneyAndSomeCustomersHasAllergyAndPooledTrue_PositiveResultOneCustomerWasLeftWithMoeny()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true, true, false, true, false, true);

            //======================================================== customer

            //Ok, customer has enough money
            Customer customerJulie = new Customer
            {
                FullName = "Julie Mirage",
                Budget = 50
            };

            manager.AddCustomer(customerJulie);

            //has not enough money
            Customer customerChristian = new Customer
            {
                FullName = "Christian Donnovan",
                Budget = 10
            };

            manager.AddCustomer(customerChristian);

            //has not enough money
            Customer customerElon = new Customer
            {
                FullName = "Elon Carousel",
                Budget = 5
            };

            manager.AddCustomer(customerElon);

            //                                       has allergy    |    OK      |     OK      | has not enough money | has not enough money |
            string[] dataFakeArr = { "Pooled", "Bernard Unfortunate", "Adam Smith", "Julie Mirage", "Christian Donnovan", "Elon Carousel", "Irish Fish", "Irish Fish", "Irish Fish", "Irish Fish", "Irish Fish" };
            var command = new TableCommand(dataFakeArr, manager);

            //3(potatoes)+3(potatoes)+25(tuna) = 31 * (1 + 0.3) = 40.3;  40.3 * (1 - 0.2) =  32.24;
            var expectedBudget = 218.96;                 //90 + 32.24 + 32.24 + 32.24 + 32.24 = 218,96
            var expectedBudgetOfCustomerBernard = 30;
            var expectedBudgetOfCustomerAdam = 3.8;
            var expectedBudgetOfCustomerJulie = 0;
            var expectedBudgetOfCustomerChristian = 0;
            var expectedBudgetOfCustomerElon = 0;


            //when
            var report = command.Execute();

            //then
            var restaurantNew = manager.GetRestaurant(AppConfig.Settings.MainRestaurantName);
            var warehouseDishFriesNew = manager.GetWarehouseDish("Fries");
            var warehouseIngredientPotatoesNew = manager.GetWarehouseIngredient("Potatoes");
            var warehouseIngredientTunaNew = manager.GetWarehouseIngredient("Tuna");
            var customerBernardNew = manager.GetCustomer("Bernard Unfortunate");
            var customerAdamNew = manager.GetCustomer("Adam Smith");
            var customerJulieNew = manager.GetCustomer("Julie Mirage");
            var customerChristianNew = manager.GetCustomer("Christian Donnovan");
            var customerElonNew = manager.GetCustomer("Elon Carousel");


            Assert.IsTrue(report.CommandSucceeded);
            Assert.AreEqual(expectedBudget, restaurantNew.Budget);
            Assert.IsNull(warehouseDishFriesNew);
            Assert.IsNull(warehouseIngredientPotatoesNew);
            Assert.IsNull(warehouseIngredientTunaNew);
            Assert.AreEqual(expectedBudgetOfCustomerBernard, customerBernardNew.Budget);
            Assert.AreEqual(expectedBudgetOfCustomerAdam, customerAdamNew.Budget);
            Assert.AreEqual(expectedBudgetOfCustomerJulie, customerJulieNew.Budget);
            Assert.AreEqual(expectedBudgetOfCustomerChristian, customerChristianNew.Budget);
            Assert.AreEqual(expectedBudgetOfCustomerElon, customerElonNew.Budget);
        }

        [Test]
        public void Execute_SomeCustomersHasNoEnoughMoneySomeCustomersHasEnoughMoneyAndSomeCustomersHasAllergyAndPooledTrue_PositiveResultTwoCustomersWereLeftWithMoeny()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true, true, false, true, false, true);

            //======================================================== customer

            //Ok, customer has enough money
            Customer customerJulie = new Customer
            {
                FullName = "Julie Mirage",
                Budget = 200
            };

            manager.AddCustomer(customerJulie);

            //has not enough money
            Customer customerChristian = new Customer
            {
                FullName = "Christian Donnovan",
                Budget = 10
            };

            manager.AddCustomer(customerChristian);

            //has not enough money
            Customer customerElon = new Customer
            {
                FullName = "Elon Carousel",
                Budget = 5
            };

            manager.AddCustomer(customerElon);

            //                                       has allergy    |    OK      |     OK      | has not enough money | has not enough money |
            string[] dataFakeArr = { "Pooled", "Bernard Unfortunate", "Adam Smith", "Julie Mirage", "Christian Donnovan", "Elon Carousel", "Irish Fish", "Irish Fish", "Irish Fish", "Irish Fish", "Irish Fish" };
            var command = new TableCommand(dataFakeArr, manager);

            //3(potatoes)+3(potatoes)+25(tuna) = 31 * (1 + 0.3) = 40.3;  40.3 * (1 - 0.2) =  32.24;
            var expectedBudget = 218.96;                //90 + 32.24 + 32.24 + 32.24 + 32.24 = 218.96
            var expectedBudgetOfCustomerBernard = 30;
            var expectedBudgetOfCustomerAdam = 26.9;
            var expectedBudgetOfCustomerJulie = 126.9;
            var expectedBudgetOfCustomerChristian = 0;
            var expectedBudgetOfCustomerElon = 0;


            //when
            var report = command.Execute();

            //then
            var restaurantNew = manager.GetRestaurant(AppConfig.Settings.MainRestaurantName);
            var warehouseDishFriesNew = manager.GetWarehouseDish("Fries");
            var warehouseIngredientPotatoesNew = manager.GetWarehouseIngredient("Potatoes");
            var warehouseIngredientTunaNew = manager.GetWarehouseIngredient("Tuna");
            var customerBernardNew = manager.GetCustomer("Bernard Unfortunate");
            var customerAdamNew = manager.GetCustomer("Adam Smith");
            var customerJulieNew = manager.GetCustomer("Julie Mirage");
            var customerChristianNew = manager.GetCustomer("Christian Donnovan");
            var customerElonNew = manager.GetCustomer("Elon Carousel");

            Assert.IsTrue(report.CommandSucceeded);
            Assert.AreEqual(expectedBudget, restaurantNew.Budget);
            Assert.IsNull(warehouseDishFriesNew);
            Assert.IsNull(warehouseIngredientPotatoesNew);
            Assert.IsNull(warehouseIngredientTunaNew);
            Assert.AreEqual(expectedBudgetOfCustomerBernard, customerBernardNew.Budget);
            Assert.AreEqual(expectedBudgetOfCustomerAdam, customerAdamNew.Budget);
            Assert.AreEqual(expectedBudgetOfCustomerJulie, customerJulieNew.Budget);
            Assert.AreEqual(expectedBudgetOfCustomerChristian, customerChristianNew.Budget);
            Assert.AreEqual(expectedBudgetOfCustomerElon, customerElonNew.Budget);
        }

        [Test]
        public void Execute_SomeCustomersHasNoEnoughMoneySomeCustomersHasEnoughMoneyAndSomeCustomersHasAllergyAndPooledTrueAndGenerallyCastomersHaveNotEnoughMoneyForTable_NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true, true, false, true, false, true);

            //======================================================== customer

            //has not enough money
            Customer customerJulie = new Customer
            {
                FullName = "Julie Mirage",
                Budget = 35
            };

            manager.AddCustomer(customerJulie);

            //has not enough money
            Customer customerChristian = new Customer
            {
                FullName = "Christian Donnovan",
                Budget = 10
            };

            manager.AddCustomer(customerChristian);

            //has not enough money
            Customer customerElon = new Customer
            {
                FullName = "Elon Carousel",
                Budget = 5
            };

            manager.AddCustomer(customerElon);

            //                                       has allergy    |    OK      |     OK      | has not enough money | has not enough money |
            string[] dataFakeArr = { "Pooled", "Bernard Unfortunate", "Adam Smith", "Julie Mirage", "Christian Donnovan", "Elon Carousel", "Irish Fish", "Irish Fish", "Irish Fish", "Irish Fish", "Irish Fish" };
            var command = new TableCommand(dataFakeArr, manager);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsNotNull(report.Message.EndsWith(" => fail, not enough money in customers for doing this operation"));
        }

        private void GenerateBaseFakeDatabase(ManagetRepositoryMock manager, bool isCommandEnabled)
        {
            //======================================================== config
            CommandConfig commandConfig = new CommandConfig()
            {
                Audit = true,
                Budget = true,
                Buy = true,
                Order = true,
                Table = isCommandEnabled
            };

            TaxConfig taxConfig = new TaxConfig()
            {
                DailyTax = 20,
                ProfitMargin = 30,
                TransactionTax = 20
            };

            AllergyConfig allergyConfig = new AllergyConfig()
            {
                DishesWithAllergies = "Waste"
            };

            TimeCostConfig timeCostConfig = new TimeCostConfig()
            {
                BudgetCost = 250
            };

            var appsetting = new Appsetting()
            {
                Command = commandConfig,
                Tax = taxConfig,
                TimeCost = timeCostConfig,
                MainRestaurantName = "Gurman",
                Allergy = allergyConfig,
                Warehouse = new WarehouseConfig
                {
                    MaxTrash = 0
                },
                Tip = new TipConfig { MaxTipValue = 0 }
            };

            AppConfig.Settings = appsetting;
        }
        [Test]
        public void Execute_AllCustomersHasEnoughMoneyAndEveryOneHasNoAllergyAndPooledTrueWeRunItAsSimpleTablePooledRecommend_PositiveResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, false, true, true, true, false, true);

            //======================================================== customer

            //Ok, customer has enough money
            Customer customerJulie = new Customer
            {
                FullName = "Julie Mirage",
                Budget = 200
            };

            manager.AddCustomer(customerJulie);

            //has not enough money
            Customer customerChristian = new Customer
            {
                FullName = "Christian Donnovan",
                Budget = 75
            };

            manager.AddCustomer(customerChristian);

            //has not enough money
            Customer customerElon = new Customer
            {
                FullName = "Elon Carousel",
                Budget = 50
            };

            manager.AddCustomer(customerElon);

            //                                       has allergy    |    OK      |     OK      | has not enough money | has not enough money |
            string[] dataFakeArr = { "Pooled", "Bernard Unfortunate", "Adam Smith", "Julie Mirage", "Christian Donnovan", "Elon Carousel", "Recommend", "Potatoes", "Tuna", "Irish Fish", "Recommend", "Potatoes", "Tuna", "Irish Fish", "Irish Fish" };
            var command = new TableCommand(dataFakeArr, manager);

            //3(potatoes)+3(potatoes)+25(tuna) = 31 * (1 + 0.3) = 40.3;  40.3 * (1 + 0.2) =  48.36(allergy);  40.3 * (1 - 0.2) =  32.24((no allargy));
            //90 + 32.24 + 32.24 + 32.24 + 32.24 + 32.24 = 251.2
            var expectedBudget = 251.2;
            var expectedBudgetOfCustomerBernard = 19.7;
            var expectedBudgetOfCustomerAdam = 59.7;
            var expectedBudgetOfCustomerJulie = 159.7;
            var expectedBudgetOfCustomerChristian = 34.7;
            var expectedBudgetOfCustomerElon = 9.7;


            //when
            var report = command.Execute();

            //then
            var restaurantNew = manager.GetRestaurant(AppConfig.Settings.MainRestaurantName);
            var warehouseDishFriesNew = manager.GetWarehouseDish("Fries");
            var warehouseIngredientPotatoesNew = manager.GetWarehouseIngredient("Potatoes");
            var warehouseIngredientTunaNew = manager.GetWarehouseIngredient("Tuna");
            var customerBernardNew = manager.GetCustomer("Bernard Unfortunate");
            var customerAdamNew = manager.GetCustomer("Adam Smith");
            var customerJulieNew = manager.GetCustomer("Julie Mirage");
            var customerChristianNew = manager.GetCustomer("Christian Donnovan");
            var customerElonNew = manager.GetCustomer("Elon Carousel");

            Assert.IsTrue(report.CommandSucceeded);
            Assert.AreEqual(expectedBudget, restaurantNew.Budget);
            Assert.IsNull(warehouseDishFriesNew);
            Assert.IsNull(warehouseIngredientPotatoesNew);
            Assert.IsNull(warehouseIngredientTunaNew);
            Assert.AreEqual(expectedBudgetOfCustomerBernard, customerBernardNew.Budget);
            Assert.AreEqual(expectedBudgetOfCustomerAdam, customerAdamNew.Budget);
            Assert.AreEqual(expectedBudgetOfCustomerJulie, customerJulieNew.Budget);
            Assert.AreEqual(expectedBudgetOfCustomerChristian, customerChristianNew.Budget);
            Assert.AreEqual(expectedBudgetOfCustomerElon, customerElonNew.Budget);
        }
        private void GenerateFakeDatabase(ManagetRepositoryMock manager, bool IsAllergy, bool IsEnoughWarehouseIngredient, bool isEnoughBudgetOfCustomer, bool isEnoughBudgetOfRestaurant, bool IsAllergyEveryOne, bool isCommandEnabled, bool isBankrupt = false)
        {
            //======================================================== config
            GenerateBaseFakeDatabase(manager, isCommandEnabled);

            //======================================================== restaurant
            Restaurant restaurant = new Restaurant
            {
                Name = AppConfig.Settings.MainRestaurantName,
                Budget = isBankrupt ? -60 : 60
            };

            if (isEnoughBudgetOfRestaurant)
            {
                restaurant.Budget = 90;
            }

            manager.AddRestaurant(restaurant);

            //======================================================== ingredient
            Ingredient ingredientPotatoes = new Ingredient
            {
                Name = "Potatoes",
                Price = 3
            };

            Ingredient ingredientTuna = new Ingredient
            {
                Name = "Tuna",
                Price = 25
            };

            manager.AddIngredient(ingredientPotatoes);
            manager.AddIngredient(ingredientTuna);

            //======================================================== dish
            Dish dishSmashedPotatoes = new Dish
            {
                Name = "Smashed Potatoes",
                Ingredients = new List<Ingredient>() { ingredientPotatoes },
            };

            Dish dishFries = new Dish
            {
                Name = "Fries",
                Ingredients = new List<Ingredient>() { ingredientPotatoes }
            };

            Dish dishIrishFish = new Dish
            {
                Name = "Irish Fish",
                Dishes = new List<Dish>() { dishSmashedPotatoes, dishFries },
                Ingredients = new List<Ingredient>() { ingredientTuna }
            };

            manager.AddDish(dishFries);
            manager.AddDish(dishSmashedPotatoes);
            manager.AddDish(dishIrishFish);

            //======================================================== warehouse dish
            WarehouseDish warehouseDishFries = new WarehouseDish
            {
                Dish = dishFries,
                Quantity = 3
            };

            manager.AddWarehouseDish(warehouseDishFries);

            //======================================================== warehouse ingredient
            WarehouseIngredient warehouseIngredientPotatoes = new WarehouseIngredient
            {
                Ingredient = ingredientPotatoes,
                Quantity = 7
            };

            manager.AddWarehouseIngredient(warehouseIngredientPotatoes);

            if (IsEnoughWarehouseIngredient)
            {
                WarehouseIngredient warehouseIngredientTuna = new WarehouseIngredient
                {
                    Ingredient = ingredientTuna,
                    Quantity = 5
                };

                manager.AddWarehouseIngredient(warehouseIngredientTuna);
            }

            //======================================================== customer
            Customer customerBernard = new Customer
            {
                FullName = "Bernard Unfortunate",
                Budget = 30
            };

            if (IsAllergy || IsAllergyEveryOne)
            {
                customerBernard.Allergies = new List<Ingredient>() { ingredientPotatoes };
            }

            if (isEnoughBudgetOfCustomer)
            {
                customerBernard.Budget = 60;
            }

            manager.AddCustomer(customerBernard);

            Customer customerAdam = new Customer
            {
                FullName = "Adam Smith",
                Budget = 100
            };

            if (IsAllergyEveryOne)
            {
                customerAdam.Allergies = new List<Ingredient>() { ingredientPotatoes };
            }

            manager.AddCustomer(customerAdam);
        }

    }
}
