﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Infrastructure.Commands;
using NUnit.Framework;
using System.Collections.Generic;

namespace ConsoleRestaurant.Tests.Infrastructure
{
    public class BuyCommandTests
    {
        [Test]
        public void GetTotalPrice_MarkupMarginTax_TotalPrice()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateBaseFakeDatabase(manager, true);

            var expectedTotalPrice = 130;
            var ing = new Ingredient
            {
                Price = 100
            };

            var dish = new Dish
            {
                Ingredients = new List<Ingredient> { ing }
            };

            //when
            var totalPrice = dish.GetTotalPrice();

            //then
            Assert.AreEqual(expectedTotalPrice, totalPrice);
        }

        [TestCase(2, true, 0)]
        [TestCase(2, false, 5)]
        [TestCase(4, true, 0)]
        [TestCase(4, false, 0)]
        public void GetDiscountAmount_CustomerMakeTheThirdBuying_ProperDiscount(int times, bool isAllergy, double expectedDiscount)
        {
            //given
            var manager = new ManagetRepositoryMock();

            Customer customerBernard = new Customer
            {
                FullName = "Bernard Unfortunate",
                Budget = 60
            };

            ActivityOfBuying activityOfBuying = new ActivityOfBuying()
            {
                Customer = customerBernard,
                Times = times
            };

            manager.AddActivityOfBuying(activityOfBuying);

            CouponConfig couponConfig = new CouponConfig()
            {
                EveryThirdDiscount = 10
            };

            Appsetting appsetting = new Appsetting()
            {
                Coupon = couponConfig
            };

            AppConfig.Settings = appsetting;

            string[] dataFakeArr = { "Bernard Unfortunate", "Irish Fish" };
            var command = new BuyCommand(dataFakeArr, manager);

            //when
            var result = command.GetDiscountAmount(customerBernard, 50, isAllergy);

            //then
            Assert.AreEqual(expectedDiscount, result);
        }

        [Test]
        public void Execute_CommandIsNotEnabledToUse_NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, false, false, false, true, true, false);

            string[] dataFakeArr = { "Bernard Unfortunate", "Irish Fish" };
            var command = new BuyCommand(dataFakeArr, manager, false);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsTrue(report.Message.EndsWith(" disabled"));
        }

        [Test]
        public void Execute_PassedDataHasWrong_NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true, false, false, true, true, false);

            string[] dataFakeArr = { "Bernard Unfortunate" };
            var command = new BuyCommand(dataFakeArr, manager);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsTrue(report.Message.EndsWith("Record is not correct"));
        }

        [Test]
        public void Execute_WarehouseHasNoEnoughIngredients_NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true, false, false, true, true, false);

            string[] dataFakeArr = { "Bernard Unfortunate", "Irish Fish" };
            var command = new BuyCommand(dataFakeArr, manager);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsTrue(report.Message.EndsWith("fail, not enough ingredients or dishes on the warehouse"));
        }

        [Test]
        public void Execute_CustomerHasAllergyAndAllergyIsWasteAndWarehouseHasAll_PositiveResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true, true, true, true, true, false);

            AllergyConfig allergyConfig = new AllergyConfig()
            {
                DishesWithAllergies = "Waste"
            };

            WarehouseConfig warehouseConfig = new WarehouseConfig()
            {
                MaxDishType = 3,
                MaxIngredientType = 10,
                TotalMaximum = 30
            };

            AppConfig.Settings.Allergy = allergyConfig;
            AppConfig.Settings.Warehouse = warehouseConfig;

            var expectedBudget = 60;
            var expectedQuantityWarehouseDishFries = 2;             //3-1=2
            var expectedQuantityWarehouseIngredientPotatoes = 4;    //5-1=4
            var expectedQuantityWarehouseIngredientTuna = 4;        //5-1=4

            string[] dataFakeArr = { "Bernard Unfortunate", "Irish Fish" };
            var command = new BuyCommand(dataFakeArr, manager, false);

            //when
            var report = command.Execute();

            //then
            var restaurantNew = manager.GetRestaurant(AppConfig.Settings.MainRestaurantName);
            var warehouseDishFriesNew = manager.GetWarehouseDish("Fries");
            var warehouseIngredientPotatoesNew = manager.GetWarehouseIngredient("Potatoes");
            var warehouseIngredientTunaNew = manager.GetWarehouseIngredient("Tuna");

            Assert.IsTrue(report.CommandSucceeded);
            Assert.AreEqual(expectedBudget, restaurantNew.Budget);
            Assert.AreEqual(expectedQuantityWarehouseDishFries, warehouseDishFriesNew.Quantity);
            Assert.AreEqual(expectedQuantityWarehouseIngredientPotatoes, warehouseIngredientPotatoesNew.Quantity);
            Assert.AreEqual(expectedQuantityWarehouseIngredientTuna, warehouseIngredientTunaNew.Quantity);
        }

        [Test]
        public void Execute_CustomerHasAllergyAndAllergyIsKeepAndWarehouseHasAll_PositiveResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true, true, true, true, true, false);

            AllergyConfig allergyConfig = new AllergyConfig()
            {
                DishesWithAllergies = "Keep"
            };

            WarehouseConfig warehouseConfig = new WarehouseConfig()
            {
                MaxDishType = 3,
                MaxIngredientType = 10,
                TotalMaximum = 30,
                SpoilRate = 0
            };

            AppConfig.Settings.Allergy = allergyConfig;
            AppConfig.Settings.Warehouse = warehouseConfig;

            var expectedBudget = 49.93;        //3(potatoes)+3(potatoes)+25(tuna) = 31 * (1 + 0.3) = 40.3;  40.3 * 0.25 = 10.075  60 - 10.075 = 49.92 (49.93)
            var expectedQuantityWarehouseDishIrishFish = 1;         //0+1=1
            var expectedQuantityWarehouseDishFries = 2;             //3-1=2
            var expectedQuantityWarehouseIngredientPotatoes = 4;    //5-1=4
            var expectedQuantityWarehouseIngredientTuna = 4;        //5-1=4

            string[] dataFakeArr = { "Bernard Unfortunate", "Irish Fish" };
            var command = new BuyCommand(dataFakeArr, manager, false);

            //when
            var report = command.Execute();

            //then
            var restaurantNew = manager.GetRestaurant(AppConfig.Settings.MainRestaurantName);
            var warehouseIngredientIrishFishNew = manager.GetWarehouseDish("Irish Fish");
            var warehouseDishFriesNew = manager.GetWarehouseDish("Fries");
            var warehouseIngredientPotatoesNew = manager.GetWarehouseIngredient("Potatoes");
            var warehouseIngredientTunaNew = manager.GetWarehouseIngredient("Tuna");

            Assert.IsTrue(report.CommandSucceeded);
            Assert.AreEqual(expectedBudget, restaurantNew.Budget);
            Assert.AreEqual(expectedQuantityWarehouseDishIrishFish, warehouseIngredientIrishFishNew.Quantity);
            Assert.AreEqual(expectedQuantityWarehouseDishFries, warehouseDishFriesNew.Quantity);
            Assert.AreEqual(expectedQuantityWarehouseIngredientPotatoes, warehouseIngredientPotatoesNew.Quantity);
            Assert.AreEqual(expectedQuantityWarehouseIngredientTuna, warehouseIngredientTunaNew.Quantity);
        }

        [Test]
        public void Execute_CustomerHasAllergyAndAllergyStateIsLimitAsKeepAndWarehouseHasAll_PositiveResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true, true, true, true, true, false);

            AllergyConfig allergyConfig = new AllergyConfig()
            {
                DishesWithAllergies = "30"
            };

            WarehouseConfig warehouseConfig = new WarehouseConfig()
            {
                MaxDishType = 3,
                MaxIngredientType = 10,
                TotalMaximum = 30
            };

            AppConfig.Settings.Allergy = allergyConfig;
            AppConfig.Settings.Warehouse = warehouseConfig;

            var expectedBudget = 49.93;        //3(potatoes)+3(potatoes)+25(tuna) = 31 * (1 + 0.3) = 40.3;  40.3 * 0.25 = 10.075  60 - 10.075 = 49.92 (49.93)
            var expectedQuantityWarehouseDishIrishFish = 1;         //0+1=1
            var expectedQuantityWarehouseDishFries = 2;             //3-1=2
            var expectedQuantityWarehouseIngredientPotatoes = 4;    //5-1=4
            var expectedQuantityWarehouseIngredientTuna = 4;        //5-1=4

            string[] dataFakeArr = { "Bernard Unfortunate", "Irish Fish" };
            var command = new BuyCommand(dataFakeArr, manager, false);

            //when
            var report = command.Execute();

            //then
            var restaurantNew = manager.GetRestaurant(AppConfig.Settings.MainRestaurantName);
            var warehouseIngredientIrishFishNew = manager.GetWarehouseDish("Irish Fish");
            var warehouseDishFriesNew = manager.GetWarehouseDish("Fries");
            var warehouseIngredientPotatoesNew = manager.GetWarehouseIngredient("Potatoes");
            var warehouseIngredientTunaNew = manager.GetWarehouseIngredient("Tuna");

            Assert.IsTrue(report.CommandSucceeded);
            Assert.AreEqual(expectedBudget, restaurantNew.Budget);
            Assert.AreEqual(expectedQuantityWarehouseDishIrishFish, warehouseIngredientIrishFishNew.Quantity);
            Assert.AreEqual(expectedQuantityWarehouseDishFries, warehouseDishFriesNew.Quantity);
            Assert.AreEqual(expectedQuantityWarehouseIngredientPotatoes, warehouseIngredientPotatoesNew.Quantity);
            Assert.AreEqual(expectedQuantityWarehouseIngredientTuna, warehouseIngredientTunaNew.Quantity);
        }

        [Test]
        public void Execute_CustomerHasAllergyAndAllergyStateIsLimitAsWasteAndWarehouseHasAll_PositiveResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true, true, true, true, true, false);

            AllergyConfig allergyConfig = new AllergyConfig()
            {
                DishesWithAllergies = "100"
            };

            WarehouseConfig warehouseConfig = new WarehouseConfig()
            {
                MaxDishType = 3,
                MaxIngredientType = 10,
                TotalMaximum = 30
            };

            AppConfig.Settings.Allergy = allergyConfig;
            AppConfig.Settings.Warehouse = warehouseConfig;

            var expectedBudget = 60;
            var expectedQuantityWarehouseDishFries = 2;             //3-1=2
            var expectedQuantityWarehouseIngredientPotatoes = 4;    //5-1=4
            var expectedQuantityWarehouseIngredientTuna = 4;        //5-1=4

            string[] dataFakeArr = { "Bernard Unfortunate", "Irish Fish" };
            var command = new BuyCommand(dataFakeArr, manager, false);

            //when
            var report = command.Execute();

            //then
            var restaurantNew = manager.GetRestaurant(AppConfig.Settings.MainRestaurantName);
            var warehouseDishFriesNew = manager.GetWarehouseDish("Fries");
            var warehouseIngredientPotatoesNew = manager.GetWarehouseIngredient("Potatoes");
            var warehouseIngredientTunaNew = manager.GetWarehouseIngredient("Tuna");

            Assert.IsTrue(report.CommandSucceeded);
            Assert.AreEqual(expectedBudget, restaurantNew.Budget);
            Assert.AreEqual(expectedQuantityWarehouseDishFries, warehouseDishFriesNew.Quantity);
            Assert.AreEqual(expectedQuantityWarehouseIngredientPotatoes, warehouseIngredientPotatoesNew.Quantity);
            Assert.AreEqual(expectedQuantityWarehouseIngredientTuna, warehouseIngredientTunaNew.Quantity);
        }

        [Test]
        public void Execute_CustomerHasAllergyAndAllergyStateIsKeepAsWasteNoSpaceInTheWarehouseToPutNewDishOrBudgetOfRestaurantNotEnoughToPayForNewDishAndWarehouseHasAll_PositiveResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true, true, true, true, true, false);

            AllergyConfig allergyConfig = new AllergyConfig()
            {
                DishesWithAllergies = "100"
            };

            WarehouseConfig warehouseConfig = new WarehouseConfig()
            {
                MaxDishType = 0,
                MaxIngredientType = 10,
                TotalMaximum = 30
            };

            AppConfig.Settings.Allergy = allergyConfig;
            AppConfig.Settings.Warehouse = warehouseConfig;

            var expectedBudget = 60;
            var expectedQuantityWarehouseDishFries = 2;             //3-1=2
            var expectedQuantityWarehouseIngredientPotatoes = 4;    //5-1=4
            var expectedQuantityWarehouseIngredientTuna = 4;        //5-1=4

            string[] dataFakeArr = { "Bernard Unfortunate", "Irish Fish" };
            var command = new BuyCommand(dataFakeArr, manager, false);

            //when
            var report = command.Execute();

            //then
            var restaurantNew = manager.GetRestaurant(AppConfig.Settings.MainRestaurantName);
            var warehouseDishFriesNew = manager.GetWarehouseDish("Fries");
            var warehouseIngredientPotatoesNew = manager.GetWarehouseIngredient("Potatoes");
            var warehouseIngredientTunaNew = manager.GetWarehouseIngredient("Tuna");

            Assert.IsTrue(report.CommandSucceeded);
            Assert.AreEqual(expectedBudget, restaurantNew.Budget);
            Assert.AreEqual(expectedQuantityWarehouseDishFries, warehouseDishFriesNew.Quantity);
            Assert.AreEqual(expectedQuantityWarehouseIngredientPotatoes, warehouseIngredientPotatoesNew.Quantity);
            Assert.AreEqual(expectedQuantityWarehouseIngredientTuna, warehouseIngredientTunaNew.Quantity);
        }

        [Test]
        public void Execute_BudgetIsBankrupt_NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true, true, true, true, false, false, true);
            string[] dataFakeArr = { "Bernard Unfortunate", "Irish Fish" };
            var command = new BuyCommand(dataFakeArr, manager, false);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsTrue(report.IsBankruptcy);
        }

        [Test]
        public void Execute_CustomerHasNoAllergyAndHasNoEnoughMoney_NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true, false, true, false, true, false);

            string[] dataFakeArr = { "Bernard Unfortunate", "Irish Fish" };
            var command = new BuyCommand(dataFakeArr, manager, false);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsTrue(report.Message.EndsWith(" => fail, not enough money"));
        }

        [Test]
        public void Execute_CustomerHasNoAllergyAndHasEnoughMoneyAndWarehouseHasAll_PositiveResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true, false, true, true, true, false);

            var expectedBudget = 92.24;        //3(potatoes)+3(potatoes)+25(tuna) = 31 * 1.3 = 40.3;   40.3 * (1 - 0.2) = 32.24;  60 + 32.24 = 92.24
            var expectedQuantityWarehouseDishFries = 2;             //3-1=2
            var expectedQuantityWarehouseIngredientPotatoes = 4;    //5-1=4
            var expectedQuantityWarehouseIngredientTuna = 4;        //5-1=4
            var expectedBudgetOfCustomer = 19.7;  //3(potatoes)+3(potatoes)+25(tuna) = 31 * 1.3 = 40.3;   60 - 40.3 = 19.7

            string[] dataFakeArr = { "Bernard Unfortunate", "Irish Fish" };
            var command = new BuyCommand(dataFakeArr, manager, false);

            //when
            var report = command.Execute();

            //then
            var restaurantNew = manager.GetRestaurant(AppConfig.Settings.MainRestaurantName);
            var warehouseDishFriesNew = manager.GetWarehouseDish("Fries");
            var warehouseIngredientPotatoesNew = manager.GetWarehouseIngredient("Potatoes");
            var warehouseIngredientTunaNew = manager.GetWarehouseIngredient("Tuna");
            var customerNew = manager.GetCustomer("Bernard Unfortunate");

            Assert.IsTrue(report.CommandSucceeded);
            Assert.AreEqual(expectedQuantityWarehouseDishFries, warehouseDishFriesNew.Quantity);
            Assert.AreEqual(expectedQuantityWarehouseIngredientPotatoes, warehouseIngredientPotatoesNew.Quantity);
            Assert.AreEqual(expectedQuantityWarehouseIngredientTuna, warehouseIngredientTunaNew.Quantity);
            Assert.AreEqual(expectedBudgetOfCustomer, customerNew.Budget);
            Assert.AreEqual(expectedBudget, restaurantNew.Budget);
        }

        [Test]
        public void Execute_CustomerHasNoAllergyAndHasEnoughMoneyAndWarehouseHasAllAndHasDiscount_PositiveResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true, false, true, true, true, true);

            var expectedBudget = 89.02;        //3(potatoes)+3(potatoes)+25(tuna) = 31 * 1.3 = 40.3; 40.3 * (1 - 0.1) = 36.27;  36.27 * (1 - 0.2) = 29.02;  60 + 29.02 = 89.02
            var expectedQuantityWarehouseDishFries = 2;             //3-1=2
            var expectedQuantityWarehouseIngredientPotatoes = 4;    //5-1=4
            var expectedQuantityWarehouseIngredientTuna = 4;        //5-1=4
            var expectedBudgetOfCustomer = 23.73;  //3(potatoes)+3(potatoes)+25(tuna) = 31 * 1.3 = 40.3; 40.3 * (1 - 0.1) = 36.27;  60 - 36.27 = 23.73

            string[] dataFakeArr = { "Bernard Unfortunate", "Irish Fish" };
            var command = new BuyCommand(dataFakeArr, manager, false);

            //when
            var report = command.Execute();

            //then
            var restaurantNew = manager.GetRestaurant(AppConfig.Settings.MainRestaurantName);
            var warehouseDishFriesNew = manager.GetWarehouseDish("Fries");
            var warehouseIngredientPotatoesNew = manager.GetWarehouseIngredient("Potatoes");
            var warehouseIngredientTunaNew = manager.GetWarehouseIngredient("Tuna");
            var customerNew = manager.GetCustomer("Bernard Unfortunate");

            Assert.IsTrue(report.CommandSucceeded);
            Assert.AreEqual(expectedBudget, restaurantNew.Budget);
            Assert.AreEqual(expectedQuantityWarehouseDishFries, warehouseDishFriesNew.Quantity);
            Assert.AreEqual(expectedQuantityWarehouseIngredientPotatoes, warehouseIngredientPotatoesNew.Quantity);
            Assert.AreEqual(expectedQuantityWarehouseIngredientTuna, warehouseIngredientTunaNew.Quantity);
            Assert.AreEqual(expectedBudgetOfCustomer, customerNew.Budget);
        }
        [Test]
        public void Execute_CustomerHasNoAllergyAndHasEnoughMoneyAndWarehouseHasAllRecommend_PositiveResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true, false, true, true, true, false);
            var fakeRnd = new FakeRnd();
            var queue = new Queue<double>(); //{ 0, 0, 0, 1, 2, 1 }
            queue.Enqueue(0);
            queue.Enqueue(0);
            queue.Enqueue(0);
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(1);
            fakeRnd.Values = queue;

            var expectedBudget = 92.24;        //3(potatoes)+3(potatoes)+25(tuna) = 31 * 1.3 = 40.3;   40.3 * (1 - 0.2) = 32.24;  60 + 32.24 = 92.24
            var expectedQuantityWarehouseDishFries = 2;             //3-1=2
            var expectedQuantityWarehouseIngredientPotatoes = 4;    //5-1=4
            var expectedQuantityWarehouseIngredientTuna = 4;        //5-1=4
            var expectedBudgetOfCustomer = 19.7;  //3(potatoes)+3(potatoes)+25(tuna) = 31 * 1.3 = 40.3;   60 - 40.3 = 19.7

            string[] dataFakeArr = { "Bernard Unfortunate", "Recommend", "Potatoes", "Tuna" };
            var command = new BuyCommand(dataFakeArr, manager, false);
            command.rnd = fakeRnd;
            //when
            var report = command.Execute();

            //then
            var restaurantNew = manager.GetRestaurant(AppConfig.Settings.MainRestaurantName);
            var warehouseDishFriesNew = manager.GetWarehouseDish("Fries");
            var warehouseIngredientPotatoesNew = manager.GetWarehouseIngredient("Potatoes");
            var warehouseIngredientTunaNew = manager.GetWarehouseIngredient("Tuna");
            var customerNew = manager.GetCustomer("Bernard Unfortunate");

            Assert.IsTrue(report.CommandSucceeded);
            Assert.AreEqual(expectedQuantityWarehouseDishFries, warehouseDishFriesNew.Quantity);
            Assert.AreEqual(expectedQuantityWarehouseIngredientPotatoes, warehouseIngredientPotatoesNew.Quantity);
            Assert.AreEqual(expectedQuantityWarehouseIngredientTuna, warehouseIngredientTunaNew.Quantity);
            Assert.AreEqual(expectedBudgetOfCustomer, customerNew.Budget);
            Assert.AreEqual(expectedBudget, restaurantNew.Budget);
        }
        private void GenerateBaseFakeDatabase(ManagetRepositoryMock manager, bool isCommandEnabled)
        {
            //======================================================== config
            CommandConfig commandConfig = new CommandConfig()
            {
                Audit = true,
                Budget = true,
                Buy = isCommandEnabled,
                Order = true,
                Table = true
            };

            TaxConfig taxConfig = new TaxConfig()
            {
                DailyTax = 20,
                ProfitMargin = 30,
                TransactionTax = 20
            };

            CouponConfig couponConfig = new CouponConfig()
            {
                EveryThirdDiscount = 10
            };

            AllergyConfig allergyConfig = new AllergyConfig()
            {
                DishesWithAllergies = "Waste"
            };

            TimeCostConfig timeCostConfig = new TimeCostConfig()
            {
                BudgetCost = 250
            };

            var appsetting = new Appsetting()
            {
                Command = commandConfig,
                Tax = taxConfig,
                TimeCost = timeCostConfig,
                MainRestaurantName = "Gurman",
                Allergy = allergyConfig,
                Coupon = couponConfig,
                Tip = new TipConfig
                {
                    MaxTipValue = 0
                },
                Warehouse = new WarehouseConfig { MaxTrash = 0, SpoilRate = 0 }
            };

            AppConfig.Settings = appsetting;
        }

        private void GenerateFakeDatabase(ManagetRepositoryMock manager, bool isCommandEnabled, bool IsAllergy, bool IsEnoughWarehouseIngredient, bool isEnoughBudgetOfCustomer, bool isEnoughBudgetOfRestaurant, bool isDiscount, bool isBankrupt = false)
        {
            //======================================================== config
            GenerateBaseFakeDatabase(manager, isCommandEnabled);

            //======================================================== restaurant
            Restaurant restaurant = new Restaurant
            {
                Name = AppConfig.Settings.MainRestaurantName,
                Budget = isBankrupt ? -30 : 30
            };

            if (isEnoughBudgetOfRestaurant)
            {
                restaurant.Budget = 60;
            }

            manager.AddRestaurant(restaurant);

            //======================================================== ingredient
            Ingredient ingredientPotatoes = new Ingredient
            {
                Name = "Potatoes",
                Price = 3
            };

            Ingredient ingredientTuna = new Ingredient
            {
                Name = "Tuna",
                Price = 25
            };

            Ingredient ingredientChicken = new Ingredient
            {
                Name = "Chicken",
                Price = 25
            };

            manager.AddIngredient(ingredientPotatoes);
            manager.AddIngredient(ingredientTuna);
            manager.AddIngredient(ingredientChicken);

            //======================================================== dish
            Dish dishSmashedPotatoes = new Dish
            {
                Name = "Smashed Potatoes",
                Ingredients = new List<Ingredient> { ingredientPotatoes },
            };

            Dish dishFries = new Dish
            {
                Name = "Fries",
                Ingredients = new List<Ingredient> { ingredientPotatoes }
            };

            Dish dishIrishFish = new Dish
            {
                Name = "Irish Fish",
                Dishes = new List<Dish> { dishSmashedPotatoes, dishFries },
                Ingredients = new List<Ingredient> { ingredientTuna }
            };

            manager.AddDish(dishFries);
            manager.AddDish(dishSmashedPotatoes);
            manager.AddDish(dishIrishFish);

            //======================================================== warehouse dish
            WarehouseDish warehouseDishFries = new WarehouseDish
            {
                Dish = dishFries,
                Quantity = 3
            };
            /* WarehouseDish warehouseDishSmashedpotatoes = new WarehouseDish
             {
                 Dish = dishSmashedPotatoes,
                 Quantity = 0
             };
             WarehouseDish warehouseDishIrishFish = new WarehouseDish
             {
                 Dish = dishIrishFish,
                 Quantity = 0
             };*/
            manager.AddWarehouseDish(warehouseDishFries);
            /* manager.AddWarehouseDish(warehouseDishSmashedpotatoes);
             manager.AddWarehouseDish(warehouseDishIrishFish);*/

            //======================================================== warehouse ingredient
            var warehouseIngredientPotatoes = new WarehouseIngredient
            {
                Ingredient = ingredientPotatoes,
                Quantity = 0
            };

            var warehouseIngredientTuna = new WarehouseIngredient
            {
                Ingredient = ingredientTuna,
                Quantity = 0
            };

            if (IsEnoughWarehouseIngredient)
            {
                warehouseIngredientPotatoes.Quantity = 5;
                warehouseIngredientTuna.Quantity = 5;
            }

            manager.AddWarehouseIngredient(warehouseIngredientPotatoes);
            manager.AddWarehouseIngredient(warehouseIngredientTuna);

            //======================================================== customer
            Customer customerBernard = new Customer
            {
                FullName = "Bernard Unfortunate",
                Budget = 30
            };

            if (IsAllergy)
            {
                customerBernard.Allergies = new List<Ingredient> { ingredientPotatoes };
            }

            if (isEnoughBudgetOfCustomer)
            {
                customerBernard.Budget = 60;
            }

            manager.AddCustomer(customerBernard);

            //======================================================== activity of buying
            if (isDiscount)
            {
                ActivityOfBuying activityOfBuying = new ActivityOfBuying()
                {
                    Customer = customerBernard,
                    Times = 2
                };

                manager.AddActivityOfBuying(activityOfBuying);
            }
        }
    }
}
