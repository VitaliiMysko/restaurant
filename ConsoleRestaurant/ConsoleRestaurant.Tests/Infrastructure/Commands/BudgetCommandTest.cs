﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Infrastructure.Commands;
using NUnit.Framework;

namespace ConsoleRestaurant.Tests.Infrastructure
{
    public class BudgetCommandTest
    {
        [Test]
        public void Execute_CommandIsNotEnabledToUse_NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateBaseFakeDatabase(manager, false);

            var dataFakeArr = new string[] { "-", "1" };
            var command = new BudgetCommand(dataFakeArr, manager);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsTrue(report.Message.EndsWith(" disabled"));
        }

        [Test]
        public void Execute_PassedDataHasWrong_NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateBaseFakeDatabase(manager, true);

            var dataFakeArr = new string[] { "-" };
            var command = new BudgetCommand(dataFakeArr, manager);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsTrue(report.Message.EndsWith("Record is not correct"));
        }

        [Test]
        public void Execute_AmountIsNotCorrect_NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateBaseFakeDatabase(manager, true);

            var dataFakeArr = new string[] { "-", "12a" };
            var command = new BudgetCommand(dataFakeArr, manager);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsTrue(report.Message.EndsWith("is not correct"));
        }

        [TestCase("+", "30", true, 90)]
        [TestCase("+", "30", false, 75)]
        [TestCase("-", "30", true, 30)]
        [TestCase("-", "30", false, 15)]
        [TestCase("-", "200", true, -140)]
        [TestCase("-", "200", false, -240)]
        [TestCase("=", "30", true, 30)]
        [TestCase("Profit", "30", true, 54)]
        [TestCase("Tip", "30", true, 87)]
        public void Execute_MakeBudgetNew_PositiveResult(string sign, string amountStr, bool noPaxTax, double expectedBudget)
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true);

            var dataFakeArr = new string[] { sign, amountStr };
            var command = new BudgetCommand(dataFakeArr, manager, noPaxTax);

            //when
            var report = command.Execute();

            //then
            var restaurantNew = manager.GetRestaurant(AppConfig.Settings.MainRestaurantName);

            Assert.IsTrue(report.CommandSucceeded);
            Assert.AreEqual(expectedBudget, restaurantNew.Budget);
        }


        [TestCase("Environmental fine", "3000", true, 70)]
        public void Execute_MakeBudgetNewAfterEnvironmentalFine_PositiveResult(string sign, string amountStr, bool noPaxTax, double expectedBudget)
        {
            //given
            var manager = new ManagetRepositoryMock();

            CommandConfig commandConfig = new CommandConfig()
            {
                Budget = true
            };

            TimeCostConfig timeCostConfig = new TimeCostConfig()
            {
                BudgetCost = 250
            };

            TaxConfig taxConfig = new TaxConfig()
            {
                WasteTax = 15
            };

            AppConfig.Settings = new Appsetting()
            {
                Command = commandConfig,
                Tax = taxConfig,
                TimeCost = timeCostConfig,
                MainRestaurantName = "Gurman",
            };

            Restaurant restaurant = new Restaurant
            {
                Name = "Gurman",
                Budget = 600
            };

            manager.AddRestaurant(restaurant);


            var dataFakeArr = new string[] { sign, amountStr };
            var command = new BudgetCommand(dataFakeArr, manager, noPaxTax);

            //when
            var report = command.Execute();

            //then
            var restaurantNew = manager.GetRestaurant(AppConfig.Settings.MainRestaurantName);

            Assert.IsTrue(report.CommandSucceeded);
            Assert.AreEqual(expectedBudget, restaurantNew.Budget);
        }

        [Test]
        public void Execute_MakeUnknownActWithBudget_NegativeResult()
        {
            //given
            var sign = "&";

            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true);

            var dataFakeArr = new string[] { sign, "55" };
            var command = new BudgetCommand(dataFakeArr, manager);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsTrue(report.Message.EndsWith($"Error! Budget is fail. Unknown sign \"{sign}\""));
        }

        [TestCase("=", "30", true, 30)]
        [TestCase("+", "200", true, 140)]
        public void Execute_RestaurantIsBankruptAndTryChangeIt_PositiveResult(string sign, string amountStr, bool noPaxTax, double expectedBudget)
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true, true);

            var dataFakeArr = new string[] { sign, amountStr };
            var command = new BudgetCommand(dataFakeArr, manager, noPaxTax);

            //when
            var report = command.Execute();

            //then
            var restaurantNew = manager.GetRestaurant(AppConfig.Settings.MainRestaurantName);

            Assert.IsTrue(report.CommandSucceeded);
            Assert.AreEqual(expectedBudget, restaurantNew.Budget);
        }

        [TestCase("-", "200", true)]
        [TestCase("-", "200", false)]
        [TestCase("+", "200", false)]
        public void Execute_RestaurantIsBankruptAndTryChangeIt_NegativeResult(string sign, string amountStr, bool noPaxTax)
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true, true);

            var dataFakeArr = new string[] { sign, amountStr };
            var command = new BudgetCommand(dataFakeArr, manager, noPaxTax);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsTrue(report.IsBankruptcy);
        }

        [TestCase("+", 30, true, 90)]
        [TestCase("+", 30, false, 75)]
        [TestCase("-", 30, true, 30)]
        [TestCase("-", 30, false, 15)]
        [TestCase("-", 200, true, -140)]
        [TestCase("-", 200, false, -240)]
        [TestCase("=", 30, true, 30)]
        [TestCase("Profit", "30", true, 54)]
        public void GetNewBudgetOfRestaurant_ChangeBudget_PositiveResult(string sign, double amount, bool isCallBudgetDirectly, double expectedBudget)
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateBaseFakeDatabase(manager, true);

            Restaurant restaurant = GetTestRestaurant();

            string[] dataFakeArr = { "-", "1" };
            var command = new BudgetCommand(dataFakeArr, manager, isCallBudgetDirectly);

            var action = command.GetBudgetAction(sign);

            //when
            var budgetNew = command.GetNewBudgetOfRestaurant(restaurant, action, amount);

            //then
            Assert.AreEqual(expectedBudget, budgetNew);
        }

        private Restaurant GetTestRestaurant(bool isBankrupt = false)
        {
            return new Restaurant
            {
                Name = AppConfig.Settings.MainRestaurantName,
                Budget = isBankrupt ? -60 : 60
            };
        }

        private void GenerateBaseFakeDatabase(ManagetRepositoryMock manager, bool isCommandEnabled)
        {
            //======================================================== config
            CommandConfig commandConfig = new CommandConfig()
            {
                Audit = true,
                Budget = isCommandEnabled,
                Buy = true,
                Order = true,
                Table = true
            };

            TaxConfig taxConfig = new TaxConfig()
            {
                DailyTax = 20,
                ProfitMargin = 40,
                TransactionTax = 50,
                TipsTax = 10
            };

            TimeCostConfig timeCostConfig = new TimeCostConfig();
            timeCostConfig.BudgetCost = 250;

            var appsetting = new Appsetting()
            {
                Command = commandConfig,
                Tax = taxConfig,
                TimeCost = timeCostConfig,
                MainRestaurantName = "Gurman"
            };

            AppConfig.Settings = appsetting;
        }

        private void GenerateFakeDatabase(ManagetRepositoryMock manager, bool isCommandEnabled, bool IsBankrupt = false)
        {
            //======================================================== config
            GenerateBaseFakeDatabase(manager, isCommandEnabled);

            //======================================================== restaurant
            Restaurant restaurant = GetTestRestaurant(IsBankrupt);
            manager.AddRestaurant(restaurant);
        }

    }
}
