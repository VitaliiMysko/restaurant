﻿using ConsoleRestaurant.Infrastructure.Commands;
using NUnit.Framework;
using ConsoleRestaurant.Application;
using System;

namespace ConsoleRestaurant.Tests.Infrastructure
{
    public class CommandBuilderTest
    {
        [TestCase("buy, Adam Smith, Irish Fish", "BuyCommand")]
        [TestCase("budget, +, 400", "BudgetCommand")]
        [TestCase("order, Tuna, 20", "OrderCommand")]
        [TestCase("table, Adam Smith, Christian Donnovan, Irish Fish, Irish Fish", "TableCommand")]
        [TestCase("audit, Resources", "AuditCommand")]
        public void Build_PassCommandByString_GetRightCommand(string userCommandStr, string expectedNameCommand)
        {
            //given
            var dataArr = Helper.ParseActionLine(userCommandStr);
            var commandBuilder = new CommandBuilder(dataArr);

            //when
            var command = commandBuilder.Build();

            //then
            Assert.IsTrue(command.GetType().Name == expectedNameCommand);
        }

        [Test]
        public void Build_PassWrongCommandByString_ExceptionThrown()
        {
            var userCommandStr = "exchange, USD, 1000, UAN";

            //given
            var dataArr = Helper.ParseActionLine(userCommandStr);
            var commandBuilder = new CommandBuilder(dataArr);

            //when

            //then
            Assert.Throws<Exception>(() => commandBuilder.Build());
        }

    }
}
