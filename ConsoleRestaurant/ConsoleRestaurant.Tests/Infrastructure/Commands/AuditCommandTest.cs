﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Infrastructure.Commands;
using ConsoleRestaurant.Infrastructure.Dataes;
using NUnit.Framework;
using System.Collections.Generic;

namespace ConsoleRestaurant.Tests.Infrastructure
{
    public class AuditCommandTest
    {
        [Test]
        public void Execute_CommandIsNotEnabledToUse_NegativeResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateBaseFakeDatabase(manager, false);

            string[] dataFakeArr = { "Audit" };
            var command = new AuditCommand(dataFakeArr, manager);

            //when
            var report = command.Execute();

            //then
            Assert.IsFalse(report.CommandSucceeded);
            Assert.IsTrue(report.Message.EndsWith(" disabled"));
        }

        [Test]
        public void Execute_GetInitialData_PositiveResult()
        {
            //given
            var manager = new ManagetRepositoryMock();
            GenerateFakeDatabase(manager, true);

            new Audit(manager).Init();

            string[] dataFakeArr = { "Audit" };
            var command = new AuditCommand(dataFakeArr, manager);
            //when
            var report = command.Execute();

            //then
            Assert.IsTrue(report.CommandSucceeded);

            Assert.IsTrue(report.Message.EndsWith("INIT \n\nWarehouse: Fries: 3, Potatoes: 5\nTip: 0 | Tip tax: 0\nBudget: 30\n\nSTART \n\n \nDishes sold: 0 \nAUDIT END"));
        }

        private void GenerateBaseFakeDatabase(ManagetRepositoryMock manager, bool isCommandEnabled)
        {
            Statistic.AuditStatistic = new AuditStatistic(manager);
            //======================================================== config
            CommandConfig commandConfig = new CommandConfig()
            {
                Audit = isCommandEnabled,
                Budget = true,
                Buy = true,
                Order = true,
                Table = true
            };

            TaxConfig taxConfig = new TaxConfig()
            {
                TipsTax = 0
            };

            var appsetting = new Appsetting()
            {
                Command = commandConfig,
                MainRestaurantName = "Gurman",
                PathToAuditFile = "AuditTest.txt",
                Tax = taxConfig
            };

            AppConfig.Settings = appsetting;
        }

        private void GenerateFakeDatabase(ManagetRepositoryMock manager, bool isCommandEnabled)
        {
            //======================================================== config
            GenerateBaseFakeDatabase(manager, isCommandEnabled);

            //======================================================== restaurant
            Restaurant restaurant = new Restaurant
            {
                Name = AppConfig.Settings.MainRestaurantName,
                Budget = 30
            };

            manager.AddRestaurant(restaurant);

            //======================================================== ingredient
            Ingredient ingredientPotatoes = new Ingredient
            {
                Name = "Potatoes",
                Price = 3
            };

            manager.AddIngredient(ingredientPotatoes);

            //======================================================== dish
            Dish dishFries = new Dish
            {
                Name = "Fries",
                Ingredients = new List<Ingredient>() { ingredientPotatoes },
            };

            manager.AddDish(dishFries);

            //======================================================== warehouse dish
            WarehouseDish warehouseDishFries = new WarehouseDish
            {
                Dish = dishFries,
                Quantity = 3
            };

            manager.AddWarehouseDish(warehouseDishFries);

            //======================================================== warehouse ingredient
            WarehouseIngredient warehouseIngredientPotatoes = new WarehouseIngredient
            {
                Ingredient = ingredientPotatoes,
                Quantity = 5
            };

            manager.AddWarehouseIngredient(warehouseIngredientPotatoes);
        }

    }
}
