﻿using ConsoleRestaurant.Domain.Interfaces;
using System.Collections.Generic;

namespace ConsoleRestaurant.Tests
{
    internal class FakeRnd : IRnd
    {
        public Queue<double> Values = new Queue<double>();
        public int Next()
        {
            if (Values.Count <= 0) return 0;

            return (int)Values.Dequeue();
        }

        public int Next(int min, int max)
        {
            if (Values.Count <= 0) return 0;

            return (int)Values.Dequeue();
        }

        public double NextDouble()
        {
            if (Values.Count <= 0) return 0;

            return Values.Dequeue();
        }
    }
}
