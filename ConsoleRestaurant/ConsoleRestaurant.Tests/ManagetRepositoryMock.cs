﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Infrastructure.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleRestaurant.Tests
{
    public class ManagetRepositoryMock : BaseManagerRepository
    {
        private List<Restaurant> _recordsRestaurant = new List<Restaurant>();
        private List<Customer> _recordsCustomer = new List<Customer>();
        private List<Ingredient> _recordsIngredient = new List<Ingredient>();
        private List<Dish> _recordsDish = new List<Dish>();
        private List<WarehouseIngredient> _recordsWarehouseIngredient = new List<WarehouseIngredient>();
        private List<WarehouseDish> _recordsWarehouseDish = new List<WarehouseDish>();
        private Appsetting _recordAppsetting = new Appsetting();
        private List<ActivityOfBuying> _recordActivityOfBuying = new List<ActivityOfBuying>();
        private List<WasteDish> _recordsWasteDish = new List<WasteDish>();
        private List<WasteIngredient> _recordsWasteIngredient = new List<WasteIngredient>();
        private List<WasteDish> _recordWastePoolDish = new List<WasteDish>();
        private List<WasteIngredient> _recordWastePoolIngredient = new List<WasteIngredient>();

        //======================================================== Add
        public override Customer AddCustomer(Customer entity)
        {
            _recordsCustomer.Add(entity);
            return entity;
        }

        public override Dish AddDish(Dish entity)
        {
            _recordsDish.Add(entity);
            return entity;
        }

        public override Ingredient AddIngredient(Ingredient entity)
        {
            _recordsIngredient.Add(entity);
            return entity;
        }

        public override Restaurant AddRestaurant(Restaurant entity)
        {
            _recordsRestaurant.Add(entity);
            return entity;
        }

        public override WarehouseDish AddWarehouseDish(WarehouseDish entity)
        {
            _recordsWarehouseDish.Add(entity);
            return entity;
        }

        public override WarehouseIngredient AddWarehouseIngredient(WarehouseIngredient entity)
        {
            _recordsWarehouseIngredient.Add(entity);
            return entity;
        }

        public Appsetting AddAppsetting(Appsetting entity)
        {
            _recordAppsetting = entity;
            return entity;
        }

        public override ActivityOfBuying AddActivityOfBuying(ActivityOfBuying entity)
        {
            _recordActivityOfBuying.Add(entity);
            return entity;
        }

        public override WasteDish AddWasteDish(WasteDish entity)
        {
            _recordsWasteDish.Add(entity);
            return entity;
        }

        public override WasteIngredient AddWasteIngredient(WasteIngredient entity)
        {
            _recordsWasteIngredient.Add(entity);
            return entity;
        }


        public override WasteDish AddWastePoolDish(WasteDish entity)
        {
            _recordWastePoolDish.Add(entity);
            return entity;
        }

        public override WasteIngredient AddWastePoolIngredient(WasteIngredient entity)
        {
            _recordWastePoolIngredient.Add(entity);
            return entity;
        }

        //======================================================== Delete
        public override Customer DeleteCustomer(string name)
        {
            var record = GetCustomer(name);
            _recordsCustomer.Remove(record);
            return record;
        }

        public override Dish DeleteDish(string name)
        {
            var record = GetDish(name);
            _recordsDish.Remove(record);
            return record;
        }

        public override Ingredient DeleteIngredient(string name)
        {
            var record = GetIngredient(name);
            _recordsIngredient.Remove(record);
            return record;
        }

        public override Restaurant DeleteRestaurant(string name)
        {
            var record = GetRestaurant(name);
            _recordsRestaurant.Remove(record);
            return record;
        }

        public override WarehouseDish DeleteWarehouseDish(string name)
        {
            var record = GetWarehouseDish(name);
            _recordsWarehouseDish.Remove(record);
            return record;
        }

        public override WarehouseIngredient DeleteWarehouseIngredient(string name)
        {
            var record = GetWarehouseIngredient(name);
            _recordsWarehouseIngredient.Remove(record);
            return record;
        }

        public override ActivityOfBuying DeleteActivityOfBuying(string name)
        {
            var record = GetActivityOfBuying(name);
            _recordActivityOfBuying.Remove(record);
            return record;
        }

        public override WasteDish DeleteWasteDish(string name)
        {
            var record = GetWasteDish(name);
            _recordsWasteDish.Remove(record);
            return record;
        }

        public override WasteIngredient DeleteWasteIngredient(string name)
        {
            var record = GetWasteIngredient(name);
            _recordsWasteIngredient.Remove(record);
            return record;
        }
        public override WasteDish DeleteWastePoolDish(string name)
        {
            var record = GetWastePoolDish(name);
            _recordWastePoolDish.Remove(record);
            return record;
        }

        public override WasteIngredient DeleteWastePoolIngredient(string name)
        {
            var record = GetWastePoolIngredient(name);
            _recordWastePoolIngredient.Remove(record);
            return record;
        }

        //======================================================== Get
        public override Customer GetCustomer(string name)
        {
            return _recordsCustomer.FirstOrDefault(x => x.FullName == name);
        }

        public override Dish GetDish(string name)
        {
            return _recordsDish.FirstOrDefault(x => x.Name == name);
        }

        public override Ingredient GetIngredient(string name)
        {
            return _recordsIngredient.FirstOrDefault(x => x.Name == name);
        }

        public override Restaurant GetRestaurant(string name)
        {
            return _recordsRestaurant.FirstOrDefault(x => x.Name == name);
        }

        public override WarehouseDish GetWarehouseDish(string name)
        {
            return _recordsWarehouseDish.FirstOrDefault(x => x.Dish.Name == name);
        }

        public override WarehouseIngredient GetWarehouseIngredient(string name)
        {
            return _recordsWarehouseIngredient.FirstOrDefault(x => x.Ingredient.Name == name);
        }

        public override Appsetting GetAppsetting()
        {
            return _recordAppsetting;
        }

        public override ActivityOfBuying GetActivityOfBuying(string name)
        {
            return _recordActivityOfBuying.FirstOrDefault(x => x.Customer.FullName == name);
        }

        public override WasteDish GetWasteDish(string name)
        {
            return _recordsWasteDish.FirstOrDefault(x => x.Dish.Name == name);
        }

        public override WasteIngredient GetWasteIngredient(string name)
        {
            return _recordsWasteIngredient.FirstOrDefault(x => x.Ingredient.Name == name);
        }

        public override WasteDish GetWastePoolDish(string name)
        {
            return _recordWastePoolDish.FirstOrDefault(x => x.Dish.Name == name);
        }

        public override WasteIngredient GetWastePoolIngredient(string name)
        {
            return _recordWastePoolIngredient.FirstOrDefault(x=>x.Ingredient.Name == name);
        }

        //======================================================== GetAll
        public override List<Customer> GetAllCustomers()
        {
            return _recordsCustomer;
        }

        public override List<Dish> GetAllDishes()
        {
            return _recordsDish;
        }

        public override List<Ingredient> GetAllIngredients()
        {
            return _recordsIngredient;
        }

        public override List<Restaurant> GetAllRestaurants()
        {
            return _recordsRestaurant;
        }

        public override List<WarehouseDish> GetAllWarehouseDishes()
        {
            return _recordsWarehouseDish;
        }

        public override List<WarehouseIngredient> GetAllWarehouseIngredients()
        {
            return _recordsWarehouseIngredient;
        }

        public override List<ActivityOfBuying> GetAllActivityOfBuying()
        {
            return _recordActivityOfBuying;
        }

        public override List<WasteDish> GetAllWasteDishes()
        {
            return _recordsWasteDish;
        }

        public override List<WasteIngredient> GetAllWasteIngredients()
        {
            return _recordsWasteIngredient;
        }

        public override List<WasteDish> GetAllWastePoolDishes()
        {
            return _recordWastePoolDish;
        }

        public override List<WasteIngredient> GetAllWastePoolIngredients()
        {
            return _recordWastePoolIngredient;
        }


        //======================================================== Update
        public override Customer UpdateCustomer(Customer entity)
        {
            var record = GetCustomer(entity.FullName);
            record.Budget = entity.Budget;
            return record;
        }

        public override Dish UpdateDish(Dish entity)
        {
            var record = GetDish(entity.Name);
            record.Dishes = entity.Dishes;
            record.Ingredients = entity.Ingredients;
            return record;
        }

        public override Ingredient UpdateIngredient(Ingredient entity)
        {
            var record = GetIngredient(entity.Name);
            record.Price = entity.Price;
            return record;
        }

        public override Restaurant UpdateRestaurant(Restaurant entity)
        {
            var record = GetRestaurant(entity.Name);
            record.Budget = entity.Budget;
            return record;
        }

        public override WarehouseDish UpdateWarehouseDish(WarehouseDish entity)
        {
            var record = GetWarehouseDish(entity.Dish.Name);
            record.Quantity = entity.Quantity;
            return record;
        }

        public override WarehouseIngredient UpdateWarehouseIngredient(WarehouseIngredient entity)
        {
            var record = GetWarehouseIngredient(entity.Ingredient.Name);
            record.Quantity = entity.Quantity;
            return record;
        }

        public override ActivityOfBuying UpdateActivityOfBuying(ActivityOfBuying entity)
        {
            var record = GetActivityOfBuying(entity.Customer.FullName);
            record.Times = entity.Times;
            return record;
        }

        public override WasteDish UpdateWasteDish(WasteDish entity)
        {
            var record = GetWasteDish(entity.Dish.Name);
            record.Quantity = entity.Quantity;
            return record;
        }

        public override WasteIngredient UpdateWasteIngredient(WasteIngredient entity)
        {
            var record = GetWasteIngredient(entity.Ingredient.Name);
            record.Quantity = entity.Quantity;
            return record;
        }

        public override WasteDish UpdateWastePoolDish(WasteDish entity)
        {
            var record = GetWastePoolDish(entity.Dish.Name);
            record.Quantity = entity.Quantity;
            return record;
        }

        public override WasteIngredient UpdateWastePoolIngredient(WasteIngredient entity)
        {
            var record = GetWastePoolIngredient(entity.Ingredient.Name);
            record.Quantity = entity.Quantity;
            return record;
        }
    }
}
