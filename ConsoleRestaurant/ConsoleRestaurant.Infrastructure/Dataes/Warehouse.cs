﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using ConsoleRestaurant.Infrastructure.Outputs;
using ConsoleRestaurant.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleRestaurant.Infrastructure.Dataes
{
    public class Warehouse
    {
        private List<DataToWarehouse> _dataToWarehouseList { get; set; } = new List<DataToWarehouse>();
        private bool _isTable { get; set; }
        private readonly IOutput _outputApp;

        public readonly Waste Waste;
        private readonly BaseManagerRepository _managerRepository;
        public Warehouse(BaseManagerRepository managerRepository)
        {
            _outputApp = new OutputApp();
            Waste = new Waste(managerRepository);
            _managerRepository = managerRepository;
        }

        public Warehouse()
        {
        }

        public int GetQuantity(Ingredient ingredient)
        {
            var record = _managerRepository.GetWarehouseIngredient(ingredient.Name);
            return record != null ? record.Quantity : 0;
        }

        public int GetQuantity(Dish dish)
        {
            var record = _managerRepository.GetWarehouseDish(dish.Name);
            return record != null ? record.Quantity : 0;
        }

        public bool IsEnough(Ingredient ingredient, int needQuantity)
        {
            var currentQuantity = GetQuantity(ingredient);
            return currentQuantity >= needQuantity;
        }

        public bool IsEnough(Dish dish, int needQuantity)
        {
            var currentQuantity = GetQuantity(dish);
            return currentQuantity >= needQuantity;
        }

        public void Put(Ingredient ingredient, int quantity)
        {
            var record = _managerRepository.GetWarehouseIngredient(ingredient.Name);

            if (record != null)
            {
                int newQuantityOnWarehouse = GetQuantityOfIngredientToPut(record.Quantity, quantity);
                var wasteQuantity = GetWasteQuantity(record.Quantity, quantity, newQuantityOnWarehouse);

                if (record.Quantity != newQuantityOnWarehouse)
                {
                    record.Quantity = newQuantityOnWarehouse;
                    _managerRepository.UpdateWarehouseIngredient(record);
                }

                if (wasteQuantity > 0)
                {
                    Waste.Put(ingredient, wasteQuantity);
                }
            }
            else
            {
                var recordNew = new WarehouseIngredient()
                {
                    Ingredient = ingredient
                };

                int newQuantityOnWarehouse = GetQuantityOfIngredientToPut(recordNew.Quantity, quantity);
                var wasteQuantity = GetWasteQuantity(0, quantity, newQuantityOnWarehouse);

                recordNew.Quantity = newQuantityOnWarehouse;

                _managerRepository.AddWarehouseIngredient(recordNew);

                if (wasteQuantity > 0)
                {
                    Waste.Put(ingredient, wasteQuantity);
                }
            }

        }

        public void Put(Dish dish, int quantity)
        {
            var record = _managerRepository.GetWarehouseDish(dish.Name);

            if (record != null)
            {
                int newQuantityOnWarehouse = GetQuantityOfDishToPut(record.Quantity, quantity);
                var wasteQuantity = GetWasteQuantity(record.Quantity, quantity, newQuantityOnWarehouse);

                if (record.Quantity != newQuantityOnWarehouse)
                {
                    record.Quantity = newQuantityOnWarehouse;
                    _managerRepository.UpdateWarehouseDish(record);
                }

                if (wasteQuantity > 0)
                {
                    Waste.Put(dish, wasteQuantity);
                }

            }
            else
            {
                var recordNew = new WarehouseDish()
                {
                    Dish = dish
                };

                int newQuantityOnWarehouse = GetQuantityOfDishToPut(recordNew.Quantity, quantity);
                var wasteQuantity = GetWasteQuantity(recordNew.Quantity, quantity, newQuantityOnWarehouse);

                recordNew.Quantity = newQuantityOnWarehouse;

                _managerRepository.AddWarehouseDish(recordNew);

                if (wasteQuantity > 0)
                {
                    Waste.Put(dish, wasteQuantity);
                }
            }

        }

        public void Put(List<DataToWarehouse> dataToWarehouseList)
        {
            _dataToWarehouseList = dataToWarehouseList;

            ControllerWarehouseAddGoods();
        }

        private int GetWasteQuantity(int currentQuantityOnWarehouse, int needToAddQuantity, int newQuantityOnWarehouse)
        {
            return currentQuantityOnWarehouse + needToAddQuantity - newQuantityOnWarehouse;
        }

        private int GetQuantityOfIngredientToPut(int currentQuantityOnWarehouse, int needToAddQuantity)
        {
            needToAddQuantity = GetCorrectedQuantityMakeCheckingTotal(needToAddQuantity);
            return currentQuantityOnWarehouse + needToAddQuantity > AppConfig.Settings.Warehouse.MaxIngredientType ? AppConfig.Settings.Warehouse.MaxIngredientType : currentQuantityOnWarehouse + needToAddQuantity;
        }

        private int GetQuantityOfDishToPut(int currentQuantityOnWarehouse, int needToAddQuantity)
        {
            needToAddQuantity = GetCorrectedQuantityMakeCheckingTotal(needToAddQuantity);
            return currentQuantityOnWarehouse + needToAddQuantity > AppConfig.Settings.Warehouse.MaxDishType ? AppConfig.Settings.Warehouse.MaxDishType : currentQuantityOnWarehouse + needToAddQuantity;
        }

        private int GetCorrectedQuantityMakeCheckingTotal(int needToAddQuantity)
        {
            var totalQuantityOnWarehouse = GetTotal();
            return totalQuantityOnWarehouse + needToAddQuantity > AppConfig.Settings.Warehouse.TotalMaximum ? AppConfig.Settings.Warehouse.TotalMaximum - totalQuantityOnWarehouse : needToAddQuantity;
        }

        private void PickUpGoods(Ingredient ingredient, int quantity)
        {
            var record = _managerRepository.GetWarehouseIngredient(ingredient.Name);

            if (record == null)
            {
                return;
            }

            record.Quantity -= quantity;

            if (record.Quantity > 0)
            {
                _managerRepository.UpdateWarehouseIngredient(record);
            }
            else
            {
                _managerRepository.DeleteWarehouseIngredient(ingredient.Name);
            }
        }

        public void Take(Ingredient ingredient, int quantity)
        {
            var isEnough = IsEnough(ingredient, quantity);

            if (!isEnough)
            {
                throw new Exception($"Error! The ingredient \"{ingredient.Name}\" is not on the warehouse");
            }

            PickUpGoods(ingredient, quantity);
        }

        private void PickUpGoods(Dish dish, int quantity)
        {
            var record = _managerRepository.GetWarehouseDish(dish.Name);

            if (record == null)
            {
                return;
            }

            record.Quantity -= quantity;

            if (record.Quantity > 0)
            {
                _managerRepository.UpdateWarehouseDish(record);
            }
            else
            {
                _managerRepository.DeleteWarehouseDish(dish.Name);
            }
        }

        private void ControllerWarehouseForPickUpGoods()
        {
            foreach (var dataToWarehouse in _dataToWarehouseList)
            {
                if (dataToWarehouse.Dish != null)
                {
                    PickUpGoods(dataToWarehouse.Dish, dataToWarehouse.QuantityThatNeed);
                }

                if (dataToWarehouse.Ingredient != null)
                {
                    PickUpGoods(dataToWarehouse.Ingredient, dataToWarehouse.QuantityThatNeed);
                }
            }
        }

        private void ControllerWarehouseAddGoods()
        {
            foreach (var dataToWarehouse in _dataToWarehouseList)
            {
                if (dataToWarehouse.Dish != null)
                {
                    Put(dataToWarehouse.Dish, dataToWarehouse.QuantityThatNeed);
                }

                if (dataToWarehouse.Ingredient != null)
                {
                    Put(dataToWarehouse.Ingredient, dataToWarehouse.QuantityThatNeed);
                }
            }
        }

        public void Take(Dish dish)
        {
            //quantity by default equals 1
            var checkWarehouseOk = CheckWarehouse(dish);

            if (!checkWarehouseOk)
            {
                throw new Exception($"Error! The dish \"{dish.Name}\" cannot take from, because one or more ingredients are not on the warehouse");
            }

            ControllerWarehouseForPickUpGoods();
        }

        public void Take(List<Dish> dishes)
        {
            //quantity by default equals 1
            _dataToWarehouseList.Clear();

            var checkWarehouseOk = CheckWarehouse(dishes);

            if (!checkWarehouseOk)
            {
                throw new Exception($"Error! The dishes cannot take from, because one or more ingredients are not on the warehouse");
            }

            ControllerWarehouseForPickUpGoods();
        }

        private bool HasErrorToTakeData()
        {
            var isError = false;
            foreach (var dataToWarehouse in _dataToWarehouseList)
            {
                if (dataToWarehouse.IsError)
                {
                    isError = true;
                    _outputApp.Print($"Error! Not enough \"{dataToWarehouse.Ingredient.Name}\" on the warehouse (have {dataToWarehouse.QuantityInWarehouse} need {dataToWarehouse.QuantityThatNeed})");
                }
            }

            return isError;
        }

        public bool CheckWarehouse(List<Dish> dishes)
        {
            _isTable = true;
            _dataToWarehouseList.Clear();

            GetAllComponentsOfDishFromWarehouse(dishes);

            var isError = HasErrorToTakeData();
            return !isError;
        }

        public bool CheckWarehouse(Dish dish)
        {
            var isError = false;

            if (!_isTable)
            {
                _dataToWarehouseList.Clear();
            }

            GetAllComponentsOfDishFromWarehouse(dish);

            if (!_isTable)
            {
                isError = HasErrorToTakeData();
            }

            return !isError;
        }

        public List<DataToWarehouse> GetAllComponentsOfDishFromWarehouse(List<Dish> dishes)
        {
            foreach (var dish in dishes)
            {
                ParseDishToComponents(dish);
            }

            return _dataToWarehouseList;
        }

        public List<DataToWarehouse> GetAllComponentsOfDishFromWarehouse(Dish dish)
        {
            ParseDishToComponents(dish);
            return _dataToWarehouseList;
        }

        private void ParseDishToComponents(Dish dish)
        {
            var result = InputDataToWarehouse(dish);

            if (result)
            {
                return;
            }

            if (dish.Ingredients != null)
            {
                foreach (var ingredient in dish.Ingredients)
                {
                    InputDataToWarehouse(ingredient);
                }
            };

            if (dish.Dishes != null)
            {
                foreach (var subdish in dish.Dishes)
                {
                    ParseDishToComponents(subdish);
                }
            }
        }

        private bool InputDataToWarehouse(Dish dish)
        {
            var wDish = _dataToWarehouseList.Find(item => item.Dish.Name == dish.Name);

            if (wDish != null)
            {
                if (wDish.QuantityInWarehouse == wDish.QuantityThatNeed)
                {
                    return false;
                }
                wDish.QuantityThatNeed += 1;
                return true;
            }

            var recordD = _managerRepository.GetWarehouseDish(dish.Name);

            if (recordD != null)
            {
                var data = new DataToWarehouse()
                {
                    Dish = recordD.Dish,
                    QuantityInWarehouse = recordD.Quantity,
                    QuantityThatNeed = 1
                };
                _dataToWarehouseList.Add(data);
                return true;
            }

            return false;
        }

        private bool InputDataToWarehouse(Ingredient ingredient)
        {
            var wIngredient = _dataToWarehouseList.Find(item => item.Ingredient.Name == ingredient.Name);

            if (wIngredient != null)
            {
                wIngredient.QuantityThatNeed += 1;
                return true;
            }

            var data = new DataToWarehouse()
            {
                Ingredient = ingredient,
                QuantityInWarehouse = 0,
                QuantityThatNeed = 1
            };

            var recordI = _managerRepository.GetWarehouseIngredient(ingredient.Name);

            if (recordI != null)
            {
                data.QuantityInWarehouse = recordI.Quantity;
            }
            //else 
            //{
            //    return false;
            //}

            _dataToWarehouseList.Add(data);
            return true;
        }

        public string GetAudit(bool ShowDishes = true, bool ShowIngredients = true)
        {
            string auditSrt = "";

            if (ShowDishes)
            {
                var wDishes = _managerRepository.GetAllWarehouseDishes();

                foreach (var wDish in wDishes)
                {
                    var currentAuditStr = $"{wDish.Dish.Name}: {wDish.Quantity}";
                    auditSrt += string.IsNullOrEmpty(auditSrt) ? currentAuditStr : ", " + currentAuditStr;
                }
            }

            if (ShowIngredients)
            {
                var wIngredients = _managerRepository.GetAllWarehouseIngredients();

                foreach (var wIngredient in wIngredients)
                {
                    var currentAuditStr = $"{wIngredient.Ingredient.Name}: {wIngredient.Quantity}";
                    auditSrt += string.IsNullOrEmpty(auditSrt) ? currentAuditStr : ", " + currentAuditStr;
                }
            }

            return auditSrt;
        }

        public int GetTotal()
        {
            return GetTotalIngredients() + GetTotalDides();
        }

        public int GetTotalIngredients()
        {
            var list = _managerRepository.GetAllWarehouseIngredients();

            var total = 0;

            foreach (var item in list)
            {
                total += item.Quantity;
            }

            return total;
        }

        public int GetTotalDides()
        {
            var list = _managerRepository.GetAllWarehouseDishes();

            var total = 0;

            foreach (var item in list)
            {
                total += item.Quantity;
            }

            return total;
        }


        public List<Dish> GetAllRecommendDishes(List<Ingredient> ingredients, Customer customer)
        {
            List<Dish> dishesRecommend = new List<Dish>();

            //Get all dish contain recomend ingredient
            dishesRecommend = _managerRepository.GetAllDishes().Where(d => ingredients.All(i => d.Contains(i.Name))).ToList();

            //Remove all dish with allergic ingredient
            dishesRecommend = dishesRecommend.Where(d => new Allergy(_managerRepository).FindCustomerAllergiesInDish(customer, d).Length == 0).ToList();

            //Remove all dish with too high price
            dishesRecommend = dishesRecommend.Where(d => customer.Budget >= d.GetPrice()).ToList();

            //Remove all dishes that we can not cook and which are not in warehouse
            dishesRecommend = dishesRecommend.Where(d => new Warehouse(_managerRepository).CheckWarehouse(d) || (_managerRepository.GetWarehouseDish(d.Name) != null && _managerRepository.GetWarehouseDish(d.Name).Quantity > 0)).ToList();

            return dishesRecommend;
        }
        public Dish GetRecommendDish(List<Ingredient> ingredients, Customer customer)
        {
            //Get dish with the highest price
            var dish = GetAllRecommendDishes(ingredients, customer).OrderByDescending(d => d.GetPrice()).FirstOrDefault();

            return dish;
        }
    }
}
