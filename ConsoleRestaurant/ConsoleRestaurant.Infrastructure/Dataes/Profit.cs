﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Infrastructure.Repositories;
using System;

namespace ConsoleRestaurant.Infrastructure.Dataes
{
    public class Profit
    {
        private readonly string _restaurantName;
        private double _initialBudget;
        private double _endBudget;

        public double InitialBudget { get => _initialBudget; }
        public double EndBudget { get => _endBudget; }

        public Profit(string restaurantName = "")
        {
            restaurantName = String.IsNullOrEmpty(restaurantName) ? AppConfig.Settings.MainRestaurantName : restaurantName;
            _restaurantName = restaurantName;
        }

        public void FixedInitialBudget(BaseManagerRepository managerRepository)
        {
            _initialBudget = GetBudgetOfRestaurant(managerRepository);
        }

        private double GetBudgetOfRestaurant(BaseManagerRepository managerRepository)
        {
            return managerRepository.GetRestaurant(_restaurantName).Budget;
        }

        private double GetTipOfRestaurant(BaseManagerRepository managerRepository)
        {
            return managerRepository.GetRestaurant(_restaurantName).Tip;
        }

        public void FixedEndBudget(BaseManagerRepository managerRepository)
        {
            //endBudget has tip (without tip tax)
            _endBudget = GetBudgetOfRestaurant(managerRepository);
        }

        public void FixedEndBudgetWithoutTip(BaseManagerRepository managerRepository)
        {
            //endBudget has tip (without tip tax)
            FixedEndBudget(managerRepository);

            //tip include tip tax
            var tip = GetTipOfRestaurant(managerRepository);

            //endBudget no has tip
            _endBudget = Math.Round(_endBudget - tip + AppConfig.Settings.Tax.GetTipsTaxAmount(tip), 2);
        }


        public double GetProfit()
        {
            return Math.Round(EndBudget - InitialBudget, 2);
        }

    }
}
