﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.SetsApp;
using ConsoleRestaurant.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleRestaurant.Infrastructure.Dataes
{
    public class AuditStatistic
    {
        private readonly BaseManagerRepository _managerRepository;

        private readonly Dictionary<Ingredient, int> _startIngredients = new();
        private readonly Dictionary<Dish, int> _startDishes = new();
        private Dictionary<Dish, int> _recommendedDishes = new();
        private Dictionary<Dish, double> _profitDishes = new();
        private Dictionary<Dish, int> _previusDishes = new();
        private Dictionary<Ingredient, int> _usedIngredients = new();
        private int _dishesSold = 0;

        public AuditStatistic(BaseManagerRepository managerRepository)
        {
            _managerRepository = managerRepository;

            managerRepository.GetAllWarehouseIngredients().ForEach(i => _startIngredients.Add(i.Ingredient, i.Quantity));
            managerRepository.GetAllWarehouseDishes().ForEach(i => _startDishes.Add(i.Dish, i.Quantity));

            _previusDishes = _startDishes.ToDictionary(k => k.Key, v => v.Value);
        }

        public string GetStatistic()
        {
            string AllStatistic = "";
            if (_recommendedDishes.Count != 0)
            {
                var mostRecommendedDish = _recommendedDishes.OrderByDescending(i => i.Value).FirstOrDefault().Key;
                AllStatistic += $"\nMost Recomended dish: {mostRecommendedDish.Name}";
            }
            if (_profitDishes.Count != 0)
            {
                var mostProfitableDish = _profitDishes.OrderByDescending(i => i.Value).FirstOrDefault();

                var price = mostProfitableDish.Key.GetTotalPrice();
                var times = price != 0 ? Math.Round(mostProfitableDish.Value / price, 2) : 0;

                AllStatistic += $"\nMost Profitable dish: {mostProfitableDish.Key.Name}[{mostProfitableDish.Value}], {times}";
            }
            if (_usedIngredients.Count != 0)
            {
                var mostUsedIngredients = _usedIngredients.OrderByDescending(i => i.Value).Take(3);
                var usedIngredientsStatistic = "";

                foreach (var usedIngredient in mostUsedIngredients)
                {
                    if (string.IsNullOrEmpty(usedIngredientsStatistic))
                    {
                        usedIngredientsStatistic = $"{ usedIngredient.Key.Name}, { usedIngredient.Value}";
                    }
                    else
                    {
                        usedIngredientsStatistic += $"; { usedIngredient.Key.Name}, { usedIngredient.Value}";
                    }
                }

                AllStatistic += $"\nMost used ingredients: {usedIngredientsStatistic}";
            }
            AllStatistic += $"\nDishes sold: {_dishesSold}";
            return AllStatistic;
        }

        public void AddStatistic(Report report)
        {
            if (report.CommandSucceeded == false) return;

            if (report.Command == Command.Buy)
            {
                BuyReport buyReport = (BuyReport)report;

                if (buyReport.IsAllergy == true) return;

                var dish = buyReport.Dish;
                AddDishToProfitDishes(dish, buyReport);

                CalculateUsedIngredients(dish);

                if (buyReport.IsRecommend)
                {
                    AddDishToRecommendDishes(dish);
                }
                _dishesSold++;
            }

            if (report.Command == Command.Table)
            {
                for (int i = 0; i < (double)report.ExtraReports.Count / 2; i++)
                {
                    AddStatistic(report.ExtraReports[i]);
                }
            }
            SetPreviusDishes();
        }

        private void CalculateUsedIngredients(Dish dish)
        {
            if (IsDishUsedFromWarehouse(dish))
            {
                return;
            }

            if (dish.Dishes != null)
            {
                foreach (var dishRecipe in dish.Dishes)
                {
                    CalculateUsedIngredients(dishRecipe);
                }
            }

            if (dish.Ingredients != null)
            {
                foreach (var ingredient in dish.Ingredients)
                {
                    if (_usedIngredients.Keys.Any(i => i.Name == ingredient.Name))
                    {
                        var usedIngredient = _usedIngredients.Keys.Where(i => i.Name == ingredient.Name).First();
                        _usedIngredients[usedIngredient]++;
                    }
                    else
                    {
                        _usedIngredients.Add(ingredient, 1);
                    }
                }
            }
        }

        private bool IsDishUsedFromWarehouse(Dish dish)
        {
            if (_previusDishes.Any(d => d.Key.Name == dish.Name))
            {
                var dictDish = _previusDishes.Where(d => d.Key.Name == dish.Name).First().Key;
                if (_previusDishes[dictDish] > 0)
                {
                    if (_managerRepository.GetAllWarehouseDishes().Any(d => d.Dish.Name == dish.Name))
                    {
                        if (_managerRepository.GetWarehouseDish(dish.Name).Quantity < _previusDishes[dictDish])
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private void SetPreviusDishes()
        {
            _previusDishes = _managerRepository.GetAllWarehouseDishes().ToDictionary(i => i.Dish, j => j.Quantity);
        }

        private void AddDishToRecommendDishes(Dish dish)
        {
            if (_recommendedDishes.Keys.Any(i => i.Name == dish.Name))
            {
                _recommendedDishes[dish]++;
            }
            else
            {
                _recommendedDishes.Add(dish, 1);
            }
        }

        private void AddDishToProfitDishes(Dish dish, BuyReport buyReport)
        {
            if (_profitDishes.Keys.Any(i => i.Name == dish.Name))
            {
                _profitDishes[dish] += buyReport.PriceOfDish;
            }
            else
            {
                _profitDishes.Add(dish, buyReport.PriceOfDish);
            }
        }
    }
}
