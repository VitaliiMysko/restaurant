﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using ConsoleRestaurant.Infrastructure.Commands;
using ConsoleRestaurant.Infrastructure.Repositories;

namespace ConsoleRestaurant.Infrastructure.Dataes
{
    public class DoCommand
    {
        private readonly BaseManagerRepository _managerRepository;
        public DoCommand(BaseManagerRepository managerRepository)
        {
            _managerRepository = managerRepository;
        }

        private Report Run(ICommand command, bool isCheck)
        {
            if (isCheck)
            {
                return command.Check();
            }

            return command.Execute();
        }

        public Report DoBudget(string sign, double amount, bool isCheck = false, bool noPayTax = false)
        {
            string[] dataArr = { sign, amount.ToString() };
            var budgetCommand = new BudgetCommand(dataArr, _managerRepository, noPayTax);

            return Run(budgetCommand, isCheck);
        }

        public Report DoBuyCommand(string customerFullName, string dishName, bool isCheck = false, bool spoilOn = false, bool isTip = true)
        {
            string[] dataArr = { customerFullName, dishName };
            var buyCommand = new BuyCommand(dataArr, _managerRepository, spoilOn, isTip);

            return Run(buyCommand, isCheck);
        }

        public Report DoOrderCommand(string orderThingName,
            string quantity,
            bool isCheck = false,
            int ingredientVolatility = 10,
            int dishVolatility = 25)
        {
            string[] dataArr = { orderThingName, quantity };
            var orderCommand = new OrderCommand(dataArr, _managerRepository, ingredientVolatility, dishVolatility);

            return Run(orderCommand, isCheck);
        }

    }
}
