﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.SetsApp;
using ConsoleRestaurant.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleRestaurant.Infrastructure.Dataes
{
    public class Allergy
    {
        private readonly BaseManagerRepository _managerRepository;
        private DoCommand _doCommand;

        public Allergy()
        {
            _managerRepository = ManagerRepository.GetInstance();
            _doCommand = new DoCommand(_managerRepository);
        }

        public Allergy(BaseManagerRepository managerRepository)
        {
            _managerRepository = managerRepository;
            _doCommand = new DoCommand(managerRepository);
        }

        public string[] FindCustomerAllergiesInDish(Customer customer, Dish dish)
        {
            List<string> allergies = new List<string>();

            if (customer.Allergies != null)
            {
                foreach (var allergy in customer.Allergies)
                {
                    var dishHasAllargy = FindAllergy(dish, allergy);

                    if (dishHasAllargy)
                    {
                        allergies.Add(allergy.Name);
                    }
                }
            }

            return allergies.ToArray();
        }

        public bool FindAllergy(Dish dish, Ingredient allergy)
        {
            var dishHasAllargy = false;

            if (dish.Ingredients != null)
            {
                var allergyIngredient = dish.Ingredients.Where(item => item.Name == allergy.Name).FirstOrDefault();

                if (allergyIngredient != null)
                {
                    return true;
                };

            };

            if (dish.Dishes != null)
            {
                foreach (var subdish in dish.Dishes)
                {
                    dishHasAllargy = FindAllergy(subdish, allergy);
                }
            }

            return dishHasAllargy;
        }

        public Report GetReportAllergy(BuyReport report, string[] allergies)
        {
            var allergyState = AppConfig.Settings.Allergy.State;

            if (allergyState == AllergyState.Waste)
            {
                AllergyWaste(report, allergies);
            }
            else if (allergyState == AllergyState.Keep)
            {
                AllergyKeep(report, allergies);
            }
            else
            {
                //Limit
                if (report.PriceOfDish < AppConfig.Settings.Allergy.Limit)
                {
                    AllergyWaste(report, allergies);
                }
                else
                {
                    AllergyKeep(report, allergies);
                }
            }

            return report;
        }

        private void AllergyKeep(BuyReport report, string[] allergies)
        {
            var keepPrice = Math.Round(report.PriceOfDish * 0.25, 2);
            var budgetReport = _doCommand.DoBudget("-", keepPrice, true, true);

            if (!budgetReport.CommandSucceeded || budgetReport.IsBankruptcy)
            {
                AllergyWaste(report, allergies);
                return;
            }

            if (AppConfig.Settings.Warehouse.MaxDishType == 0)
            {
                AllergyWaste(report, allergies);
                return;
            }

            report.Update(budgetReport);

            report.PriceOfDish = keepPrice;
            report.TransactionTaxAmount = 0;
            report.AllergyBaseState = AllergyBaseState.Keep;
            report.AddMessage(GetMessageAllergies(report, allergies));
        }

        private void AllergyWaste(BuyReport report, string[] allergies)
        {
            report.CommandSucceeded = !report.IsBankruptcy;
            report.AllergyBaseState = AllergyBaseState.Waste;
            report.AddMessage(GetMessageAllergies(report, allergies));
        }

        private string GetMessageAllergies(BuyReport report, string[] allergies)
        {
            var allergiesSrt = string.Join(", ", allergies);
            return $"{report.Customer.FullName} [{report.BudgetOfCustomer}], {report.Dish.Name} [{report.PriceOfDish} (Tax: {report.TransactionTaxAmount})] => fail, can't buy, allergic to: {allergiesSrt}";
        }

        public Report DoAllergy(BuyReport report, Warehouse warehouse, Dish dish)
        {
            //we take "dish" from the warehouse
            //it's can be as exactly dish and as mixed dishes and ingredients that it compose of
            warehouse.Take(dish);

            if (report.AllergyBaseState == AllergyBaseState.Keep)
            {
                //we put dish directly NOT mixed dishes and ingredients that it compose of 
                warehouse.Put(dish, 1);
                _doCommand.DoBudget("-", report.PriceOfDish, false, true);
            }
            else
            {
                //Waste
                warehouse.Waste.Put(dish, 1);
            }

            return report;
        }

    }
}
