﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Infrastructure.Repositories;

namespace ConsoleRestaurant.Infrastructure.Dataes
{
    public class ActivityCustomer
    {
        private readonly BaseManagerRepository _managerRepository;
        public ActivityCustomer(BaseManagerRepository managerRepository)
        {
            _managerRepository = managerRepository;

        }

        public int GetActivity(Customer customer)
        {
            var activityOfBuying = _managerRepository.GetActivityOfBuying(customer.FullName);

            if (activityOfBuying == null)
            {
                return 0;
            }

            return activityOfBuying.Times;
        }

        public void ChangeActivityOfBuying(Customer customer)
        {
            var activityOfBuying = _managerRepository.GetActivityOfBuying(customer.FullName);

            if (activityOfBuying == null)
            {
                AddActivityOfBuying(customer);
            }
            else
            {
                UpdateActivityOfBuying(activityOfBuying);
            }
        }

        private ActivityOfBuying AddActivityOfBuying(Customer customer)
        {
            var activityOfBuyingNew = new ActivityOfBuying()
            {
                Customer = customer,
                Times = 1
            };

            _managerRepository.AddActivityOfBuying(activityOfBuyingNew);
            return activityOfBuyingNew;
        }

        private ActivityOfBuying UpdateActivityOfBuying(ActivityOfBuying activityOfBuying)
        {
            activityOfBuying.Times += 1;
            _managerRepository.UpdateActivityOfBuying(activityOfBuying);
            return activityOfBuying;
        }


    }
}
