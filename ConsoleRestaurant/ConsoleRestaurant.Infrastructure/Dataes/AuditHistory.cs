﻿using ConsoleRestaurant.Domain.Entities;
using System.IO;

namespace ConsoleRestaurant.Infrastructure.Dataes
{
    public class AuditHistory
    {
        private readonly string _path;
        private string _history;

        private string _commandInfo;
        private string _warehouseInfo;
        private string _wasteInfo;
        private string _budgetInfo;

        private string _tipInfo;

        
        public AuditHistory()
        {
            _path = AppConfig.Settings.PathToAuditFile;
            _history = "";
        }

        public string GetHistory()
        {
            ReadFile();
            return _history;
        }

        private string GetCurrentInfo()
        {
            var reportAudit = "";

            if (string.IsNullOrEmpty(_history))
            {
                reportAudit += "\nINIT \n\n";
                reportAudit += GetStatisticalData();
                reportAudit += "START \n\n";
            }
            else
            {
                reportAudit += _commandInfo;
                reportAudit += GetStatisticalData();
            }

            return reportAudit;
        }

        private string GetStatisticalData()
        {
            return _warehouseInfo + _wasteInfo + _tipInfo + _budgetInfo;
        }

        public void AddWarehouseInfo(string info)
        {
            _warehouseInfo = "Warehouse: " + info + "\n";
        }

        public void AddWasteInfo(string info)
        {
            _wasteInfo = info != string.Empty ? "Waste: " + info + "\n" : "";
        }

        public void AddBudgetInfo(string info)
        {
            _budgetInfo = "Budget: " + info + "\n\n";
        }

        public void AddTipInfo(string tipInfo, string tipTaxInfo)
        {
            _tipInfo = "Tip: " + tipInfo + " | Tip tax: " + tipTaxInfo + "\n";
        }

        public void AddCommandInfo(string info)
        {
            _commandInfo = "Command: " + info + "\n";
        }

        public void Save()
        {
            ReadFile();

            _history += GetCurrentInfo();
            WriteFile(_history);
        }

        public void Delete()
        {
            WriteFile("");
        }

        private void ReadFile()
        {
            if (_history != string.Empty)
            {
                return;
            }

            _history = File.ReadAllText(_path);
        }

        private void WriteFile(string data)
        {
            File.WriteAllText(_path, data);
        }
    }
}
