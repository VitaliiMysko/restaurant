﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleRestaurant.Infrastructure.Dataes
{
    public class Chart
    {
        private static Chart _instance;

        private Dictionary<string, List<double>> _ingredientCountHistory { get; set; } = new Dictionary<string, List<double>>();
        private List<string> _allIngredients { get; set; } = new List<string>();

        private readonly BaseManagerRepository _managerRepository;

        public static Chart GetInstance()
        {
            if (_instance == null)
                _instance = new Chart();
            return _instance;
        }

        private Chart()
        {
            _managerRepository = ManagerRepository.GetInstance();
        }

        public void Init()
        {
            _allIngredients = _managerRepository.GetAllIngredients().Select(i => i.Name).ToList();
            _allIngredients.ForEach(i => _ingredientCountHistory.Add(i, new List<double>()));
            MadeScreenshotWarehouse();
        }

        public void MadeScreenshotWarehouse()
        {
            List<string> ingredientPresentOnWarehouse = new List<string>();

            var allWarehouseIngredients = _managerRepository.GetAllWarehouseIngredients();

            allWarehouseIngredients.ForEach(i =>
            {
                ingredientPresentOnWarehouse.Add(i.Ingredient.Name);
                _ingredientCountHistory[i.Ingredient.Name].Add(i.Quantity);
            });

            var ingredientAbsentOnWarehouse = _allIngredients.Except(ingredientPresentOnWarehouse);
            ingredientAbsentOnWarehouse.ToList().ForEach(i => _ingredientCountHistory[i].Add(0));
        }

        public void Show()
        {
            Dictionary<string, List<double>> ingredientsForCharting = GetIngredientsForCharting();

            ShowLinePlot(ingredientsForCharting);
            ShowBarGraph(ingredientsForCharting);
        }

        private void ShowLinePlot(Dictionary<string, List<double>> ingredientsForCharting)
        {
            if (!AppConfig.Settings.Chart.LinePlotChartEnable)
                return;

            var plt = new ScottPlot.Plot(1000, 600);

            var firstItem = ingredientsForCharting.FirstOrDefault();
            var countAct = firstItem.Value != null ? firstItem.Value.Count : 0;

            double[] dataX = new double[countAct];

            for (int i = 0; i < countAct; i++)
            {
                dataX[i] = i + 1;
            }

            foreach (var item in ingredientsForCharting)
            {
                double[] dataY = item.Value.ToArray();
                plt.AddScatter(dataX, dataY, null, 3, 10, ScottPlot.MarkerShape.asterisk, ScottPlot.LineStyle.Solid, item.Key);
            }

            // additional styling
            plt.Title("Chart changing basic ingredients in the warehouse");
            plt.XLabel("Acts");
            plt.YLabel("Quantity");

            plt.Legend();
            plt.SaveFig(AppConfig.Settings.Chart.PathToLinePlotChartFile);
        }

        private void ShowBarGraph(Dictionary<string, List<double>> ingredientsForCharting)
        {
            if (!AppConfig.Settings.Chart.BarGraphChartEnable)
                return;

            var plt = new ScottPlot.Plot(1000, 600);

            var firstItem = ingredientsForCharting.FirstOrDefault();
            var countAct = firstItem.Value != null ? firstItem.Value.Count : 0;

            // collect the data into groups
            string[] groupLabels = new string[countAct];
            string[] seriesLabels = new string[ingredientsForCharting.Count];
            double[][] barHeights = new double[ingredientsForCharting.Count][];
            double[][] barHeightsCoppy = new double[ingredientsForCharting.Count][];

            for (int i = 0; i < countAct; i++)
            {
                groupLabels[i] = (i + 1).ToString();
            }

            for (int i = 0; i < ingredientsForCharting.Count; i++)
            {
                var ingredientForCharting = ingredientsForCharting.ElementAt(i);
                seriesLabels[i] = ingredientForCharting.Key;
                barHeights[i] = ingredientForCharting.Value.ToArray();
            }

            //plt.PlotBarGroups(groupLabels, seriesLabels, barHeights);
            plt.AddBarGroups(groupLabels, seriesLabels, barHeights, barHeightsCoppy);

            // additional styling
            plt.Title("Chart changing basic ingredients in the warehouse");
            plt.XLabel("Acts");
            plt.YLabel("Quantity");

            plt.Legend();
            plt.SaveFig(AppConfig.Settings.Chart.PathToBarGraphChartFile);
        }

        private Dictionary<string, List<double>> GetIngredientsForCharting()
        {
            Dictionary<string, List<double>> ingredientsForCharting = new Dictionary<string, List<double>>();
            var chartVisibility = AppConfig.Settings.Chart.ChartVisibility;

            foreach (var item in _ingredientCountHistory)
            {
                var ingredient = item.Key;
                var quantities = item.Value;

                //every ingrediens has the sane quantity
                //go away if we have less then one quantity
                if (quantities.Count < 2)
                    break;

                int countChanges = GetCountChanges(quantities);

                var localChange = countChanges / (double)quantities.Count * 100;

                localChange = Math.Round(localChange, 2);

                if (localChange > chartVisibility)
                {
                    ingredientsForCharting.Add(ingredient, quantities);
                }
            }

            return ingredientsForCharting;
        }

        private int GetCountChanges(List<double> quantities)
        {
            int countChanges = 0;

            var iterations = quantities.Count() - 1;

            for (int i = 0; i < iterations; i++)
            {
                countChanges += quantities[i] != quantities[i + 1] ? 1 : 0;
            }

            return countChanges;
        }
    }
}
