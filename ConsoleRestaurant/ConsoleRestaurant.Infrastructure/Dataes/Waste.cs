﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using ConsoleRestaurant.Infrastructure.Outputs;
using ConsoleRestaurant.Infrastructure.Repositories;
using System;
using System.Linq;
namespace ConsoleRestaurant.Infrastructure.Dataes
{
    public class Waste
    {
        private readonly IOutput _outputApp;
        private readonly BaseManagerRepository _managerRepository;
        public Waste(BaseManagerRepository managerRepository)
        {
            _outputApp = new OutputApp();
            _managerRepository = managerRepository;
        }
        public void Clear()
        {
            ClearWasteIngredients();
            ClearWasteDishes();
        }
        public void SpoilIngredient(Ingredient ingredient)
        {
            var warehouseIngredient = _managerRepository.GetAllWarehouseIngredients().Find(wi => wi.Ingredient.Name == ingredient.Name);
            var wasteIngrerdient = _managerRepository.GetAllWasteIngredients().Find(wi => wi.Ingredient.Name == ingredient.Name);

            if (warehouseIngredient == null) return;
            if (wasteIngrerdient == null)
            {
                wasteIngrerdient = new WasteIngredient();
                wasteIngrerdient.Quantity = 0;
                wasteIngrerdient.Ingredient = warehouseIngredient.Ingredient;
            }
            int spoilAmount = CalculateSpoiling(ingredient);

            warehouseIngredient.Quantity -= spoilAmount;
            wasteIngrerdient.Quantity += spoilAmount;

            if (_managerRepository.GetAllWasteIngredients().Any(i => i.Ingredient.Name == wasteIngrerdient.Ingredient.Name))
            {
                _managerRepository.UpdateWasteIngredient(wasteIngrerdient);
            }
            else _managerRepository.AddWasteIngredient(wasteIngrerdient);

            _managerRepository.UpdateWarehouseIngredient(warehouseIngredient);
        }
        private int CalculateSpoiling(Ingredient ingredient)
        {
            var rand = new Random(DateTime.Now.Millisecond);
            var SpoilAmount = 0;
            for (int i = 0; i < _managerRepository.GetAllWarehouseIngredients().Find(i => i.Ingredient.Name == ingredient.Name).Quantity; i++)
            {
                if (rand.NextDouble() < AppConfig.Settings.Warehouse.SpoilRate / 100)
                {
                    SpoilAmount++;
                }
            }
            return SpoilAmount;
        }

        private void ClearWasteIngredients()
        {
            var allWasteIngredients = _managerRepository.GetAllWasteIngredients().Where(i => true).ToList();

            foreach (var wasteIngredient in allWasteIngredients)
            {
                _managerRepository.DeleteWasteIngredient(wasteIngredient.Ingredient.Name);
            }
        }

        private void ClearWasteDishes()
        {
            var allWasteDishes = _managerRepository.GetAllWasteDishes().Where(i => true).ToList();

            foreach (var wasteDish in allWasteDishes)
            {
                _managerRepository.DeleteWasteDish(wasteDish.Dish.Name);
            }
        }

        public int GetQuantity(Ingredient ingredient)
        {
            var record = _managerRepository.GetWasteIngredient(ingredient.Name);
            return record != null ? record.Quantity : 0;
        }

        public int GetQuantity(Dish dish)
        {
            var record = _managerRepository.GetWasteDish(dish.Name);
            return record != null ? record.Quantity : 0;
        }
        public bool IsPoisoning()
        {
            bool isPoisoning = GetTotalTrashAmount() > AppConfig.Settings.Warehouse.MaxTrash ? true : false;
            return isPoisoning;
        }

        private int GetTotalTrashAmount()
        {
            int sum = 0;
            _managerRepository.GetAllWasteIngredients().ForEach(i => sum += i.Quantity);
            _managerRepository.GetAllWasteDishes().ForEach(i => sum += i.Quantity);
            return sum;
        }

        public bool IsEnough(Ingredient ingredient, int needQuantity)
        {
            var currentQuantity = GetQuantity(ingredient);
            return currentQuantity >= needQuantity;
        }

        public bool IsEnough(Dish dish, int needQuantity)
        {
            var currentQuantity = GetQuantity(dish);
            return currentQuantity >= needQuantity;
        }

        public void Put(Ingredient ingredient, int quantity)
        {
            var record = _managerRepository.GetWasteIngredient(ingredient.Name);

            if (record != null)
            {
                record.Quantity += quantity;
                _managerRepository.UpdateWasteIngredient(record);
            }
            else
            {
                var wasteIngredient = new WasteIngredient()
                {
                    Ingredient = ingredient,
                    Quantity = quantity
                };
                _managerRepository.AddWasteIngredient(wasteIngredient);
            }

            ShowMessage(GetMessage(ingredient.Name, quantity));
        }

        public void SpoilForCooking(string dishName)
        {
            if (!_managerRepository.GetAllDishes().Any(d => d.Name == dishName))
            {
                return;
            }
            var dish = _managerRepository.GetDish(dishName);
            if (dish.Dishes != null)
            {
                foreach (var recipeDish in dish.Dishes)
                {
                    if (_managerRepository.GetWarehouseDish(recipeDish.Name) != null)
                    {
                        if (_managerRepository.GetWarehouseDish(recipeDish.Name).Quantity > 0)
                        {
                            continue;
                        }
                    }
                    SpoilForCooking(recipeDish.Name);
                }
            }
            SpoilIngredients();
        }

        public void SpoilIngredients()
        {
            foreach (var ingredient in _managerRepository.GetAllWarehouseIngredients())
            {
                SpoilIngredient(ingredient.Ingredient);
            }
        }

        public void Put(Dish dish, int quantity)
        {
            var record = _managerRepository.GetWasteDish(dish.Name);

            if (record != null)
            {
                record.Quantity += quantity;
                _managerRepository.UpdateWasteDish(record);
            }
            else
            {
                var wasteDish = new WasteDish()
                {
                    Dish = dish,
                    Quantity = quantity
                };
                _managerRepository.AddWasteDish(wasteDish);
            }

            ShowMessage(GetMessage(dish.Name, quantity));
        }

        private void PickUpGoods(Ingredient ingredient, int quantity)
        {
            var record = _managerRepository.GetWasteIngredient(ingredient.Name);

            if (record == null)
            {
                return;
            }

            record.Quantity -= quantity;

            if (record.Quantity > 0)
            {
                _managerRepository.UpdateWasteIngredient(record);
            }
            else
            {
                _managerRepository.DeleteWasteIngredient(ingredient.Name);
            }
        }

        public void Take(Ingredient ingredient, int quantity)
        {
            var isEnough = IsEnough(ingredient, quantity);

            if (!isEnough)
            {
                throw new Exception($"Error! The ingredient \"{ingredient.Name}\" is not on the warehouse");
            }

            PickUpGoods(ingredient, quantity);
        }

        private void PickUpGoods(Dish dish, int quantity)
        {
            var record = _managerRepository.GetWasteDish(dish.Name);

            if (record == null)
            {
                return;
            }

            record.Quantity -= quantity;

            if (record.Quantity > 0)
            {
                _managerRepository.UpdateWasteDish(record);
            }
            else
            {
                _managerRepository.DeleteWasteDish(dish.Name);
            }
        }


        public void Take(Dish dish, int quantity)
        {
            var isEnough = IsEnough(dish, quantity);

            if (!isEnough)
            {
                throw new Exception($"Error! The dish \"{dish.Name}\" cannot take from, because one or more ingredients are not on the wasted");
            }

            PickUpGoods(dish, quantity);
        }

        public string GetAudit(bool ShowDishes = true, bool ShowIngredients = true)
        {
            string auditStr = "";

            if (ShowDishes)
            {
                var wDishes = _managerRepository.GetAllWasteDishes();

                foreach (var wDish in wDishes)
                {
                    var currentAuditStr = $"{wDish.Dish.Name}: {wDish.Quantity}";
                    auditStr += string.IsNullOrEmpty(auditStr) ? currentAuditStr : ", " + currentAuditStr;
                }
            }

            if (ShowIngredients)
            {
                var wIngredients = _managerRepository.GetAllWasteIngredients();

                foreach (var wIngredient in wIngredients)
                {
                    var currentAuditStr = $"{wIngredient.Ingredient.Name}: {wIngredient.Quantity}";
                    auditStr += string.IsNullOrEmpty(auditStr) ? currentAuditStr : ", " + currentAuditStr;
                }
            }

            return auditStr;
        }

        private string GetMessage(string name, int quantity)
        {
            return $"Wasted: {name} {quantity}";
        }

        private void ShowMessage(string message)
        {
            _outputApp.Print(message);
        }

        public double GetTotalWasteAmount()
        {
            return Math.Round(GetTotalWasteDishesAmount() + GetTotalWasteIngredientsAmount(), 2);
        }

        public double GetTotalWasteIngredientsAmount()
        {
            var allWasteIngredients = _managerRepository.GetAllWasteIngredients().Where(i => i.Quantity > 0).ToList();
            return allWasteIngredients.Sum(i => i.Ingredient.Price * i.Quantity);
        }

        public double GetTotalWasteDishesAmount()
        {
            var allWasteDishes = _managerRepository.GetAllWasteDishes().Where(i => i.Quantity > 0).ToList();
            return allWasteDishes.Sum(i => i.Dish.GetPrice() * i.Quantity);
        }


    }
}
