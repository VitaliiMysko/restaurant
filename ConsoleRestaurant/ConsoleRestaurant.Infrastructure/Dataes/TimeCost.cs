﻿using ConsoleRestaurant.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRestaurant.Infrastructure.Dataes
{
    public static class TimeCost
    {
        public static double[] CalculatedBaseTime(string fullCommandEntered)
        {
            var timeUnits = GetTimeUnits(fullCommandEntered);
            var delta = Math.Ceiling(AppConfig.Settings.TimeCost.BaseTime * (double)timeUnits / 100);

            var timeCostMax = timeUnits + delta;
            var timeCostMin =  timeUnits - delta > 0 ? timeUnits - delta : 0;

            return new double[2]{ timeCostMin, timeCostMax};
        }

        private static int GetTimeUnits(string sentence)
        {
            return sentence.Count(c => "aeiou".Contains(Char.ToLower(c)));
        }

        private static double CalculatedExtaTime(Dish dish)
        {
            double extraTime = GetTimeUnits(dish.Name);

            if (dish.Dishes != null)
            {
                foreach (var item in dish.Dishes)
                {
                    extraTime += CalculatedExtaTime(item);
                }
            }

            if (dish.Ingredients != null)
            {
                foreach (var item in dish.Ingredients)
                {
                    extraTime += GetTimeUnits(item.Name);
                }
            }

            return extraTime;
        }

        public static double CalculatedTimeBudgetCommand()
        {
            return AppConfig.Settings.TimeCost.BudgetCost;
        }

        public static double[] CalculatedTimeBuyCommand(string fullCommandEntered, Dish dish)
        {
            var baseTime = CalculatedBaseTime(fullCommandEntered);

            double extraTime = Math.Round(CalculatedExtaTime(dish));

            baseTime[0] += extraTime;
            baseTime[1] += extraTime;

            return baseTime;
        }


        public static double CalculatedTimeOrderCommand(Ingredient ingredient)
        {
            return 10 * 1 * 1.05 + 1000;
        }

        public static double CalculatedTimeOrderCommand(Dish dish)
        {
            return 10 * 1 * 1.1 + 1000;
        }

        public static double[] CalculatedTimeTableCommand(string fullCommandEntered, List<Dish> dishes, List<Customer> customers)
        {
            var baseTime = CalculatedBaseTime(fullCommandEntered);

            double extraTime = 0;

            foreach (var dish in dishes)
            {
                extraTime += CalculatedExtaTime(dish);
            }

            extraTime = Math.Round(extraTime * 1.05 * customers.Count);

            baseTime[0] += extraTime;
            baseTime[1] += extraTime;

            return baseTime;
        }

        public static double CalculatedTimeAuditCommand()
        {
            return AppConfig.Settings.TimeCost.AuditCost;
        }

    }
}
