﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.SetsApp;
using ConsoleRestaurant.Infrastructure.Repositories;
using System;

namespace ConsoleRestaurant.Infrastructure.Dataes
{
    public class Audit
    {
        private readonly BaseManagerRepository _managerRepository;

        private AuditHistory _auditHistory;
        private AuditStatistic _auditStatistic;
        public Audit(BaseManagerRepository managerRepository)
        {
            _managerRepository = managerRepository;
            _auditHistory = new AuditHistory();
            _auditStatistic = Statistic.AuditStatistic;
        }

        public void Init()
        {
            Clear();
            AddStatisticalData();
            Save();
        }

        public string GetAudit()
        {
            return $"{_auditHistory.GetHistory()} {_auditStatistic.GetStatistic()} \nAUDIT END";
        }

        private void AddWarehouse()
        {
            _auditHistory.AddWarehouseInfo(new Warehouse(_managerRepository).GetAudit());
        }

        private void AddWaste()
        {
            _auditHistory.AddWasteInfo(new Waste(_managerRepository).GetAudit());
        }

        private void AddBudget()
        {
            _auditHistory.AddBudgetInfo(_managerRepository.GetRestaurant(AppConfig.Settings.MainRestaurantName).Budget.ToString());
        }

        private void AddTip()
        {
            var tip = _managerRepository.GetRestaurant(AppConfig.Settings.MainRestaurantName).Tip;
            var tipTax = Math.Round(AppConfig.Settings.Tax.GetTipsTaxAmount(tip), 2);

            _auditHistory.AddTipInfo(tip.ToString(), tipTax.ToString());
        }

        private void AddCommand(string info)
        {
            _auditHistory.AddCommandInfo(info);
        }

        public void SaveInfo(Report report)
        {
            if (report.Command == Command.Audit || report.Command == Command.Chart || !report.CommandSucceeded)
            {
                return;
            }

            AddCommand(report.Result);
            AddStatisticalData();
            _auditStatistic.AddStatistic(report);

            Save();
        }

        private void Save()
        {
            _auditHistory.Save();
        }

        public void Clear()
        {
            _auditHistory.Delete();
        }

        private void AddStatisticalData()
        {
            AddWarehouse();
            AddBudget();
            AddWaste();
            AddTip();
        }
    }
}
