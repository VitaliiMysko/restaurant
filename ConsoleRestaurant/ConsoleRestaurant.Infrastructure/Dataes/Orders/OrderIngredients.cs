﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using ConsoleRestaurant.Infrastructure.Repositories;
using ConsoleRestaurant.Domain.Rnd;
using System;

namespace ConsoleRestaurant.Infrastructure.Dataes
{
    public class OrderIngredients : IOrder
    {
        private readonly string _ingredientName;
        private int _quantity;
        private int _volatility;

        private Ingredient _ingredient;

        private readonly BaseManagerRepository _managerRepository;

        public IRnd rnd = new Rnd();
        public OrderIngredients(BaseManagerRepository managerRepository,
            string ingredientName,
            int quantity,
            int volatility = 10)
        {
            _managerRepository = managerRepository;
            _ingredientName = ingredientName;
            _quantity = quantity;
            _volatility = volatility;
        }

        private void SetData()
        {
            _ingredient = _managerRepository.GetIngredient(_ingredientName);
        }

        private bool IsCorrectData(OrderReport report)
        {
            var isCorrect = true;

            if (_ingredient == null)
            {
                report.AddMessage($"Error! Ingredient \"{_ingredient.Name}\" not found ");
                isCorrect = false;
            }

            return isCorrect;
        }

        public OrderReport Check(OrderReport report)
        {
            SetData();

            if (!IsCorrectData(report))
            {
                return report;
            }

            if (_ingredient.Price == 0)
            {
                report.AddMessage($"Attation. The price of {_ingredientName} = zero");
                return report;
            }

            report.Amount = Math.Round(_ingredient.Price * GetVolatility() * _quantity, 2);

            report.TimeCost = TimeCost.CalculatedTimeOrderCommand(_ingredient);

            return report;
        }

        private double GetVolatility()
        {
            var max = 1 + (double)_volatility / 100;
            var min = 1 - (double)_volatility / 100;
            var i = rnd.NextDouble() * (max - min) + min;
            return Math.Round(i, 2);
        }

        public void AddToWarehouse()
        {
            var warehouse = new Warehouse(_managerRepository);
            warehouse.Put(_ingredient, _quantity);
        }
    }
}
