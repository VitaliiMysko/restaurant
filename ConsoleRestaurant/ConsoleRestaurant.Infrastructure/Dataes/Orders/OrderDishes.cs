﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using ConsoleRestaurant.Infrastructure.Repositories;
using System;

namespace ConsoleRestaurant.Infrastructure.Dataes
{
    public class OrderDishes : IOrder
    {
        private readonly string _dishName;
        private int _quantity;
        private int _volatility;

        private Dish _dish;

        private readonly BaseManagerRepository _managerRepository;

        public OrderDishes(BaseManagerRepository managerRepository,
            string dishName,
            int quantity,
            int volatility = 25)
        {
            _managerRepository = managerRepository;
            _dishName = dishName;
            _quantity = quantity;
            _volatility = volatility;
        }

        private void SetData()
        {
            _dish = _managerRepository.GetDish(_dishName);
        }

        private bool IsCorrectData(OrderReport report)
        {
            var isCorrect = true;

            if (_dish == null)
            {
                report.AddMessage($"Error! Dish \"{_dish.Name}\" not found ");
                isCorrect = false;
            }

            return isCorrect;
        }

        public OrderReport Check(OrderReport report)
        {
            SetData();

            if (!IsCorrectData(report))
            {
                return report;
            }

            var price = _dish.GetPrice();

            if (price == 0)
            {
                report.AddMessage($"Attation. The price of {_dishName} = zero");
                return report;
            }

            report.Amount = Math.Round(price * GetVolatility() * _quantity, 2);

            report.TimeCost = TimeCost.CalculatedTimeOrderCommand(_dish);

            return report;
        }
        private double GetVolatility()
        {
            if (_volatility == 0) return 1;
            return (((new Random(DateTime.Now.Millisecond).Next() % _volatility) / 50) - _volatility / 100) + 1;
        }
        public void AddToWarehouse()
        {
            var warehouse = new Warehouse(_managerRepository);
            warehouse.Put(_dish, _quantity);
        }
    }

}
