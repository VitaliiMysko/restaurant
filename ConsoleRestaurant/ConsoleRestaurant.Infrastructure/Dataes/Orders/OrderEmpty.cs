﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;

namespace ConsoleRestaurant.Infrastructure.Dataes
{
    public class OrderEmpty : IOrder
    {
        public OrderReport Check(OrderReport report)
        {
            report.AddMessage($"This operation disabled!");
            return report;
        }

        public void AddToWarehouse()
        {
        }
    }

}
