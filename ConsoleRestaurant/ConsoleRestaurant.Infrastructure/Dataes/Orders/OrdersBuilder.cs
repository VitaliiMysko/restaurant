﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using ConsoleRestaurant.Domain.SetsApp;
using ConsoleRestaurant.Infrastructure.Repositories;

namespace ConsoleRestaurant.Infrastructure.Dataes
{
    public class OrdersBuilder
    {
        private readonly string _orderThingName;
        private int _quantity;
        private int _ingredientVolatility;
        private int _dishVolatility;

        private readonly BaseManagerRepository _managerRepository;

        public OrdersBuilder(BaseManagerRepository managerRepository,
            string orderThingName,
            int quantity,
            int ingredientVolatility,
            int dishVolatility)
        {
            _managerRepository = managerRepository;
            _orderThingName = orderThingName;
            _quantity = quantity;
            _ingredientVolatility = ingredientVolatility;
            _dishVolatility = dishVolatility;
        }

        public IOrder Build()
        {
            if (AppConfig.Settings.Order.State == OrderState.Ingredients)
            {
                return new OrderIngredients(_managerRepository, _orderThingName, _quantity, _ingredientVolatility);
            }
            else if (AppConfig.Settings.Order.State == OrderState.Dishes)
            {
                return new OrderDishes(_managerRepository, _orderThingName, _quantity, _dishVolatility);
            }
            else if (AppConfig.Settings.Order.State == OrderState.All)
            {
                if (_managerRepository.GetIngredient(_orderThingName) != null)
                {
                    return new OrderIngredients(_managerRepository, _orderThingName, _quantity, _ingredientVolatility);
                }

                return new OrderDishes(_managerRepository, _orderThingName, _quantity, _dishVolatility);
            }
            else
            {
                return new OrderEmpty();
            }
        }
    }
}
