﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using ConsoleRestaurant.Domain.SetsApp;
using ConsoleRestaurant.Infrastructure.Dataes;
using ConsoleRestaurant.Infrastructure.Repositories;
using System;

namespace ConsoleRestaurant.Infrastructure.Commands
{
    public class OrderCommand : ICommand
    {
        private readonly string[] _commandDataArr;
        private readonly string _commandEntered;
        private string _orderThingName;
        private int _quantity;
        private int _ingredientVolatility;
        private int _dishVolatility;

        private IOrder _order;

        private readonly BaseManagerRepository _managerRepository;

        public OrderCommand(
            string[] dataArr,
            BaseManagerRepository managerRepository,
            int ingredientVolatility = 10,
            int dishVolatility = 25)
        {
            _managerRepository = managerRepository;
            _commandDataArr = dataArr;
            _commandEntered = nameof(Command.Order) + ": " + String.Join(", ", _commandDataArr);
            _ingredientVolatility = ingredientVolatility;
            _dishVolatility = dishVolatility;
        }

        private bool DoValidation(Report report)
        {
            var nameCommand = report.Command.ToString();
            if (new Warehouse(_managerRepository).Waste.IsPoisoning())
            {
                report.AddMessage($"Error! Restaurant is poisoning");
                return false;
            }
            if (!AppConfig.Settings.Command.HasCommandEnable(nameCommand))
            {
                report.AddMessage($"Error! Command \"{nameCommand}\" disabled");
                return false;
            }

            if (_commandDataArr.Length < 2)
            {
                report.AddMessage("Error! Record is not correct");
                return false;
            }

            if (_commandDataArr.Length % 2 != 0)
            {
                report.AddMessage("Error! Record is not correct");
                return false;
            }

            if (_commandDataArr.Length == 2)
            {
                _orderThingName = _commandDataArr[0];

                try
                {
                    _quantity = int.Parse(_commandDataArr[1]);
                }
                catch (Exception)
                {
                    report.AddMessage($"Error: entered quantity \"{_commandDataArr[1]}\" is not correct");
                    return false;
                }
            }

            return true;
        }

        public Report Check()
        {
            var report = new OrderReport();
            report.Command = Command.Order;
            report.CommandEntered = _commandEntered;

            if (!DoValidation(report))
            {
                return report;
            }

            report.Multiple = _commandDataArr.Length > 2;

            if (report.Multiple)
            {
                if (!CheckAllOrderCommands(report))
                {
                    return report;
                }

                //additional condition - current budget enough apart for each order but not fact that it will working for all complexly and we check it
                var budgetReport1 = new DoCommand(_managerRepository).DoBudget("-", report.Amount, true);
                report.Update(budgetReport1);

                report.TimeCost = Math.Round(report.TimeCost - 1000 * (_commandDataArr.Length / 2 - 1));

                report.AddMessage(report.CommandSucceeded ? GetMessage(report) : "");
                return report;
            }

            _order = new OrdersBuilder(_managerRepository, _orderThingName, _quantity, _dishVolatility, _ingredientVolatility).Build();
            report = _order.Check(report);

            if (report.Amount == 0)
            {
                return report;
            }

            var budgetReport = new DoCommand(_managerRepository).DoBudget("-", report.Amount, true);
            report.Update(budgetReport);

            report.TimeCost = report.TimeCost;

            report.AddMessage(report.CommandSucceeded ? GetMessage(report) : "");
            return report;
        }

        public Report Execute()
        {
            var report = (OrderReport)Check();

            if (!report.CommandSucceeded)
            {
                return report;
            }

            return DoOrderCommand(report);
        }

        private Report DoOrderCommand(OrderReport report)
        {
            if (report.Multiple)
            {
                return RunAllOrderCommands(report);
            }

            new Warehouse(_managerRepository).Waste.SpoilIngredients();

            _order.AddToWarehouse();

            new DoCommand(_managerRepository).DoBudget("-", report.Amount);

            return report;
        }

        private bool CheckAllOrderCommands(OrderReport report)
        {
            var commandSucceeded = true;
            var isBankruptcy = false;

            var countOrder = (_commandDataArr.Length) / 2;

            for (int i = 0; i < countOrder; i++)
            {
                var currentOrderThingName = _commandDataArr[i * 2];
                var quantity = _commandDataArr[i * 2 + 1];

                var orderReport = (OrderReport)new DoCommand(_managerRepository).DoOrderCommand(currentOrderThingName, quantity, true, _ingredientVolatility, _dishVolatility);

                //we break cycle and return negative result if we get fault during executing the command
                if (!orderReport.CommandSucceeded)
                {
                    report.AddMessage(orderReport.Result);
                    return false;
                }

                //commandSucceeded = !orderReport.CommandSucceeded ? false : commandSucceeded;
                isBankruptcy = orderReport.IsBankruptcy ? orderReport.IsBankruptcy : isBankruptcy;
                report.Amount += orderReport.Amount;
                report.TimeCost += orderReport.TimeCost;
            }

            report.IsBankruptcy = isBankruptcy;
            report.Amount = Math.Round(report.Amount, 2);
            return commandSucceeded;
        }

        private Report RunAllOrderCommands(OrderReport report)
        {
            var countOrder = (_commandDataArr.Length) / 2;

            for (int i = 0; i < countOrder; i++)
            {
                var currentOrderThingName = _commandDataArr[i * 2];
                var quantity = _commandDataArr[i * 2 + 1];

                var orderReport = new DoCommand(_managerRepository).DoOrderCommand(currentOrderThingName, quantity, false, _ingredientVolatility, _dishVolatility);

                report.ExtraReports.Add(orderReport);
            }

            report.CommandSucceeded = true;

            return report;
        }

        private string GetMessage(OrderReport report)
        {
            return $"success (Tax: {GetTransactionTaxAmount(report.Amount)}) | time {report.TimeCost}";
        }

        private double GetTransactionTaxAmount(double amount)
        {
            return AppConfig.Settings.Tax.GetTransactionTaxAmount(amount);
        }

    }
}
