﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using ConsoleRestaurant.Domain.SetsApp;
using ConsoleRestaurant.Infrastructure.Dataes;
using ConsoleRestaurant.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleRestaurant.Infrastructure.Commands
{
    class CleanOffCommand : ICommand
    {
        private readonly string[] _commandDataArr;
        private readonly string _commandEntered;
        private double _wastePrice = 0;

        private readonly BaseManagerRepository _managerRepository;

        public CleanOffCommand(string[] dataArr,
            BaseManagerRepository managerRepository)
        {
            _managerRepository = managerRepository;
            _commandDataArr = dataArr;
            _commandEntered = nameof(Command.Cleanoff) + ": " + String.Join(", ", _commandDataArr);
        }

        private bool DoValidation(Report report)
        {
            var nameCommand = report.Command.ToString();

            if (!AppConfig.Settings.Command.HasCommandEnable(nameCommand))
            {
                report.AddMessage($"Error! Command \"{nameCommand}\" disabled");
                return false;
            }

            return true;
        }


        public Report Check()
        {
            var report = new Report();
            report.Command = Command.Cleanoff;
            report.CommandEntered = _commandEntered;

            if (!DoValidation(report))
            {
                return report;
            }

            report.CommandSucceeded = true;
            return report;
        }

        public Report Execute()
        {
            var report = Check();

            if (!report.CommandSucceeded)
            {
                return report;
            }

            return DoCleanOffCommand(report);

        }

        private Report DoCleanOffCommand(Report report)
        {
            List<WasteDish> wasteDishes = _managerRepository.GetAllWasteDishes().Where(i => true).ToList();
            List<WasteIngredient> wasteIngredients = _managerRepository.GetAllWasteIngredients().Where(i => true).ToList();

            wasteDishes.ForEach(i => _wastePrice += i.Dish.GetPrice() * i.Quantity);
            wasteIngredients.ForEach(i => _wastePrice += i.Ingredient.Price * i.Quantity);

            List<WasteDish> wastePoolDishes = _managerRepository.GetAllWastePoolDishes().Where(i => true).ToList();
            List<WasteIngredient> wastePoolIngredients = _managerRepository.GetAllWasteIngredients().Where(i => true).ToList();

            List<WasteDish> removeDishes = new List<WasteDish>();
            List<WasteIngredient> removeIngredients = new List<WasteIngredient>();

            foreach (var wasteDish in wasteDishes)
            {
                foreach (var wastePool in wastePoolDishes)
                {
                    if (wastePool.Dish.Name == wasteDish.Dish.Name)
                    {
                        wastePool.Quantity += wasteDish.Quantity;
                        removeDishes.Add(wasteDish);
                        break;
                    }
                }
            }
            foreach (var i in removeDishes)
            {
                wasteDishes.Remove(i);
            }
            wastePoolDishes.AddRange(wasteDishes);
            foreach (var wasteIngredient in wasteIngredients)
            {
                foreach (var wastePool in wastePoolIngredients)
                {
                    if (wastePool.Ingredient.Name == wasteIngredient.Ingredient.Name)
                    {
                        wastePool.Quantity += wasteIngredient.Quantity;
                        removeIngredients.Add(wasteIngredient);
                        break;
                    }
                }
            }
            foreach (var i in removeIngredients)
            {
                wasteIngredients.Remove(i);
            }
            wastePoolDishes.AddRange(wasteDishes);

            new Warehouse(_managerRepository).Waste.Clear();
            var allWasteIngredients = _managerRepository.GetAllWasteIngredients().Where(i => true).ToList();

            foreach (var wasteIngredient in allWasteIngredients)
            {
                _managerRepository.DeleteWasteIngredient(wasteIngredient.Ingredient.Name);
            }

            foreach (var i in _managerRepository.GetAllWastePoolDishes().Where(i => true).ToList())
            {
                _managerRepository.DeleteWastePoolDish(i.Dish.Name);
            }
            foreach (var i in _managerRepository.GetAllWastePoolIngredients().Where(i => true).ToList())
            {
                _managerRepository.DeleteWastePoolIngredient(i.Ingredient.Name);
            }

            foreach (var i in wastePoolDishes)
            {
                _managerRepository.AddWastePoolDish(i);
            }
            foreach (var i in wastePoolIngredients)
            {
                _managerRepository.AddWastePoolIngredient(i);
            }

            return report;
        }
    }
}
