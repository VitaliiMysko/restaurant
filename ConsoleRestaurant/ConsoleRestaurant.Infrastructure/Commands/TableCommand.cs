﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using ConsoleRestaurant.Domain.Rnd;
using ConsoleRestaurant.Domain.SetsApp;
using ConsoleRestaurant.Infrastructure.Dataes;
using ConsoleRestaurant.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleRestaurant.Infrastructure.Commands
{
    public class TableCommand : ICommand
    {
        private const string POOLED_ARG = "Pooled";

        private readonly string[] _commandDataArr;
        private readonly string _commandEntered;
        private int _countCustomersDishes;
        private bool _isPooled = false;
        private bool _isRecommend = false;
        private readonly BaseManagerRepository _managerRepository;

        public IRnd rnd = new Rnd();

        List<Customer> _customers = new List<Customer>();
        List<Dish> _dishes = new List<Dish>();

        //init budget of customers
        Dictionary<string, double> customersBudgetInit = new Dictionary<string, double>();
        //new budget of customers after pooling
        Dictionary<string, double> customersBudgetAfterPooled = new Dictionary<string, double>();
        //fix price of order for each customers
        Dictionary<string, double> customersPriceOrder = new Dictionary<string, double>();
        //here we keep value (budget of customers - price of order) for pooling
        Dictionary<string, double> customersBudgetPooled = new Dictionary<string, double>();
        //here we keep amount for buying tip if the flag "_isPooler" = true
        Dictionary<string, double> customersAmountForBuyingTip = new Dictionary<string, double>();

        private List<int> _indexOfRecommendDishes = new List<int>();

        public TableCommand(string[] dataArr,
            BaseManagerRepository managerRepository)
        {
            _managerRepository = managerRepository;
            _commandDataArr = dataArr.Where(i => i != POOLED_ARG).ToArray();
            _commandEntered = nameof(Command.Table) + ": " + String.Join(", ", _commandDataArr);
            _isRecommend = dataArr.Any(i => i == "Recommend");
            _isPooled = dataArr[0] == POOLED_ARG;
        }

        private bool DoValidation(Report report)
        {
            var nameCommand = report.Command.ToString();

            if (new Warehouse(_managerRepository).Waste.IsPoisoning())
            {
                report.AddMessage($"Error! Restaurant is poisoning");
                return false;
            }

            if (!AppConfig.Settings.Command.HasCommandEnable(nameCommand))
            {
                report.AddMessage($"Error! Command \"{nameCommand}\" disabled");
                return false;
            }

            if (_commandDataArr.Length <= 2)
            {
                report.AddMessage("Error! Table is fail. Record is not correct");
                return false;
            }

            if (_customers.Count != _dishes.Count)
            {
                report.AddMessage("Error! Table is fail. The numbers of customers do not equal the numbers of dishes");
                return false;
            }

            _countCustomersDishes = _dishes.Count;

            if (_isRecommend)
            {
                if (_dishes.Any(i => i == null || i.Name == "Recommend"))
                {
                    report.AddMessage("Error! Table is fail. Not recommened dish or not enough money in customers for doing this operation");
                    return false;
                }
            }

            string[] customersArr = new string[_countCustomersDishes];
            Array.Copy(_commandDataArr, 0, customersArr, 0, _countCustomersDishes);

            if (_customers.Count != _customers.Distinct().Count())
            {
                report.AddMessage("Error! Table is fail. One person can appear only once at the table");
                return false;
            }

            return true;
        }

        public Report Check()
        {
            var report = new BuyReport();
            report.Command = Command.Table;
            report.CommandEntered = _commandEntered;

            //Check a present all ingredients / dishes in "table"
            SetCustomers();
            SetDishes();

            if (!DoValidation(report))
            {
                return report;
            }

            if (!CheckAllBuyCommands(report))
            {

                if (_isPooled && !report.AreMoney && !report.IsBankruptcy)
                {
                    double totalCustomerBudget = customersBudgetInit.Values.Sum();
                    double totalCustomerPriceOrder = customersPriceOrder.Values.Sum();
                    if (totalCustomerBudget < totalCustomerPriceOrder)
                    {
                        report.AddMessage("Error! Table is fail. Not enough money in customers for doing this operation");
                        return report;
                    }

                    PooledAllBudget();
                    GetAmountForBuyingTip();
                    FixedPooledBudgedOfCostomers();

                    _isPooled = false;

                    //we spoiled before, we don't need spoil twice
                    if (!CheckAllBuyCommands(report, false))
                    {
                        FixedInitBudgedOfCostomers();
                        return report;
                    }

                    _isPooled = true;
                }
                else
                {
                    return report;
                }

            }
            else
            {
                _isPooled = false;
            }

            //above in CheckAllBuyCommands(report) we ran spoil and after that we check warehouse
            if (!CheckWarehouse(_dishes))
            {
                //we have to returt init budget of customers
                if (_isPooled)
                {
                    FixedInitBudgedOfCostomers();
                }

                report.AddMessage("Error! Table is fail. Not enough ingredients or dishes on the warehouse");
                return report;
            }

            var priceOfTable = Math.Round(report.PriceOfDish - report.TransactionTaxAmount, 2);

            //When customers have allergy then restaurant lost money.
            //Situation like this - current budget enough apart for each customer but not fact that it will working for all complexly and we check it
            if (priceOfTable < 0)
            {
                //We pass clean amount of "table", we don't pay tax for it
                var budgetReport = new DoCommand(_managerRepository).DoBudget("+", priceOfTable, true, true);
                report.Update(budgetReport);
            }
            else
            {
                report.CommandSucceeded = true;
            }

            var timeCostMaxMin = TimeCost.CalculatedTimeTableCommand(report.CommandEntered, _dishes, _customers);

            report.TimeCost = rnd.Next((int)timeCostMaxMin[0], (int)timeCostMaxMin[1] + 1);

            report.AddMessage($"success; money amount: {report.PriceOfDish} (Tax: {report.TransactionTaxAmount}) | time {report.TimeCost}");

            var countExtraReports = report.ExtraReports.Count;

            for (int i = 0; i < _customers.Count; i++)
            {
                report.AddMessage(report.ExtraReports[countExtraReports - _customers.Count + i].Message);
            }

            return report;
        }

        public Report Execute()
        {
            var report = (BuyReport)Check();

            if (!report.CommandSucceeded)
            {
                return report;
            }

            return DoTableCommand(report);
        }

        private bool CheckAllBuyCommands(BuyReport report, bool spoilOn = true)
        {
            var commandSucceeded = true;
            var isBankruptcy = false;
            var areMoney = true;
            report.PriceOfDish = 0;
            report.TransactionTaxAmount = 0;

            for (int i = 0; i < _countCustomersDishes; i++)
            {
                var customerFullName = _customers[i].FullName;
                var dishName = _dishes[i].Name;

                //Check particular "buy command"
                var buyReport = (BuyReport)new DoCommand(_managerRepository).DoBuyCommand(customerFullName, dishName, true, spoilOn, !_isPooled);
                buyReport.IsRecommend = _indexOfRecommendDishes.Any(index => index == i);

                commandSucceeded = !buyReport.CommandSucceeded ? false : commandSucceeded;
                isBankruptcy = buyReport.IsBankruptcy ? buyReport.IsBankruptcy : isBankruptcy;

                //this show us that one or more customer have not enough money for buying
                areMoney = !buyReport.AreMoney && !buyReport.IsAllergy ? false : areMoney;

                if (_isPooled)
                {
                    if (!buyReport.IsAllergy)
                    {
                        customersBudgetInit.Add(customerFullName, buyReport.BudgetOfCustomer);
                        customersBudgetAfterPooled.Add(customerFullName, buyReport.BudgetOfCustomer);
                        customersPriceOrder.Add(customerFullName, buyReport.PriceOfDish);
                        customersBudgetPooled.Add(customerFullName, Math.Round(buyReport.BudgetOfCustomer - buyReport.PriceOfDish, 2));
                    }
                    customersAmountForBuyingTip.Add(customerFullName, 0);
                }

                report.ExtraReports.Add(buyReport);
                report.PriceOfDish += !buyReport.IsAllergy ? buyReport.PriceOfDish : 0;
                report.PriceOfDish = Math.Round(report.PriceOfDish, 2);
                report.TransactionTaxAmount += buyReport.TransactionTaxAmount;
                report.TransactionTaxAmount = Math.Round(report.TransactionTaxAmount, 2);
            }

            report.IsBankruptcy = isBankruptcy;
            report.AreMoney = areMoney;
            return commandSucceeded;
        }

        private Report DoTableCommand(Report report)
        {
            for (int i = 0; i < _countCustomersDishes; i++)
            {
                var customerFullName = _customers[i].FullName;
                var dishName = _dishes[i].Name;
                BuyReport buyReport = (BuyReport)new DoCommand(_managerRepository).DoBuyCommand(customerFullName, dishName, false, false, !_isPooled);
                buyReport.IsRecommend = _indexOfRecommendDishes.Any(index => index == i);

                report.ExtraReports.Add(buyReport);

                if (_isPooled)
                {
                    var amountForTip = customersAmountForBuyingTip[customerFullName];

                    if (amountForTip > 0 && AppConfig.Settings.Tip.IsTipped())
                    {
                        var customer = _managerRepository.GetCustomer(customerFullName);
                        var tip = AppConfig.Settings.Tip.GetTipAmount(amountForTip);

                        //if (customersBudgetPooled[customerFullName] < tip)
                        if (customer.Budget < tip)
                        {
                            tip = customer.Budget;
                        }

                        if (tip > 0)
                        {
                            customer.ReduceBudget(tip);
                            _managerRepository.UpdateCustomer(customer);

                            new DoCommand(_managerRepository).DoBudget("Tip", tip);
                        }
                    }
                }
            }

            return report;
        }
        private void SetDishes()
        {
            for (int i = _countCustomersDishes; i < _commandDataArr.Length; i++)
            {
                var str = _commandDataArr[i];

                if (str == "Recommend")
                {
                    _dishes.Add(new Dish { Name = "Recommend" });
                    _indexOfRecommendDishes.Add(_dishes.Count - 1);
                }
                else if (_managerRepository.GetAllDishes().Any(d => d.Name == str))
                {
                    _dishes.Add(_managerRepository.GetDish(str));
                }
            }

            if (_dishes.Count != _customers.Count) return;

            SetRecommendDishes();
        }

        private void SetRecommendDishes()
        {
            var warehouse = new Warehouse(_managerRepository);
            Dictionary<Customer, Dish> customersOrder = new Dictionary<Customer, Dish>();
            Dictionary<Customer, List<Ingredient>> allRecommendIngredients = GetAllRecommendIngredients();

            //fill customersOrder (I just copy this from stack overflow)
            customersOrder = _customers.Zip(_dishes, (k, v) => new { Key = k, Value = v }).ToDictionary(x => x.Key, x => x.Value);


            if (_isPooled)
            {
                var dishes = GetBestPooledRecommendDishes(customersOrder);

                if (!object.Equals(dishes, null))
                {
                    //fill customersOrder with best dishes
                    customersOrder.Clear();
                    customersOrder = _customers.Zip(dishes, (k, v) => new { Key = k, Value = v }).ToDictionary(x => x.Key, x => x.Value);
                }
            }
            else
            {
                foreach (var item in customersOrder)
                {
                    if (item.Value.Name != "Recommend") continue;

                    customersOrder[item.Key] = warehouse.GetRecommendDish(allRecommendIngredients[item.Key], item.Key);
                }
            }

            _dishes = customersOrder.Values.ToList();
        }

        private List<Dish> GetBestPooledRecommendDishes(Dictionary<Customer, Dish> customersOrder)
        {
            var warehouse = new Warehouse(_managerRepository);

            Dictionary<Customer, List<Ingredient>> allRecommendIngredients = GetAllRecommendIngredients();
            Dictionary<Customer, List<Dish>> availableDishes = new Dictionary<Customer, List<Dish>>();
            //set all dishes
            foreach (var item in customersOrder)
            {
                if (item.Value.Name != "Recommend")
                {
                    availableDishes.Add(item.Key, new List<Dish> { item.Value });
                }
                else
                {
                    availableDishes.Add(item.Key, warehouse.GetAllRecommendDishes(allRecommendIngredients[item.Key], item.Key));
                }
            }

            //Get all variant of dishes
            List<List<Dish>> variantsOfDishes = GetAllVariantsOfDishes(availableDishes.Values.ToList());

            //calculate available budget
            var customersBudget = _customers.Sum(i => i.Budget);

            //calculate all prices
            var totalPrices = new Dictionary<List<Dish>, double>();
            foreach (var item in variantsOfDishes)
            {
                totalPrices.Add(item, item.Sum(i => i.GetTotalPrice()));
            }

            if (totalPrices.Count == 0)
            {
                return null;
            }

            //get max price less than available budget
            double maxPrice = totalPrices.Values.First();
            if (totalPrices.Count > 1)
                maxPrice = totalPrices.Values.Where(i => i <= customersBudget).Max();

            //get list with best price
            var bestDishes = totalPrices.Where(i => i.Value == maxPrice).First().Key;

            return bestDishes;
        }

        private List<List<Dish>> GetAllVariantsOfDishes(List<List<Dish>> availableDishes)
        {
            IEnumerable<List<Dish>> combos = new List<List<Dish>>() { new List<Dish>() };

            foreach (var inner in availableDishes)
            {
                combos = combos.SelectMany(r => inner
                .Select(x =>
                {
                    var n = r.Where(i => true).ToList();
                    if (x != null)
                    {
                        n.Add(x);
                    }
                    return n;
                }).ToList());
            }

            // Remove combinations were all items are empty
            return combos.Where(c => c.Count > 0).ToList();
        }

        private Dictionary<Customer, List<Ingredient>> GetAllRecommendIngredients()
        {
            var allRecommendIngredients = new Dictionary<Customer, List<Ingredient>>();
            int dishIndex = 0;
            for (int i = 0; i < _commandDataArr.Length;)
            {
                string str = _commandDataArr[i++];
                //look to recommend flag
                if (str == "Recommend")
                {
                    var recommendIngredient = new List<Ingredient>();
                    //parse recommend ingredient
                    while (i < _commandDataArr.Length)
                    {
                        str = _commandDataArr[i];
                        //if parsed string is name of ingredient add to list
                        if (_managerRepository.GetAllIngredients().Any(j => str == j.Name))
                        {
                            recommendIngredient.Add(_managerRepository.GetIngredient(str));
                            i++;
                        }
                        else//else add to dictionary and break while
                        {
                            break;
                        }
                    }
                    allRecommendIngredients.Add(_customers[dishIndex], recommendIngredient);
                    dishIndex++;
                    continue;
                }
                else if (_managerRepository.GetAllDishes().Any(j => j.Name == str))
                {
                    dishIndex++;
                }
            }
            return allRecommendIngredients;
        }

        private void SetCustomers()
        {
            var allCustomers = _managerRepository.GetAllCustomers();
            var customersName = new List<string>();
            customersName = _commandDataArr.Where(str => allCustomers.Any(c => c.FullName == str)).ToList();
            customersName.ForEach(str => _customers.Add(_managerRepository.GetCustomer(str)));
        }


        private bool CheckWarehouse(List<Dish> dishes)
        {
            var warehouse = new Warehouse(_managerRepository);
            var checkWarehouseOk = warehouse.CheckWarehouse(dishes);
            return checkWarehouseOk;
        }

        private void FixedPooledBudgedOfCostomers()
        {
            foreach (var item in customersBudgetAfterPooled)
            {
                var customer = _managerRepository.GetCustomer(item.Key);
                customer.Budget = item.Value;
                _managerRepository.UpdateCustomer(customer);
            }
        }

        private void FixedInitBudgedOfCostomers()
        {
            foreach (var item in customersBudgetInit)
            {
                var customer = _managerRepository.GetCustomer(item.Key);
                customer.Budget = item.Value;
                _managerRepository.UpdateCustomer(customer);
            }
        }

        private void PooledAllBudget()
        {
            var customersWithNegativeBudget = customersBudgetPooled.Where(i => i.Value < 0);

            var countCustomersWithNegativeBudget = customersWithNegativeBudget.Count();

            if (countCustomersWithNegativeBudget == 0)
            {
                return;
            }

            double totalNegativeBudget = 0;

            foreach (var item in customersWithNegativeBudget)
            {
                var value = item.Value * (-1);
                totalNegativeBudget += value;
                customersBudgetAfterPooled[item.Key] = Math.Round(customersBudgetAfterPooled[item.Key] - item.Value, 2);
                customersBudgetPooled[item.Key] = Math.Round(customersBudgetPooled[item.Key] - item.Value, 2);                 //0
            }

            totalNegativeBudget = Math.Round(totalNegativeBudget, 2);

            var avaregeAmountCustomerWithNegativeBudget = countCustomersWithNegativeBudget != 0 ? totalNegativeBudget / countCustomersWithNegativeBudget : 0;


            var customersWithPositiveBudget = customersBudgetPooled.Where(i => i.Value > 0);

            //here can lost 1 coins (100 / 3 = 33.3) | 33.3 * 3 = 99.9, we lost 0.01 
            foreach (var item in customersWithPositiveBudget)
            {
                customersBudgetAfterPooled[item.Key] = Math.Round(customersBudgetAfterPooled[item.Key] - avaregeAmountCustomerWithNegativeBudget, 2);
                customersBudgetPooled[item.Key] = Math.Round(item.Value - avaregeAmountCustomerWithNegativeBudget, 2);
            }

            PooledAllBudget();
        }

        private void GetAmountForBuyingTip()
        {
            var totalPriceOrder = customersPriceOrder.Values.Sum();
            var customersWithPositiveBudget = customersBudgetPooled.Where(i => i.Value > 0);
            var countCustomersWithPisitiveBudget = customersWithPositiveBudget.Count();

            var amountForTip = countCustomersWithPisitiveBudget != 0 ? Math.Round(totalPriceOrder / countCustomersWithPisitiveBudget, 2) : 0;

            //here can lost 1 coins (100 / 3 = 33.3) | 33.3 * 3 = 99.9, we lost 0.01 
            foreach (var item in customersWithPositiveBudget)
            {
                customersAmountForBuyingTip[item.Key] = amountForTip;
            }
        }

    }
}
