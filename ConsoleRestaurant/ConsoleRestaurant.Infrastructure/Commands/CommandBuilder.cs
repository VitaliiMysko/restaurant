﻿using ConsoleRestaurant.Domain.Interfaces;
using ConsoleRestaurant.Domain.SetsApp;
using ConsoleRestaurant.Infrastructure.Repositories;
using System;
using System.Linq;

namespace ConsoleRestaurant.Infrastructure.Commands
{
    public class CommandBuilder
    {
        private readonly string _action;
        private readonly string[] _dataArr;
        public CommandBuilder(string[] dataArrFull)
        {
            _action = dataArrFull[0];
            _dataArr = dataArrFull.Where((item, idx) => idx != 0).ToArray();
        }

        public ICommand Build()
        {
            var manager = ManagerRepository.GetInstance();

            switch (_action)
            {
                case nameof(Command.Audit):

                    return new AuditCommand(_dataArr, manager);

                case nameof(Command.Budget):

                    return new BudgetCommand(_dataArr, manager, true);

                case nameof(Command.Buy):

                    return new BuyCommand(_dataArr, manager);

                case nameof(Command.Order):

                    return new OrderCommand(_dataArr, manager);

                case nameof(Command.Table):

                    return new TableCommand(_dataArr, manager);

                case nameof(Command.Cleanoff):

                    return new CleanOffCommand(_dataArr, manager);

                case nameof(Command.Chart):

                    return new ChartCommand(_dataArr, manager);

                default:

                    throw new Exception($"Error! Command \"{_action}\" is not correct");
            }

        }
    }
}