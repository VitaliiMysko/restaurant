﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using ConsoleRestaurant.Domain.SetsApp;
using ConsoleRestaurant.Domain.Settings;
using ConsoleRestaurant.Infrastructure.Dataes;
using ConsoleRestaurant.Infrastructure.Repositories;
using System;

namespace ConsoleRestaurant.Infrastructure.Commands
{
    public class BudgetCommand : ICommand
    {
        private readonly string[] _commandDataArr;
        private readonly string _commandEntered;
        private string _restaurantName;
        private string _sign;
        private double _amount;
        private readonly bool _noPayTax;

        private BudgetAction _action;
        private Restaurant _restaurant;

        private readonly BaseManagerRepository _managerRepository;

        public BudgetCommand(string[] dataArr,
            BaseManagerRepository managerRepository,
            bool noPayTax = false)
        {
            _managerRepository = managerRepository;
            _commandDataArr = dataArr;
            _commandEntered = nameof(Command.Budget) + ": " + String.Join(", ", _commandDataArr);

            _noPayTax = noPayTax;
        }

        private bool DoValidation(Report report)
        {
            var nameCommand = report.Command.ToString();

            if (!AppConfig.Settings.Command.HasCommandEnable(nameCommand))
            {
                report.AddMessage($"Error! Command \"{nameCommand}\" disabled");
                return false;
            }

            if (_commandDataArr.Length != 2)
            {
                report.AddMessage("Error! Record is not correct");
                return false;
            }

            _restaurantName = AppConfig.Settings.MainRestaurantName;

            _sign = _commandDataArr[0];
            _action = GetBudgetAction(_sign);

            try
            {
                _amount = double.Parse(_commandDataArr[1]);
            }
            catch (Exception)
            {
                report.AddMessage($"Error: entered budget \"{_commandDataArr[1]}\" is not correct");
                return false;
            }

            if (_action == BudgetAction.Unknown)
            {
                report.AddMessage($"Error! Budget is fail. Unknown sign \"{_sign}\" ");
                return false;
            }

            return true;
        }

        private void SetData()
        {
            _restaurant = _managerRepository.GetRestaurant(_restaurantName);
        }

        private bool IsCorrectData(BudgetReport report)
        {
            var isCorrect = true;

            if (_restaurant == null)
            {
                report.AddMessage($"Error! Restauraunt \"{_restaurant.Name}\" not found ");
                isCorrect = false;
            }
            return isCorrect;
        }

        public Report Check()
        {
            var report = new BudgetReport();
            report.Command = Command.Budget;
            report.CommandEntered = _commandEntered;

            if (!DoValidation(report))
            {
                return report;
            }

            SetData();

            if (!IsCorrectData(report))
            {
                return report;
            }

            if (_restaurant.IsBankrupt)
            {
                if (!(_noPayTax && (_action == BudgetAction.Equals || _action == BudgetAction.Plus)))
                {
                    report.IsBankruptcy = true;
                    return report;
                }
            }

            report.Budget = GetNewBudgetOfRestaurant(_restaurant, _action, _amount);
            report.Tip = _action == BudgetAction.Tip ? _amount : 0;

            report.TimeCost = TimeCost.CalculatedTimeBudgetCommand();

            report.AddMessage(GetMessage(report, _amount));

            if (report.Budget <= 0)
            {
                //if budget get negative (bankrupt) on this step, operation will correct because budget was positive before
                report.IsBankruptcy = true;
            }

            report.CommandSucceeded = true;
            return report;
        }

        public Report Execute()
        {
            var report = (BudgetReport)Check();

            if (!report.CommandSucceeded)
            {
                return report;
            }

            return DoBudgetCommand(report);
        }

        private Report DoBudgetCommand(BudgetReport report)
        {
            _restaurant.Budget = Math.Round(report.Budget, 2);
            _restaurant.Tip = _action != BudgetAction.Profit ? Math.Round(_restaurant.Tip + report.Tip, 2) : 0;

            UpdataRestaurant(_restaurant);

            return report;
        }

        private void UpdataRestaurant(Restaurant restaurant)
        {
            _managerRepository.UpdateRestaurant(restaurant);
        }

        private string GetMessage(BudgetReport report, double amount)
        {
            var budget = report.Budget;

            if (budget > 0)
            {
                //daily tax
                if (_action == BudgetAction.Profit)
                {
                    return $" success (Profit: {GetProfitDailyTaxAmount(amount)})";
                }

                //daily tax
                if (_action == BudgetAction.EnvironmentalFine)
                {
                    return $" success (Environmental fine: {GetEnvironmentalFineDailyTaxAmount(amount)})";
                }

                if (_action == BudgetAction.Tip)
                {
                    return $" success (Tip tax: {GetTipTaxAmount(amount)})";
                }

                if (_noPayTax)
                {
                    return $" success (Tax: 0) | time {report.TimeCost}";
                }

                return $" success (Tax: {GetTransactionTaxAmount(amount)}) | time {report.TimeCost}";
            }
            else
            {
                //return $"Restaurant \"{_restaurantName}\" is bankrupt. Budget: {budget}";
                return $"Budget: {budget}";
            }
        }

        public double GetNewBudgetOfRestaurant(Restaurant restaurant, BudgetAction action, double amount)
        {
            double budgetNew = 0;

            if (action == BudgetAction.Profit)
            {
                budgetNew = GetBudgetAfterDailyTax(restaurant, amount);
            }
            else if (action == BudgetAction.EnvironmentalFine)
            {
                budgetNew = GetBudgetAfterEnvironmentalFineDailyTax(restaurant, amount);
            }
            else if (action == BudgetAction.Tip)
            {
                budgetNew = GetBudgetAfterTipTax(restaurant, amount);
            }
            else
            {
                budgetNew = GetBudgetAfterTransactionTax(restaurant, action, amount);
            }

            budgetNew = Math.Round(budgetNew, 2);
            return budgetNew;
        }

        private double GetBudgetAfterDailyTax(Restaurant restaurant, double amount)
        {
            var taxAmount = GetProfitDailyTaxAmount(amount);
            return restaurant.Budget - taxAmount;
        }

        private double GetBudgetAfterEnvironmentalFineDailyTax(Restaurant restaurant, double amount)
        {
            var taxAmount = GetEnvironmentalFineDailyTaxAmount(amount);

            var countOfLimits = Math.Floor(taxAmount / Constants.EnvironmentalLimit);

            var extraWasteTax = countOfLimits * Constants.ExrtaWasteTaxOverEnvironmentalLimit;

            return restaurant.Budget - taxAmount - extraWasteTax;
        }

        private double GetBudgetAfterTipTax(Restaurant restaurant, double amount)
        {
            var taxAmount = GetTipTaxAmount(amount);
            return restaurant.Budget + amount - taxAmount;
        }

        private double GetBudgetAfterTransactionTax(Restaurant restaurant, BudgetAction action, double amount)
        {
            double taxAmount = GetTransactionTaxAmount(amount); ;

            double budgetNew = 0;

            if (action == BudgetAction.Plus)
            {
                budgetNew = restaurant.Budget + amount - taxAmount;
            }
            else if (action == BudgetAction.Minus)
            {
                budgetNew = restaurant.Budget - (amount + taxAmount);
            }
            else
            {
                //action == BudgetAction.Equals
                budgetNew = amount;
            }

            return budgetNew;
        }

        private double GetTransactionTaxAmount(double amount)
        {
            if (_noPayTax)
            {
                return 0;
            }

            return AppConfig.Settings.Tax.GetTransactionTaxAmount(amount);
        }

        private double GetProfitDailyTaxAmount(double amount)
        {
            return AppConfig.Settings.Tax.GetDailyTaxAmount(amount);
        }

        private double GetEnvironmentalFineDailyTaxAmount(double amount)
        {
            return AppConfig.Settings.Tax.GetWasteTaxAmount(amount);
        }

        private double GetTipTaxAmount(double amount)
        {
            return AppConfig.Settings.Tax.GetTipsTaxAmount(amount);
        }

        public BudgetAction GetBudgetAction(string signStr)
        {
            if (signStr == "=")
            {
                return BudgetAction.Equals;
            }
            else if (signStr == "+")
            {
                return BudgetAction.Plus;
            }
            else if (signStr == "-")
            {
                return BudgetAction.Minus;
            }
            else if (signStr == "Profit")
            {
                //daily tax
                return BudgetAction.Profit;
            }
            else if (signStr == "Environmental fine")
            {
                //daily tax
                return BudgetAction.EnvironmentalFine;

            }
            else if (signStr == "Tip")
            {
                return BudgetAction.Tip;
            }
            else
            {
                return BudgetAction.Unknown;
            }
        }
    }
}
