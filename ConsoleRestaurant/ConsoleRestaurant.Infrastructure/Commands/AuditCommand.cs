﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using ConsoleRestaurant.Domain.SetsApp;
using ConsoleRestaurant.Infrastructure.Dataes;
using ConsoleRestaurant.Infrastructure.Repositories;
using System;

namespace ConsoleRestaurant.Infrastructure.Commands
{
    public class AuditCommand : ICommand
    {
        private readonly string[] _commandDataArr;
        private readonly string _commandEntered;

        private readonly BaseManagerRepository _managerRepository;
        public AuditCommand(string[] dataArr,
            BaseManagerRepository managerRepository)
        {
            _managerRepository = managerRepository;
            _commandDataArr = dataArr;
            _commandEntered = nameof(Command.Budget) + ": " + String.Join(", ", _commandDataArr);
        }

        private bool DoValidation(Report report)
        {
            var nameCommand = report.Command.ToString();

            if (!AppConfig.Settings.Command.HasCommandEnable(nameCommand))
            {
                report.AddMessage($"Error! Command \"{nameCommand}\" disabled");
                return false;
            }

            return true;
        }

        public Report Check()
        {
            var report = new Report();
            report.Command = Command.Audit;
            report.CommandEntered = _commandEntered;

            if (!DoValidation(report))
            {
                return report;
            }

            report.CommandSucceeded = true;
            return report;
        }

        public Report Execute()
        {
            var report = Check();

            if (!report.CommandSucceeded)
            {
                return report;
            }

            return DoAuditCommand(report);
        }

        private Report DoAuditCommand(Report report)
        {
            report.AddMessage(new Audit(_managerRepository).GetAudit());
            return report;
        }

    }
}
