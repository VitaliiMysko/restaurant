﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using ConsoleRestaurant.Domain.Rnd;
using ConsoleRestaurant.Domain.SetsApp;
using ConsoleRestaurant.Domain.Settings;
using ConsoleRestaurant.Infrastructure.Dataes;
using ConsoleRestaurant.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleRestaurant.Infrastructure.Commands
{
    public class BuyCommand : ICommand
    {
        private readonly string[] _commandDataArr;
        private readonly string _commandEntered;
        private string _customerFullName;
        private string _dishName;

        private Customer _customer;
        private Dish _dish;

        private List<Ingredient> _customerWishlist = new List<Ingredient>();
        private List<Ingredient> _recommendIngredients = new List<Ingredient>();

        private readonly BaseManagerRepository _managerRepository;

        public IRnd rnd = new Rnd();

        private bool _isRecommend;
        private bool _spoilOn;
        private bool _tipOn;

        public BuyCommand(string[] dataArr,
            BaseManagerRepository managerRepository,
            bool spoilOn = true,
            bool tipOn = true)
        {
            _managerRepository = managerRepository;
            _commandDataArr = dataArr;
            _commandEntered = nameof(Command.Buy) + ": " + String.Join(", ", _commandDataArr);
            _isRecommend = _commandDataArr.Any(i => i == "Recommend");
            _spoilOn = spoilOn;
            _tipOn = tipOn;
        }

        private bool DoValidation(Report report)
        {
            var nameCommand = report.Command.ToString();

            if (!AppConfig.Settings.Command.HasCommandEnable(nameCommand))
            {
                report.AddMessage($"Error! Command \"{nameCommand}\" disabled");
                return false;
            }

            if (_commandDataArr.Length != 2 && !_isRecommend)
            {
                report.AddMessage("Error! Record is not correct");
                return false;
            }

            _customerFullName = _commandDataArr[0];
            _dishName = _commandDataArr[1];

            return true;
        }

        private void SetData()
        {
            _customer = _managerRepository.GetCustomer(_customerFullName); 
            if (!_isRecommend)
            {
                _dish = _managerRepository.GetDish(_dishName);
            }
            else
            {
                for (int i = 2; i < _commandDataArr.Length; i++)
                {
                    _recommendIngredients.Add(_managerRepository.GetIngredient(_commandDataArr[2]));
                }
                SetRecomendDish();
            }
            SetCustomerWishList();
        }

        private void SetRecomendDish()
        {
            var warehouse = new Warehouse(_managerRepository);

            _dish = warehouse.GetRecommendDish(_recommendIngredients, _customer);
        }

        private void SetCustomerWishList()
        {
            var percent = rnd.NextDouble();
            if(percent < 0.05)
            {
                AddRandomWishlistsIngredient();
                AddRandomWishlistsIngredient();
                AddRandomWishlistsIngredient();
                return;
            }
            if (percent < 0.15)
            {
                AddRandomWishlistsIngredient();
                AddRandomWishlistsIngredient();
                return;
            }
            if (percent < 0.5)
            {
                AddRandomWishlistsIngredient();
                return;
            }
            return;
        }

        private void AddRandomWishlistsIngredient()
        {
            for (int i = 0; i < 20; i++) //we can use while(true), but "for" prevent stackOverflow exeption
            {
                var ingredient = GetRandomIngredient();

                if (_customerWishlist.Any(i => i.Name == ingredient.Name))
                {
                    if (_customer.Allergies != null)
                    {
                        if (_customer.Allergies.Any(a => a.Name == ingredient.Name))
                        {
                            continue;
                        }
                    }
                }
                else
                {
                    _customerWishlist.Add(ingredient);
                    break;
                }

            }
        }

        private Ingredient GetRandomIngredient()
        {
            List<Ingredient> allIngredients = _managerRepository.GetAllIngredients();
            Ingredient ingredient = allIngredients[rnd.Next(0, allIngredients.Count - 1)];
            return ingredient;
        }

        private bool IsCorrectData(BuyReport report)
        {
            var isCorrect = true;

            if (_dish == null)
            {
                report.AddMessage($"Error! Dish not found ");
                isCorrect = false;
            }
            if (_customer == null)
            {
                report.AddMessage($"Error! Customer not found ");
                isCorrect = false;
            }
            return isCorrect;
        }

        public Report Check()
        {
            var report = new BuyReport();
            report.Command = Command.Buy;
            report.CommandEntered = _commandEntered;
            report.IsBankruptcy = _managerRepository.GetRestaurant(AppConfig.Settings.MainRestaurantName).IsBankrupt;

            if (!DoValidation(report))
            {
                return report;
            }

            SetData();

            if (!IsCorrectData(report))
            {
                return report;
            }

            report.Customer = _customer;
            report.Dish = _dish;

            var warehouse = new Warehouse(_managerRepository);

            if (!warehouse.CheckWarehouse(_dish))
            {
                report.AddMessage("fail, not enough ingredients or dishes on the warehouse");
                return report;
            }

            var allergy = new Allergy(_managerRepository);
            string[] allergies = allergy.FindCustomerAllergiesInDish(_customer, _dish);
            report.IsAllergy = allergies.Length != 0;
            report.IsRecommend = _isRecommend;

            report.BudgetOfCustomer = _customer.Budget;

            var priceOfDish = _dish.GetTotalPrice();

            report.Discount = GetDiscountAmount(_customer, priceOfDish, report.IsAllergy);
            report.PriceOfDish = Math.Round(priceOfDish - report.Discount, 2);
            report.AreMoney = report.BudgetOfCustomer >= report.PriceOfDish;
            report.TransactionTaxAmount = GetTransactionTaxAmount(report.PriceOfDish);

            if (_tipOn && report.AreMoney && (AppConfig.Settings.Tip.IsTipped() || IsDishContainWishlistIngredients(_dish)))
            {
                report.Tip = AppConfig.Settings.Tip.GetTipAmount(report.PriceOfDish, report.BudgetOfCustomer) * GetWishlistFactor();
            }

            if (report.IsAllergy)
            {
                return allergy.GetReportAllergy(report, allergies);
            }

            var budgetReport = new DoCommand(_managerRepository).DoBudget("+", report.PriceOfDish, true);

            report.IsPoisoning = warehouse.Waste.IsPoisoning();

            report.Update(budgetReport);

            var timeCostMaxMin = TimeCost.CalculatedTimeBuyCommand(report.CommandEntered, _dish);

            //+5, it's conditional in point 6.13.2
            report.TimeCost = rnd.Next((int)timeCostMaxMin[0], (int)timeCostMaxMin[1] + 1) + 5;

            report.AddMessage(GetMessage(report));

            return report;
        }

        private double GetWishlistFactor()
        {
            var ingredients = GetWishlistIngredientInDish(_dish);
            return Math.Pow(ingredients.Count > 0 ? ingredients.Count : 1, 2);
        }

        private List<Ingredient> GetWishlistIngredientInDish(Dish dish)
        {
            List<Ingredient> ingredients = new List<Ingredient>();

            if (dish.Ingredients != null)
            {
                _customerWishlist.ForEach(wi =>
                {
                    if (dish.Ingredients.Where(i => i == wi).FirstOrDefault() != null)
                    {
                        ingredients.Add(wi);
                    }
                });
            }

            if (dish.Dishes != null)
            {
                foreach (var recipeDish in dish.Dishes)
                {
                    ingredients.AddRange(GetWishlistIngredientInDish(recipeDish));
                }
            }
            return ingredients.Distinct().ToList();
        }

        private bool IsDishContainWishlistIngredients(Dish dish)
        {
            if (dish.Ingredients != null && dish.Ingredients.Any(i => _customerWishlist.Any(w => w.Name == i.Name)))
            {
                return true;
            }
            else
            {
                if (dish.Dishes == null)
                {
                    return false;
                }
                else
                {
                    return dish.Dishes.Any(d => IsDishContainWishlistIngredients(d));
                }
            }
        }

        public Report Execute()
        {
            if (_spoilOn)
            {
                var warehouse = new Warehouse(_managerRepository);
                if (_commandDataArr.Length > 1)
                    warehouse.Waste.SpoilForCooking(_commandDataArr[1]);
            }
            var report = (BuyReport)Check();

            if (!report.CommandSucceeded)
            {
                return report;
            }

            return DoBuyCommand(report);
        }

        private Report DoBuyCommand(BuyReport report)
        {
            var warehouse = new Warehouse(_managerRepository);

            if (report.IsAllergy)
            {
                return new Allergy(_managerRepository).DoAllergy(report, warehouse, _dish);
            }

            warehouse.Take(_dish);

            _customer.ReduceBudget(report.PriceOfDish + report.Tip);
            UpdataCustomer(_customer);

            new ActivityCustomer(_managerRepository).ChangeActivityOfBuying(_customer);
            new DoCommand(_managerRepository).DoBudget("+", report.PriceOfDish);

            if (report.Tip > 0)
            {
                new DoCommand(_managerRepository).DoBudget("Tip", report.Tip);
            }

            return report;
        }

        private void UpdataCustomer(Customer customer)
        {
            _managerRepository.UpdateCustomer(customer);
        }

        private string GetMessage(BuyReport report)
        {
            if (report.AreMoney)
            {

                if (!(report.IsBankruptcy || report.IsPoisoning))
                {
                    report.CommandSucceeded = true;
                }

                var discountInfo = report.Discount != 0 ? $"(Discount: {report.Discount}) " : "";
                var tipInfo = report.Tip != 0 ? "Tip: " + report.Tip + " | Tip tax: " + AppConfig.Settings.Tax.GetTipsTaxAmount(report.Tip) : "";
                return $"{_customerFullName} [{report.BudgetOfCustomer}], {_dish.Name} [{report.PriceOfDish} {discountInfo}(Tax: {report.TransactionTaxAmount}) {tipInfo}] | time {report.TimeCost} => success";
            }
            else
            {
                report.CommandSucceeded = false;
                return $"{_customerFullName} [{report.BudgetOfCustomer}], {_dish.Name} [{report.PriceOfDish}] => fail, not enough money";
            }
        }

        private double GetTransactionTaxAmount(double amount)
        {
            return AppConfig.Settings.Tax.GetTransactionTaxAmount(amount);
        }

        public double GetDiscountAmount(Customer _customer, double amount, bool isAllergy)
        {
            if (isAllergy)
            {
                return 0;
            }

            if (!HasDiscountEnable(_customer))
            {
                return 0;
            }

            return AppConfig.Settings.Coupon.GetDiscountAmount(amount);
        }

        private bool HasDiscountEnable(Customer customer)
        {
            var times = new ActivityCustomer(_managerRepository).GetActivity(customer);
            times += 1;
            return times % Constants.NumberOfSuccessfulTransactionsForGettingDiscount == 0;
        }
    }
}
