﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace ConsoleRestaurant.Infrastructure.Repositories
{
    public class RestaurantsRepository : IRepository<Restaurant>
    {
        private readonly string _pathToJson;
        private List<Restaurant> _records = new List<Restaurant>();

        public RestaurantsRepository()
        {
            _pathToJson = "Databases\\Jsons\\Restaurants.json";
        }

        public Restaurant Add(Restaurant entity)
        {
            ReadJsonFile();

            _records.Add(entity);

            WriteJsonFile();

            return entity;
        }

        public Restaurant Delete(string name)
        {
            ReadJsonFile();

            var record = Get(name);

            _records.Remove(record);

            WriteJsonFile();

            return record;
        }

        public Restaurant Get(string name)
        {
            ReadJsonFile();

            var record = _records.Find(item => item.Name == name);

            //if (record == null)
            //{
            //    throw new Exception($"Error! Restauraunt \"{name}\" not found ");
            //}

            return record;
        }

        public List<Restaurant> GetAll()
        {
            ReadJsonFile();
            return _records;
        }

        public Restaurant Update(Restaurant entity)
        {
            var record = Get(entity.Name);

            record.Budget = entity.Budget;

            WriteJsonFile();

            return record;
        }

        private void ReadJsonFile()
        {
            if (_records.Count > 0)
            {
                return;
            }

            if (!File.Exists(_pathToJson))
            {
                throw new Exception("Error! Do not esseess to \"Restaurants.json\".");
            }

            var json = File.ReadAllText(_pathToJson);
            _records = JsonSerializer.Deserialize<List<Restaurant>>(json);
        }

        private void WriteJsonFile()
        {
            var newJson = JsonSerializer.Serialize(_records);
            File.WriteAllText(_pathToJson, newJson);
        }

    }
}
