﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using System.Collections.Generic;

namespace ConsoleRestaurant.Infrastructure.Repositories
{
    public class ManagerRepository : BaseManagerRepository
    {
        private static ManagerRepository _instance;

        private readonly IRepository<Customer> _repositoryCustomer;
        private readonly IRepository<Dish> _repositoryDish;
        private readonly IRepository<Ingredient> _repositoryIngredient;
        private readonly IRepository<Restaurant> _repositoryRestaurant;
        private readonly IRepository<WarehouseDish> _repositoryWarehouseDish;
        private readonly IRepository<WarehouseIngredient> _repositoryWarehouseIngredient;
        private readonly IRepositoryGet<Appsetting> _repositoryAppsetting;
        private readonly IRepository<ActivityOfBuying> _repositoryActivityOfBuying;
        private readonly IRepository<WasteDish> _repositoryWasteDish;
        private readonly IRepository<WasteIngredient> _repositoryWasteIngredient;
        private readonly IRepository<WasteIngredient> _repositoryWastePoolIngredient;
        private readonly IRepository<WasteDish> _repositoryWastePoolDish;

        public static ManagerRepository GetInstance()
        {
            if (_instance == null)
                _instance = new ManagerRepository();
            return _instance;
        }

        private ManagerRepository()
        {
            _repositoryCustomer = new CustomersRepository();
            _repositoryDish = new DishesRepository();
            _repositoryIngredient = new IngredientsRepository();
            _repositoryRestaurant = new RestaurantsRepository();
            _repositoryWarehouseDish = new WarehouseDishesRepository();
            _repositoryWarehouseIngredient = new WarehouseIngredientsRepository();
            _repositoryAppsetting = new AppsettingsRepository();
            _repositoryActivityOfBuying = new ActivityOfBuyingRepository();
            _repositoryWasteDish = new WasteDishesRepository();
            _repositoryWasteIngredient = new WasteIngredientsRepository();
            _repositoryWastePoolDish = new WastePoolDishes();
            _repositoryWastePoolIngredient = new WastePoolIngredients();
        }

        //======================================================== Add
        public override Customer AddCustomer(Customer entity)
        {
            return _repositoryCustomer.Add(entity);
        }

        public override Dish AddDish(Dish entity)
        {
            return _repositoryDish.Add(entity);
        }

        public override Ingredient AddIngredient(Ingredient entity)
        {
            return _repositoryIngredient.Add(entity);
        }

        public override Restaurant AddRestaurant(Restaurant entity)
        {
            return _repositoryRestaurant.Add(entity);
        }

        public override WarehouseDish AddWarehouseDish(WarehouseDish entity)
        {
            return _repositoryWarehouseDish.Add(entity);
        }

        public override WarehouseIngredient AddWarehouseIngredient(WarehouseIngredient entity)
        {
            return _repositoryWarehouseIngredient.Add(entity);
        }

        public override ActivityOfBuying AddActivityOfBuying(ActivityOfBuying entity)
        {
            return _repositoryActivityOfBuying.Add(entity);
        }

        public override WasteDish AddWasteDish(WasteDish entity)
        {
            return _repositoryWasteDish.Add(entity);
        }

        public override WasteIngredient AddWasteIngredient(WasteIngredient entity)
        {
            return _repositoryWasteIngredient.Add(entity);
        }

        public override WasteDish AddWastePoolDish(WasteDish entity)
        {
            return _repositoryWastePoolDish.Add(entity);
        }

        public override WasteIngredient AddWastePoolIngredient(WasteIngredient entity)
        {
            return _repositoryWastePoolIngredient.Add(entity);
        }

        //======================================================== Delete
        public override Customer DeleteCustomer(string name)
        {
            return _repositoryCustomer.Delete(name);
        }

        public override Dish DeleteDish(string name)
        {
            return _repositoryDish.Delete(name);
        }

        public override Ingredient DeleteIngredient(string name)
        {
            return _repositoryIngredient.Delete(name);
        }

        public override Restaurant DeleteRestaurant(string name)
        {
            return _repositoryRestaurant.Delete(name);
        }

        public override WarehouseDish DeleteWarehouseDish(string name)
        {
            return _repositoryWarehouseDish.Delete(name);
        }

        public override WarehouseIngredient DeleteWarehouseIngredient(string name)
        {
            return _repositoryWarehouseIngredient.Delete(name);
        }

        public override ActivityOfBuying DeleteActivityOfBuying(string name)
        {
            return _repositoryActivityOfBuying.Delete(name);
        }

        public override WasteDish DeleteWasteDish(string name)
        {
            return _repositoryWasteDish.Delete(name);
        }

        public override WasteIngredient DeleteWasteIngredient(string name)
        {
            return _repositoryWasteIngredient.Delete(name);
        }

        public override WasteDish DeleteWastePoolDish(string name)
        {
            return _repositoryWastePoolDish.Delete(name);
        }

        public override WasteIngredient DeleteWastePoolIngredient(string name)
        {
            return _repositoryWastePoolIngredient.Delete(name);
        }

        //======================================================== Get
        public override Customer GetCustomer(string name)
        {
            return _repositoryCustomer.Get(name);
        }

        public override Dish GetDish(string name)
        {
            return _repositoryDish.Get(name);
        }

        public override Ingredient GetIngredient(string name)
        {
            return _repositoryIngredient.Get(name);
        }

        public override Restaurant GetRestaurant(string name)
        {
            return _repositoryRestaurant.Get(name);
        }

        public override WarehouseDish GetWarehouseDish(string name)
        {
            return _repositoryWarehouseDish.Get(name);
        }

        public override WarehouseIngredient GetWarehouseIngredient(string name)
        {
            return _repositoryWarehouseIngredient.Get(name);
        }

        public override Appsetting GetAppsetting()
        {
            return _repositoryAppsetting.Get();
        }

        public override ActivityOfBuying GetActivityOfBuying(string name)
        {
            return _repositoryActivityOfBuying.Get(name);
        }

        public override WasteDish GetWasteDish(string name)
        {
            return _repositoryWasteDish.Get(name);
        }

        public override WasteIngredient GetWasteIngredient(string name)
        {
            return _repositoryWasteIngredient.Get(name);
        }

        public override WasteDish GetWastePoolDish(string name)
        {
            return _repositoryWastePoolDish.Get(name);
        }
        
        public override WasteIngredient GetWastePoolIngredient(string name)
        {
            return _repositoryWastePoolIngredient.Get(name);
        }
        //======================================================== GetAll
        public override List<Customer> GetAllCustomers()
        {
            return _repositoryCustomer.GetAll();
        }

        public override List<Dish> GetAllDishes()
        {
            return _repositoryDish.GetAll();
        }

        public override List<Ingredient> GetAllIngredients()
        {
            return _repositoryIngredient.GetAll();
        }

        public override List<Restaurant> GetAllRestaurants()
        {
            return _repositoryRestaurant.GetAll();
        }

        public override List<WarehouseDish> GetAllWarehouseDishes()
        {
            return _repositoryWarehouseDish.GetAll();
        }

        public override List<WarehouseIngredient> GetAllWarehouseIngredients()
        {
            return _repositoryWarehouseIngredient.GetAll();
        }

        public override List<ActivityOfBuying> GetAllActivityOfBuying()
        {
            return _repositoryActivityOfBuying.GetAll();
        }

        public override List<WasteDish> GetAllWasteDishes()
        {
            return _repositoryWasteDish.GetAll();
        }

        public override List<WasteIngredient> GetAllWasteIngredients()
        {
            return _repositoryWasteIngredient.GetAll();
        }


        public override List<WasteDish> GetAllWastePoolDishes()
        {
            return _repositoryWastePoolDish.GetAll();
        }

        public override List<WasteIngredient> GetAllWastePoolIngredients()
        {
            return _repositoryWastePoolIngredient.GetAll();
        }
        //======================================================== Update
        public override Customer UpdateCustomer(Customer entity)
        {
            return _repositoryCustomer.Update(entity);
        }

        public override Dish UpdateDish(Dish entity)
        {
            return _repositoryDish.Update(entity);
        }

        public override Ingredient UpdateIngredient(Ingredient entity)
        {
            return _repositoryIngredient.Update(entity);
        }

        public override Restaurant UpdateRestaurant(Restaurant entity)
        {
            return _repositoryRestaurant.Update(entity);
        }

        public override WarehouseDish UpdateWarehouseDish(WarehouseDish entity)
        {
            return _repositoryWarehouseDish.Update(entity);
        }

        public override WarehouseIngredient UpdateWarehouseIngredient(WarehouseIngredient entity)
        {
            return _repositoryWarehouseIngredient.Update(entity);
        }

        public override ActivityOfBuying UpdateActivityOfBuying(ActivityOfBuying entity)
        {
            return _repositoryActivityOfBuying.Update(entity);
        }

        public override WasteDish UpdateWasteDish(WasteDish entity)
        {
            return _repositoryWasteDish.Update(entity);
        }

        public override WasteIngredient UpdateWasteIngredient(WasteIngredient entity)
        {
            return _repositoryWasteIngredient.Update(entity);
        }

        public override WasteDish UpdateWastePoolDish(WasteDish entity)
        {
            return _repositoryWastePoolDish.Update(entity);
        }

        public override WasteIngredient UpdateWastePoolIngredient(WasteIngredient entity)
        {
            return _repositoryWastePoolIngredient.Update(entity);
        }
    }
}
