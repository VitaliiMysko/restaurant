﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace ConsoleRestaurant.Infrastructure.Repositories
{
    public class WarehouseIngredientsRepository : IRepository<WarehouseIngredient>
    {
        private readonly string _pathToJson;
        private List<WarehouseIngredient> _records = new List<WarehouseIngredient>();

        public WarehouseIngredientsRepository()
        {
            _pathToJson = "Databases\\Jsons\\WarehouseIngredients.json";
        }

        public WarehouseIngredient Add(WarehouseIngredient entity)
        {
            ReadJsonFile();

            _records.Add(entity);

            WriteJsonFile();

            return entity;
        }

        public WarehouseIngredient Delete(string name)
        {
            ReadJsonFile();

            var record = Get(name);

            _records.Remove(record);

            WriteJsonFile();

            return record;
        }

        public WarehouseIngredient Get(string name)
        {
            ReadJsonFile();

            var record = _records.Find(item => item.Ingredient.Name == name);

            //if (record == null)
            //{
            //    throw new Exception($"Error! Ingredient \"{name}\" not found in warehouse");
            //}

            return record;
        }

        public List<WarehouseIngredient> GetAll()
        {
            ReadJsonFile();
            return _records;
        }

        public WarehouseIngredient Update(WarehouseIngredient entity)
        {
            var record = Get(entity.Ingredient.Name);

            record.Quantity = entity.Quantity;

            WriteJsonFile();

            return record;
        }

        private void ReadJsonFile()
        {
            if (_records.Count > 0)
            {
                return;
            }

            if (!File.Exists(_pathToJson))
            {
                throw new Exception("Error! Do not esseess to \"WarehouseIngredients.json\".");
            }

            var json = File.ReadAllText(_pathToJson);
            _records = JsonSerializer.Deserialize<List<WarehouseIngredient>>(json);
        }

        private void WriteJsonFile()
        {
            var newJson = JsonSerializer.Serialize(_records);
            File.WriteAllText(_pathToJson, newJson);
        }

    }
}
