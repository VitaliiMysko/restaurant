﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using System;
using System.IO;
using System.Text.Json;

namespace ConsoleRestaurant.Infrastructure.Repositories
{
    public class AppsettingsRepository : IRepositoryGet<Appsetting>
    {
        private readonly string _pathToJson;
        private Appsetting _record = new Appsetting();

        public AppsettingsRepository()
        {
            _pathToJson = "Databases\\Jsons\\Appsettings.json";
        }

        public Appsetting Get()
        {
            ReadJsonFile();

            return _record;
        }

        private void ReadJsonFile()
        {
            //if (_record.Count > 0)
            //{
            //    return;
            //}

            if (!File.Exists(_pathToJson))
            {
                throw new Exception("Error! Do not esseess to \"Appsettings.json\".");
            }

            var json = File.ReadAllText(_pathToJson);
            _record = JsonSerializer.Deserialize<Appsetting>(json);
        }

    }
}
