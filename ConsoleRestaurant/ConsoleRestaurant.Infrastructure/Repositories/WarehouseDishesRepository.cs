﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace ConsoleRestaurant.Infrastructure.Repositories
{
    public class WarehouseDishesRepository : IRepository<WarehouseDish>
    {
        private readonly string _pathToJson;
        private List<WarehouseDish> _records = new List<WarehouseDish>();

        public WarehouseDishesRepository()
        {
            _pathToJson = "Databases\\Jsons\\WarehouseDishes.json";
        }

        public WarehouseDish Add(WarehouseDish entity)
        {
            ReadJsonFile();

            _records.Add(entity);

            WriteJsonFile();

            return entity;
        }

        public WarehouseDish Delete(string name)
        {
            ReadJsonFile();

            var record = Get(name);

            _records.Remove(record);

            WriteJsonFile();

            return record;
        }

        public WarehouseDish Get(string name)
        {
            ReadJsonFile();

            var record = _records.Find(item => item.Dish.Name == name);

            //if (record == null)
            //{
            //    throw new Exception($"Error! Dish \"{name}\" not found in warehouse");
            //}

            return record;
        }

        public List<WarehouseDish> GetAll()
        {
            ReadJsonFile();
            return _records;
        }

        public WarehouseDish Update(WarehouseDish entity)
        {
            var record = Get(entity.Dish.Name);

            record.Quantity = entity.Quantity;

            WriteJsonFile();

            return record;
        }

        private void ReadJsonFile()
        {
            if (_records.Count > 0)
            {
                return;
            }

            if (!File.Exists(_pathToJson))
            {
                throw new Exception("Error! Do not esseess to \"WarehouseDishes.json\".");
            }

            var json = File.ReadAllText(_pathToJson);
            _records = JsonSerializer.Deserialize<List<WarehouseDish>>(json);
        }

        private void WriteJsonFile()
        {
            var newJson = JsonSerializer.Serialize(_records);
            File.WriteAllText(_pathToJson, newJson);
        }

    }
}
