﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace ConsoleRestaurant.Infrastructure.Repositories
{
    public class ActivityOfBuyingRepository : IRepository<ActivityOfBuying>
    {
        private readonly string _pathToJson;
        private List<ActivityOfBuying> _records = new List<ActivityOfBuying>();

        public ActivityOfBuyingRepository()
        {
            _pathToJson = "Databases\\Jsons\\ActivityOfBuying.json";
        }

        public ActivityOfBuying Add(ActivityOfBuying entity)
        {
            ReadJsonFile();

            _records.Add(entity);

            WriteJsonFile();

            return entity;

        }

        public ActivityOfBuying Delete(string name)
        {
            ReadJsonFile();

            var record = Get(name);

            _records.Remove(record);

            WriteJsonFile();

            return record;

        }

        public ActivityOfBuying Get(string name)
        {
            ReadJsonFile();

            var record = _records.Find(item => item.Customer.FullName == name);

            //if (record == null)
            //{
            //    throw new Exception($"Error! Activity of buying of customer \"{name}\" not found ");
            //}

            return record;
        }

        public List<ActivityOfBuying> GetAll()
        {
            ReadJsonFile();
            return _records;
        }

        public ActivityOfBuying Update(ActivityOfBuying entity)
        {
            var record = Get(entity.Customer.FullName);

            record.Times = entity.Times;

            WriteJsonFile();

            return record;

        }

        private void ReadJsonFile()
        {
            if (_records.Count > 0)
            {
                return;
            }

            //if (!File.Exists(_pathToJson))
            //{
            //    throw new Exception("Error! Do not esseess to \"ActivityOfBuying.json\".");
            //}

            var json = File.ReadAllText(_pathToJson);
            _records = JsonSerializer.Deserialize<List<ActivityOfBuying>>(json);
        }

        private void WriteJsonFile()
        {
            var newJson = JsonSerializer.Serialize(_records);
            File.WriteAllText(_pathToJson, newJson);
        }

    }
}
