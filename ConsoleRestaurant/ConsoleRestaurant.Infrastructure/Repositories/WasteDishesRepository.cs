﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace ConsoleRestaurant.Infrastructure.Repositories
{
    public class WasteDishesRepository : IRepository<WasteDish>
    {
        private readonly string _pathToJson;
        private List<WasteDish> _records = new List<WasteDish>();
        public WasteDishesRepository()
        {
            _pathToJson = "Databases\\Jsons\\WasteDishes.json";
        }

        public WasteDish Add(WasteDish entity)
        {
            ReadJsonFile();

            _records.Add(entity);

            WriteJsonFile();

            return entity;
        }

        public WasteDish Delete(string name)
        {
            ReadJsonFile();

            var record = Get(name);

            _records.Remove(record);

            WriteJsonFile();

            return record;
        }

        public WasteDish Get(string name)
        {
            ReadJsonFile();

            var record = _records.Find(item => item.Dish.Name == name);

            //if (record == null)
            //{
            //    throw new Exception($"Error! Ingredient \"{name}\" not found ");
            //}

            return record;
        }

        public List<WasteDish> GetAll()
        {
            ReadJsonFile();
            return _records;
        }

        public WasteDish Update(WasteDish entity)
        {
            var record = Get(entity.Dish.Name);

            record.Quantity = entity.Quantity;

            WriteJsonFile();

            return record;
        }

        private void ReadJsonFile()
        {
            if (_records.Count > 0)
            {
                return;
            }

            if (!File.Exists(_pathToJson))
            {
                throw new Exception("Error! Do not esseess to \"WasteDishes.json\".");
            }

            var json = File.ReadAllText(_pathToJson);
            _records = JsonSerializer.Deserialize<List<WasteDish>>(json);
        }

        private void WriteJsonFile()
        {
            var newJson = JsonSerializer.Serialize(_records);
            File.WriteAllText(_pathToJson, newJson);
        }

    }
}
