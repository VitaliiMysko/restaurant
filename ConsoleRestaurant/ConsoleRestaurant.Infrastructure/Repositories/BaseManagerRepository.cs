﻿using ConsoleRestaurant.Domain.Entities;
using System.Collections.Generic;

namespace ConsoleRestaurant.Infrastructure.Repositories
{
    public abstract class BaseManagerRepository
    {
        //======================================================== Add
        public abstract Customer AddCustomer(Customer entity);
        public abstract Dish AddDish(Dish entity);
        public abstract Ingredient AddIngredient(Ingredient entity);
        public abstract Restaurant AddRestaurant(Restaurant entity);
        public abstract WarehouseDish AddWarehouseDish(WarehouseDish entity);
        public abstract WarehouseIngredient AddWarehouseIngredient(WarehouseIngredient entity);
        public abstract ActivityOfBuying AddActivityOfBuying(ActivityOfBuying entity);
        public abstract WasteDish AddWasteDish(WasteDish entity);
        public abstract WasteIngredient AddWasteIngredient(WasteIngredient entity);
        public abstract WasteDish AddWastePoolDish(WasteDish entity);
        public abstract WasteIngredient AddWastePoolIngredient(WasteIngredient entity);

        //======================================================== Delete
        public abstract Customer DeleteCustomer(string name);
        public abstract Dish DeleteDish(string name);
        public abstract Ingredient DeleteIngredient(string name);
        public abstract Restaurant DeleteRestaurant(string name);
        public abstract WarehouseDish DeleteWarehouseDish(string name);
        public abstract WarehouseIngredient DeleteWarehouseIngredient(string name);
        public abstract ActivityOfBuying DeleteActivityOfBuying(string name);
        public abstract WasteDish DeleteWasteDish(string name);
        public abstract WasteIngredient DeleteWasteIngredient(string name);
        public abstract WasteDish DeleteWastePoolDish(string name);
        public abstract WasteIngredient DeleteWastePoolIngredient(string name);

        //======================================================== Get
        public abstract Customer GetCustomer(string name);
        public abstract Dish GetDish(string name);
        public abstract Ingredient GetIngredient(string name);
        public abstract Restaurant GetRestaurant(string name);
        public abstract WarehouseDish GetWarehouseDish(string name);
        public abstract WarehouseIngredient GetWarehouseIngredient(string name);
        public abstract Appsetting GetAppsetting();
        public abstract ActivityOfBuying GetActivityOfBuying(string name);
        public abstract WasteDish GetWasteDish(string name);
        public abstract WasteIngredient GetWasteIngredient(string name);
        public abstract WasteDish GetWastePoolDish(string name);
        public abstract WasteIngredient GetWastePoolIngredient(string name);


        //======================================================== GetAll
        public abstract List<Customer> GetAllCustomers();
        public abstract List<Dish> GetAllDishes();
        public abstract List<Ingredient> GetAllIngredients();
        public abstract List<Restaurant> GetAllRestaurants();
        public abstract List<WarehouseDish> GetAllWarehouseDishes();
        public abstract List<WarehouseIngredient> GetAllWarehouseIngredients();
        public abstract List<ActivityOfBuying> GetAllActivityOfBuying();
        public abstract List<WasteDish> GetAllWasteDishes();
        public abstract List<WasteIngredient> GetAllWasteIngredients();
        public abstract List<WasteDish> GetAllWastePoolDishes();
        public abstract List<WasteIngredient> GetAllWastePoolIngredients();
        //======================================================== Update
        public abstract Customer UpdateCustomer(Customer entity);
        public abstract Dish UpdateDish(Dish entity);
        public abstract Ingredient UpdateIngredient(Ingredient entity);
        public abstract Restaurant UpdateRestaurant(Restaurant entity);
        public abstract WarehouseDish UpdateWarehouseDish(WarehouseDish entity);
        public abstract WarehouseIngredient UpdateWarehouseIngredient(WarehouseIngredient entity);
        public abstract ActivityOfBuying UpdateActivityOfBuying(ActivityOfBuying entity);
        public abstract WasteDish UpdateWasteDish(WasteDish entity);
        public abstract WasteIngredient UpdateWasteIngredient(WasteIngredient entity);
        public abstract WasteDish UpdateWastePoolDish(WasteDish entity);
        public abstract WasteIngredient UpdateWastePoolIngredient(WasteIngredient entity);
    }

}
