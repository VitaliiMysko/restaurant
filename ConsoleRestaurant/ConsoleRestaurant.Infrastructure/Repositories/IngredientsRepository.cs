﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace ConsoleRestaurant.Infrastructure.Repositories
{
    public class IngredientsRepository : IRepository<Ingredient>
    {
        private readonly string _pathToJson;
        private List<Ingredient> _records = new List<Ingredient>();
        public IngredientsRepository()
        {
            _pathToJson = "Databases\\Jsons\\Ingredients.json";
        }

        public Ingredient Add(Ingredient entity)
        {
            ReadJsonFile();

            _records.Add(entity);

            WriteJsonFile();

            return entity;
        }

        public Ingredient Delete(string name)
        {
            ReadJsonFile();

            var record = Get(name);

            _records.Remove(record);

            WriteJsonFile();

            return record;
        }

        public Ingredient Get(string name)
        {
            ReadJsonFile();

            var record = _records.Find(item => item.Name == name);

            //if (record == null)
            //{
            //    throw new Exception($"Error! Ingredient \"{name}\" not found ");
            //}

            return record;
        }

        public List<Ingredient> GetAll()
        {
            ReadJsonFile();
            return _records;
        }

        public Ingredient Update(Ingredient entity)
        {
            var record = Get(entity.Name);

            record.Price = entity.Price;

            WriteJsonFile();

            return record;
        }

        private void ReadJsonFile()
        {
            if (_records.Count > 0)
            {
                return;
            }

            if (!File.Exists(_pathToJson))
            {
                throw new Exception("Error! Do not esseess to \"Ingredients.json\".");
            }

            var json = File.ReadAllText(_pathToJson);
            _records = JsonSerializer.Deserialize<List<Ingredient>>(json);
        }

        private void WriteJsonFile()
        {
            var newJson = JsonSerializer.Serialize(_records);
            File.WriteAllText(_pathToJson, newJson);
        }
    }
}
