﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace ConsoleRestaurant.Infrastructure.Repositories
{
    public class CustomersRepository : IRepository<Customer>
    {
        private readonly string _pathToJson;
        private List<Customer> _records = new List<Customer>();

        public CustomersRepository()
        {
            _pathToJson = "Databases\\Jsons\\Customers.json";
        }

        public Customer Add(Customer entity)
        {
            ReadJsonFile();

            _records.Add(entity);

            WriteJsonFile();

            return entity;
        }

        public Customer Delete(string name)
        {
            ReadJsonFile();

            var record = Get(name);

            _records.Remove(record);

            WriteJsonFile();

            return record;
        }

        public Customer Get(string name)
        {
            ReadJsonFile();

            var record = _records.Find(item => item.FullName == name);

            if (record == null)
            {
                throw new Exception($"Error! Customer \"{name}\" not found ");
            }

            return record;
        }

        public List<Customer> GetAll()
        {
            ReadJsonFile();
            return _records;
        }

        public Customer Update(Customer entity)
        {
            var record = Get(entity.FullName);

            record.Budget = entity.Budget;

            WriteJsonFile();

            return record;
        }

        private void ReadJsonFile()
        {
            if (_records.Count > 0)
            {
                return;
            }

            if (!File.Exists(_pathToJson))
            {
                throw new Exception("Error! Do not esseess to \"Customers.json\".");
            }

            var json = File.ReadAllText(_pathToJson);
            _records = JsonSerializer.Deserialize<List<Customer>>(json);
        }

        private void WriteJsonFile()
        {
            var newJson = JsonSerializer.Serialize(_records);
            File.WriteAllText(_pathToJson, newJson);
        }

    }
}
