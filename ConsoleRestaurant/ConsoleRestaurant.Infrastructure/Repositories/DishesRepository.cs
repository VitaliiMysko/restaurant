﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace ConsoleRestaurant.Infrastructure.Repositories
{
    public class DishesRepository : IRepository<Dish>
    {
        private readonly string _pathToJson;
        private List<Dish> _records = new List<Dish>();

        public DishesRepository()
        {
            _pathToJson = "Databases\\Jsons\\Dishes.json";
        }

        public Dish Add(Dish entity)
        {
            ReadJsonFile();

            _records.Add(entity);

            WriteJsonFile();

            return entity;
        }

        public Dish Delete(string name)
        {
            ReadJsonFile();

            var record = Get(name);

            _records.Remove(record);

            WriteJsonFile();

            return record;
        }

        public Dish Get(string name)
        {
            ReadJsonFile();

            var record = _records.Find(item => item.Name == name);

            if (record == null)
            {
                throw new Exception($"Error! Dish \"{name}\" not found ");
            }

            return record;
        }

        public List<Dish> GetAll()
        {
            ReadJsonFile();
            return _records;
        }

        public Dish Update(Dish entity)
        {
            var record = Get(entity.Name);

            record.Dishes = entity.Dishes;
            record.Ingredients = entity.Ingredients;

            WriteJsonFile();

            return record;
        }

        private void ReadJsonFile()
        {
            if (_records.Count > 0)
            {
                return;
            }

            if (!File.Exists(_pathToJson))
            {
                throw new Exception("Error! Do not esseess to \"Dishes.json\".");
            }

            var json = File.ReadAllText(_pathToJson);
            _records = JsonSerializer.Deserialize<List<Dish>>(json);
        }

        private void WriteJsonFile()
        {
            var newJson = JsonSerializer.Serialize(_records);
            File.WriteAllText(_pathToJson, newJson);
        }

    }
}
