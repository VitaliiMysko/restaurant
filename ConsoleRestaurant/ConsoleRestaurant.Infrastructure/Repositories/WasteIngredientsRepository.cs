﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace ConsoleRestaurant.Infrastructure.Repositories
{
    public class WasteIngredientsRepository : IRepository<WasteIngredient>
    {
        private readonly string _pathToJson;
        private List<WasteIngredient> _records = new List<WasteIngredient>();
        public WasteIngredientsRepository()
        {
            _pathToJson = "Databases\\Jsons\\WasteIngredients.json";
        }

        public WasteIngredient Add(WasteIngredient entity)
        {
            ReadJsonFile();

            _records.Add(entity);

            WriteJsonFile();

            return entity;
        }

        public WasteIngredient Delete(string name)
        {
            ReadJsonFile();

            var record = Get(name);

            _records.Remove(record);

            WriteJsonFile();

            return record;
        }

        public WasteIngredient Get(string name)
        {
            ReadJsonFile();

            var record = _records.Find(item => item.Ingredient.Name == name);

            //if (record == null)
            //{
            //    throw new Exception($"Error! Ingredient \"{name}\" not found ");
            //}

            return record;
        }

        public List<WasteIngredient> GetAll()
        {
            ReadJsonFile();
            return _records;
        }

        public WasteIngredient Update(WasteIngredient entity)
        {
            var record = Get(entity.Ingredient.Name);

            record.Quantity = entity.Quantity;

            WriteJsonFile();

            return record;
        }

        private void ReadJsonFile()
        {
            if (_records.Count > 0)
            {
                return;
            }

            if (!File.Exists(_pathToJson))
            {
                throw new Exception("Error! Do not esseess to \"WasteIngredients.json\".");
            }

            var json = File.ReadAllText(_pathToJson);
            _records = JsonSerializer.Deserialize<List<WasteIngredient>>(json);
        }

        private void WriteJsonFile()
        {
            var newJson = JsonSerializer.Serialize(_records);
            File.WriteAllText(_pathToJson, newJson);
        }

    }
}
