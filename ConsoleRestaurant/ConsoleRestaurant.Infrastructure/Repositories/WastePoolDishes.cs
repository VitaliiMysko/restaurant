﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace ConsoleRestaurant.Infrastructure.Repositories
{
    class WastePoolDishes : IRepository<WasteDish>
    {
        List<WasteDish> _records = new List<WasteDish>();
        private readonly string _pathToJson;

        public WastePoolDishes()
        {
            _pathToJson = "Databases\\Jsons\\WastePoolDishes.json";
        }
        public WasteDish Add(WasteDish entity)
        {
            ReadJsonFile();

            _records.Add(entity);

            WriteJsonFile();

            return entity;
        }

        public WasteDish Get(string name)
        {
            ReadJsonFile();

            var record = _records.Find(item => item.Dish.Name == name);

            return record;
        }

        public List<WasteDish> GetAll()
        {
            ReadJsonFile();
            return _records;
        }

        public WasteDish Update(WasteDish entity)
        {
            var record = Get(entity.Dish.Name);

            record.Quantity = entity.Quantity;

            WriteJsonFile();

            return record;
        }
        public WasteDish Delete(string name)
        {
            ReadJsonFile();

            var record = Get(name);

            _records.Remove(record);

            WriteJsonFile();

            return record;
        }


        private void WriteJsonFile()
        {
            var newJson = JsonSerializer.Serialize(_records);
            File.WriteAllText(_pathToJson, newJson);
        }

        private void ReadJsonFile()
        {
            if (!File.Exists(_pathToJson))
            {
                throw new Exception("Error! Do not esseess to \"WastePoolIngredients.json\".");
            }
            var json = File.ReadAllText(_pathToJson);
            _records = JsonSerializer.Deserialize<List<WasteDish>>(json);
        }
    }
}
