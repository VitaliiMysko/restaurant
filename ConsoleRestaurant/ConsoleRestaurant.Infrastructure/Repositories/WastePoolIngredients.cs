﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace ConsoleRestaurant.Infrastructure.Repositories
{
    class WastePoolIngredients : IRepository<WasteIngredient>
    {
        List<WasteIngredient> _records = new List<WasteIngredient>();
        private readonly string _pathToJson;

        public WastePoolIngredients()
        {
            _pathToJson = "Databases\\Jsons\\WastePoolIngredients.json";
        }
        public WasteIngredient Add(WasteIngredient entity)
        {
            ReadJsonFile();

            _records.Add(entity);

            WriteJsonFile();

            return entity;
        }

        public WasteIngredient Delete(string name)
        {

            ReadJsonFile();

            var record = Get(name);

            _records.Remove(record);

            WriteJsonFile();

            return record;
        }

        public WasteIngredient Get(string name)
        {

            ReadJsonFile();

            var record = _records.Find(item => item.Ingredient.Name == name);

            //if (record == null)
            //{
            //    throw new Exception($"Error! Ingredient \"{name}\" not found ");
            //}

            return record;
        }

        public List<WasteIngredient> GetAll()
        {

            ReadJsonFile();
            return _records;
        }

        public WasteIngredient Update(WasteIngredient entity)
        {

            var record = Get(entity.Ingredient.Name);

            record.Quantity = entity.Quantity;

            WriteJsonFile();

            return record;
        }

        private void WriteJsonFile()
        {
            var newJson = JsonSerializer.Serialize(_records);
            File.WriteAllText(_pathToJson, newJson);
        }

        private void ReadJsonFile()
        {
            if (!File.Exists(_pathToJson))
            {
                throw new Exception("Error! Do not esseess to \"WastePoolIngredients.json\".");
            }
            var json = File.ReadAllText(_pathToJson);
            _records = JsonSerializer.Deserialize<List<WasteIngredient>>(json);
        }
    }
}
