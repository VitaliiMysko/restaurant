﻿using ConsoleRestaurant.Domain.Interfaces;
using System;

namespace ConsoleRestaurant.Application
{
    public class ConsoleReaderWithoutHelper : IConsoleReader
    {
        public ConsoleReaderWithoutHelper()
        {
            Console.WriteLine("budget, <sing>, <amount>");
            Console.WriteLine("buy, <customer>, <dish>");
            Console.WriteLine("buy, <customer>, Recommend, <ingredient1>, <ingredient2>");
            Console.WriteLine("order, <ingredient1>, <quantity1>,<dish2>, <quantity2>,...");
            Console.WriteLine("table, <customer1>, <customer2>,...,<dish1>,<dish2>,...");
            Console.WriteLine("table, <customer1>, <customer2>,...,Recommend, <ingredient1>, <ingredient2>, <dish2>,...");
            Console.WriteLine("table, Pooled, <customer1>, <customer2>,...,Recommend, <ingredient1>, <ingredient2>, <dish2>,...");
            Console.WriteLine("Audit");
            Console.WriteLine("Cleanoff");
            Console.WriteLine("Chart");
            Console.WriteLine("Entered data have to look like shown above");
            Console.WriteLine();
        }

        public string[] GetData()
        {
            string actionFull = Console.ReadLine().Trim();

            if (string.IsNullOrEmpty(actionFull))
            {
                return null;
            }

            return Helper.ParseActionLine(actionFull);
        }
    }
}
