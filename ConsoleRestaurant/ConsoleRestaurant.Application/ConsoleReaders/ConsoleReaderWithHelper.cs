﻿using ConsoleRestaurant.Domain.Interfaces;
using ConsoleRestaurant.Domain.SetsApp;
using System;
using System.Linq;

namespace ConsoleRestaurant.Application
{
    class ConsoleReaderWithHelper : IConsoleReader
    {
        public ConsoleReaderWithHelper()
        {
            Console.WriteLine("Select one of follow action: Buy, Budget, Order, Table");
        }

        public string[] GetData()
        {
            string action = Console.ReadLine().Trim();

            if (string.IsNullOrEmpty(action))
            {
                return null;
            }

            action = Helper.FirstCharToUpper(action);

            switch (action)
            {
                case nameof(Command.Buy):

                    Console.WriteLine("Enter customer:");
                    string customer = Console.ReadLine().Trim();

                    Console.WriteLine("Enter dish:");
                    string dish = Console.ReadLine().Trim();

                    string[] enteredDatasBuy = { action, customer, dish };

                    return enteredDatasBuy;

                case nameof(Command.Budget):

                    Console.WriteLine("Enter action (=,-,+):");
                    string sign = Console.ReadLine().Trim();

                    Console.WriteLine("Amount:");
                    string amount = Console.ReadLine().Trim();

                    string[] enteredDatasBudget = { action, sign, amount };

                    return enteredDatasBudget;

                case nameof(Command.Order):

                    Console.WriteLine("Enter ingredient:");
                    string ingredient = Console.ReadLine().Trim();

                    Console.WriteLine("How many:");
                    string quantity = Console.ReadLine().Trim();

                    string[] enteredDatasOrder = { action, ingredient, quantity };

                    return enteredDatasOrder;

                case nameof(Command.Table):

                    Console.WriteLine("Enter customers that separated of comma:");
                    string customers = Console.ReadLine().Trim();
                    var customersArr = customers.Split(",");

                    for (int i = 0; i < customersArr.Length; i++)
                    {
                        customersArr[i] = customersArr[i].Trim();
                    }

                    Console.WriteLine("Enter dishes that separated of comma:");
                    string dishes = Console.ReadLine().Trim();
                    var dishesArr = dishes.Split(",");

                    for (int i = 0; i < dishesArr.Length; i++)
                    {
                        dishesArr[i] = dishesArr[i].Trim();
                    }

                    string[] enteredDatasTable = { action };
                    enteredDatasTable = enteredDatasTable.Concat(customersArr).ToArray();
                    enteredDatasTable = enteredDatasTable.Concat(dishesArr).ToArray();

                    return enteredDatasTable;

                case nameof(Command.Audit):

                    string[] enteredDatasAudit = { action, "Resources" };

                    return enteredDatasAudit;

                default:

                    return null;
            }

        }
    }
}
