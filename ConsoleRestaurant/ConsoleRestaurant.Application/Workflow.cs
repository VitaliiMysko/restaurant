﻿using ConsoleRestaurant.Domain.Entities;
using ConsoleRestaurant.Domain.Interfaces;
using ConsoleRestaurant.Domain.SetsApp;
using ConsoleRestaurant.Infrastructure.Commands;
using ConsoleRestaurant.Infrastructure.Dataes;
using ConsoleRestaurant.Infrastructure.Outputs;
using ConsoleRestaurant.Infrastructure.Repositories;
using System;

namespace ConsoleRestaurant.Application
{
    public class Workflow
    {
        private readonly IConsoleReader _consoleReader;
        private readonly IOutput _outputApp;
        private readonly Audit _audit;
        private readonly Chart _chart;
        private readonly Profit _profit;

        public Workflow()
        {
            var manager = ManagerRepository.GetInstance();

            AppConfig.Settings = manager.GetAppsetting();

            _outputApp = new OutputApp();

            Statistic.AuditStatistic = new AuditStatistic(manager);
            _audit = new Audit(manager);
            _audit.Init();

            _chart = Chart.GetInstance();
            _chart.Init();

            _profit = new Profit();
            _profit.FixedInitialBudget(manager);

            Console.WriteLine("Do you want use helper (step by step) during working with app? (Yes - Enter any symbol)");
            _consoleReader = string.IsNullOrEmpty(Console.ReadLine().Trim()) ? new ConsoleReaderWithoutHelper() : new ConsoleReaderWithHelper();
        }

        public void Run()
        {
            var dataArr = _consoleReader.GetData();

            if (dataArr == null)
            {
                RunDailyTax();
                ShowChart();
                return;
            }

            try
            {
                RunCommand(dataArr);
            }
            catch (Exception e)
            {
                ToDisplay(e.Message);

                if (e.Message == "Restaurant Bankrupt!")
                {
                    return;
                }
            }

            ToDisplay("");

            Run();
        }

        private void ShowChart()
        {
            if (AppConfig.Settings.Chart.ChartShowBeforeClosingApp)
            {
                string[] dataArr = { nameof(Command.Chart) };
                RunCommand(dataArr);
            }
        }

        private void RunDailyTax()
        {
            _outputApp.Print("Daily tax:");
            RunProfitDailyTax();
            RunEnvironmentFineDailyTax();
        }

        private void RunProfitDailyTax()
        {
            _profit.FixedEndBudgetWithoutTip(ManagerRepository.GetInstance());
            var profit = _profit.GetProfit();

            string[] dataArr = { nameof(Command.Budget), "Profit", profit.ToString() };

            RunCommand(dataArr);
        }

        private void RunEnvironmentFineDailyTax()
        {
            var trash = new Waste(ManagerRepository.GetInstance());
            var totalTrashAmount = trash.GetTotalWasteAmount();

            string[] dataArr = { nameof(Command.Budget), "Environmental fine", totalTrashAmount.ToString() };

            RunCommand(dataArr);
        }

        private void RunCommand(string[] dataArr)
        {
            var commandBuilder = new CommandBuilder(dataArr);
            var command = commandBuilder.Build();
            Report report = command.Execute();

            ShowResult(report);
            AddRecordToAudit(report);
            AddRecordToChart(report);
        }

        private void AddRecordToAudit(Report report)
        {
            _audit.SaveInfo(report);
        }

        private void AddRecordToChart(Report report)
        {
            if (report.Command == Command.Buy || report.Command == Command.Order || report.Command == Command.Table)
            {
                _chart.MadeScreenshotWarehouse();
            }
        }

        private void ShowResult(Report report)
        {
            ToDisplay(report.Result);

            //foreach (var extraReport in report.ExtraReports)
            //{
            //    ShowResult(extraReport);
            //}
        }

        private void ToDisplay(string message)
        {
            _outputApp.Print(message);
        }
    }
}
