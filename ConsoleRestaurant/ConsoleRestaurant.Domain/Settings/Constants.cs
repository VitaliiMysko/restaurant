﻿
namespace ConsoleRestaurant.Domain.Settings
{
    public static class Constants
    {
        public const int NumberOfSuccessfulTransactionsForGettingDiscount = 3;
        public const int ExrtaWasteTaxOverEnvironmentalLimit = 20;
        public const int EnvironmentalLimit = 100;
    }

}
