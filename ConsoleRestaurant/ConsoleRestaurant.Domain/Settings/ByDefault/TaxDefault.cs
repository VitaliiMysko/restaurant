﻿
namespace ConsoleRestaurant.Domain.Settings
{
    public static class TaxDefault
    {
        public static int DailyTax { get; } = 20;
        public static int ProfitMargin { get; } = 30;
        public static int TransactionTax { get; } = 10;
        public static int TipsTax { get; } = 10;
        public static int WasteTax { get; } = 15;
    }
}
