﻿
namespace ConsoleRestaurant.Domain.Settings
{
    public static class PathDefault
    {
        public static string PathToAuditFile { get; } = "History.txt";
        public static string PathToLinePlotChartFile { get; } = "linePlotChart.png";
        public static string PathToBarGraphChartFile { get; } = "barGraphChart.png";
    }
}
