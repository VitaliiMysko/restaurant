﻿namespace ConsoleRestaurant.Domain.Settings
{
    public static class TipDefault
    {
        public static int MaxTipValue { get; } = 10;
    }
}
