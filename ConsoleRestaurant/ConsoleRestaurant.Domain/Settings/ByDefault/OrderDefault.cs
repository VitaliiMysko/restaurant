﻿using ConsoleRestaurant.Domain.SetsApp;

namespace ConsoleRestaurant.Domain.Settings
{
    public static class OrderDefault
    {
        public static OrderState State{ get; } = OrderState.Ingredients;
    }
}
