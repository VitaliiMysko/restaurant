﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRestaurant.Domain.Settings
{
    public static class TimeCostDefault
    {
        public static int BaseTime { get; } = 20;
        public static int BudgetCost { get; } = 250;
        public static int AuditCost { get; } = 100;
    }
}
