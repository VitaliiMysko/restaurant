﻿
namespace ConsoleRestaurant.Domain.SetsApp
{
    public enum OrderState
    {
        No,
        Ingredients,
        Dishes,
        All
    }
}
