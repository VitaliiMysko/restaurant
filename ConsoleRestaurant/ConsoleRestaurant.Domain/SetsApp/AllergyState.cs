﻿
namespace ConsoleRestaurant.Domain.SetsApp
{
    public enum AllergyState
    {
        Waste,
        Keep,
        Limit
    }

}