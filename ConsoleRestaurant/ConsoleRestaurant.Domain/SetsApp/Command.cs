﻿
namespace ConsoleRestaurant.Domain.SetsApp
{
    public enum Command
    {
        Audit,
        Budget,
        Buy,
        Chart,
        Order,
        Table,
        Cleanoff
    }
}
