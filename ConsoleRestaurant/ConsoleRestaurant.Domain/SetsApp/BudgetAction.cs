﻿
namespace ConsoleRestaurant.Domain.SetsApp
{
    public enum BudgetAction
    {
        Equals,
        Minus,
        Plus,
        //daily tax
        Profit,
        //daily tax
        EnvironmentalFine,
        Tip,
        Unknown
    }

}
