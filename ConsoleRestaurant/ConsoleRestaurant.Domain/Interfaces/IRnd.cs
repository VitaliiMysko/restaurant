﻿namespace ConsoleRestaurant.Domain.Interfaces
{
    public interface IRnd
    {
        int Next();
        int Next(int min, int max);
        double NextDouble();
    }
}
