﻿using System.Collections.Generic;

namespace ConsoleRestaurant.Domain.Interfaces
{
    public interface IRepository<T> where T : class 
    {
        public List<T> GetAll();
        public T Get(string name);
        public T Add(T entity);
        public T Update(T entity);
        public T Delete(string name);
    }
}
