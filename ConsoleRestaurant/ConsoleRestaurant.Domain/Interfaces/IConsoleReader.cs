﻿
namespace ConsoleRestaurant.Domain.Interfaces
{
    public interface IConsoleReader
    {
        public string[] GetData();
    }
}
