﻿using ConsoleRestaurant.Domain.Entities;

namespace ConsoleRestaurant.Domain.Interfaces
{
    public interface IOrder
    {
        public OrderReport Check(OrderReport report);
        public void AddToWarehouse();
    }
}
