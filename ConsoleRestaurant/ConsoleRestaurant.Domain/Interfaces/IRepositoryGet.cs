﻿
namespace ConsoleRestaurant.Domain.Interfaces
{
    public interface IRepositoryGet<T> where T : class
    {
        public T Get();
    }
}
