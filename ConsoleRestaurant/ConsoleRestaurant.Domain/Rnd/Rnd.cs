﻿using ConsoleRestaurant.Domain.Interfaces;
using System;

namespace ConsoleRestaurant.Domain.Rnd
{
    public class Rnd : IRnd
    {
        public readonly Random Random = new Random();
        
        public int Next()
        {
            return Random.Next();
        }

        public int Next(int min, int max)
        {
            return Random.Next(min, max);
        }

        public double NextDouble()
        {
            return Random.NextDouble();
        }
    }
}
