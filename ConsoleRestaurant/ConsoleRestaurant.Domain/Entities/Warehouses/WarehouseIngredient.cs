﻿
namespace ConsoleRestaurant.Domain.Entities
{
    public class WarehouseIngredient
    {
        public Ingredient Ingredient { get; set; }
        public int Quantity { get; set; }
    }
}
