﻿
namespace ConsoleRestaurant.Domain.Entities
{
    public class DataToWarehouse
    {
        public Dish Dish { get; set; } = new Dish();
        public Ingredient Ingredient { get; set; } = new Ingredient();
        public int QuantityInWarehouse { get; set; }
        public int QuantityThatNeed { get; set; }

        public bool IsError
        {
            get => QuantityInWarehouse < QuantityThatNeed;
        }
    }
}
