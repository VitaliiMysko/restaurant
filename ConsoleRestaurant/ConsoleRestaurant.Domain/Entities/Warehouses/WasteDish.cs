﻿
namespace ConsoleRestaurant.Domain.Entities
{
    public class WasteDish
    {
        public Dish Dish { get; set; }
        public int Quantity { get; set; }
    }
}
