﻿
namespace ConsoleRestaurant.Domain.Entities
{
    public class WarehouseDish
    {
        public Dish Dish { get; set; }
        public int Quantity { get; set; }
    }
}
