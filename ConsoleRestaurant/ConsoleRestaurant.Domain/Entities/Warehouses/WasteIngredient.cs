﻿
namespace ConsoleRestaurant.Domain.Entities
{
    public class WasteIngredient
    {
        public Ingredient Ingredient { get; set; }
        public int Quantity { get; set; }
    }
}
