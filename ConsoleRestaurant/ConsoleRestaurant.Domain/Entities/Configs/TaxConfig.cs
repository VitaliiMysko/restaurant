﻿using ConsoleRestaurant.Domain.Settings;
using System;

namespace ConsoleRestaurant.Domain.Entities
{
    public class TaxConfig
    {
        private int _dailyTax;
        private int _profitMargin;
        private int _transactionTax;
        private int _tipsTax;
        private int _wasteTax;

        public int DailyTax
        {
            get
            {
                return _dailyTax != 0 ? _dailyTax : TaxDefault.DailyTax;
            }
            set => _dailyTax = value;
        }
        public int ProfitMargin
        {
            get
            {
                return _profitMargin != 0 ? _profitMargin : TaxDefault.ProfitMargin;
            }
            set => _profitMargin = value;
        }
        public int TransactionTax
        {
            get
            {
                return _transactionTax != 0 ? _transactionTax : TaxDefault.TransactionTax;
            }
            set => _transactionTax = value;
        }
        public int TipsTax
        {
            get
            {
                return _tipsTax != 0 ? _tipsTax : TaxDefault.TipsTax;
            }
            set => _tipsTax = value;
        }
        public int WasteTax
        {
            get
            {
                return _wasteTax != 0 ? _wasteTax : TaxDefault.WasteTax;
            }
            set => _wasteTax = value;
        }

        public double GetDailyTaxAmount(double amount)
        {
            if (amount <= 0)
            {
                return 0;
            }

            return Math.Round(amount * DailyTax / 100, 2);
        }

        public double GetProfitMarginTaxAmount(double amount)
        {
            return Math.Round(amount * ProfitMargin / 100, 2);
        }

        public double GetTransactionTaxAmount(double amount)
        {
            return Math.Round(amount * TransactionTax / 100, 2);
        }

        public double GetTipsTaxAmount(double amount)
        {
            return Math.Round(amount * TipsTax / 100, 2);
        }

        public double GetWasteTaxAmount(double amount)
        {
            return Math.Round(amount * WasteTax / 100, 2);
        }
    }
}
