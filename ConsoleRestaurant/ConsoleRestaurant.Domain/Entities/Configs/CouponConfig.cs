﻿using ConsoleRestaurant.Domain.Settings;
using System;

namespace ConsoleRestaurant.Domain.Entities
{
    public class CouponConfig
    {
        private int _everyThirdDiscount;

        public int EveryThirdDiscount
        {
            get
            {
                return _everyThirdDiscount != 0 ? _everyThirdDiscount : CouponDefault.EveryThirdDiscount;
            }
            set => _everyThirdDiscount = value;
        }

        public double GetDiscountAmount(double amount)
        {
            return Math.Round(amount * EveryThirdDiscount / 100, 2);
        }

    }
}
