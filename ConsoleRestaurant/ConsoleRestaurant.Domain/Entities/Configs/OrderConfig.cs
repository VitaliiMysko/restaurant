﻿using ConsoleRestaurant.Domain.SetsApp;
using ConsoleRestaurant.Domain.Settings;

namespace ConsoleRestaurant.Domain.Entities
{
    public class OrderConfig
    {
        private string _allow;

        public OrderState State { get; private set; }

        public int IngredientVolatility { get; set; }
        public int DishVolatility { get; set; }
        //allow to order
        public string Allow
        {
            get
            {
                return _allow;
            }
            set
            {
                _allow = value;

                if (_allow == nameof(OrderState.No))
                {
                    this.State = OrderState.No;
                }
                else if (_allow == nameof(OrderState.Ingredients))
                {
                    this.State = OrderState.Ingredients;
                }
                else if (_allow == nameof(OrderState.Dishes))
                {
                    this.State = OrderState.Dishes;
                }
                else if (_allow == nameof(OrderState.All))
                {
                    this.State = OrderState.All;
                }
                else
                {
                    this.State = OrderDefault.State;
                }

            }
        }
        
    }
}