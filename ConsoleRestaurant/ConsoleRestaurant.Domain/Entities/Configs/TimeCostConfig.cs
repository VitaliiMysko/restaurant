﻿using ConsoleRestaurant.Domain.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRestaurant.Domain.Entities
{
    public class TimeCostConfig
    {
        private int _baseTime;
        private int _budgetCost;
        private int _auditCost;

        public int BaseTime
        {
            get
            {
                return _baseTime != 0 ? _baseTime : TimeCostDefault.BaseTime;
            }
            set => _baseTime = value;
        }

        public int BudgetCost
        {
            get
            {
                return _budgetCost != 0 ? _budgetCost : TimeCostDefault.BudgetCost;
            }
            set => _budgetCost = value;
        }

        public int AuditCost
        {
            get
            {
                return _auditCost != 0 ? _auditCost : TimeCostDefault.AuditCost;
            }
            set => _auditCost = value;
        }
    }
}
