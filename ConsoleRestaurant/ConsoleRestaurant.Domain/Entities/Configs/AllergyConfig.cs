﻿using ConsoleRestaurant.Domain.SetsApp;

namespace ConsoleRestaurant.Domain.Entities
{
    public class AllergyConfig
    {
        private string _dishesWithAllergies;
        public int Limit { get; private set; } = 0;
        public AllergyState State { get; private set; }

        public string DishesWithAllergies
        {
            get
            {
                return _dishesWithAllergies;
            }
            set
            {
                _dishesWithAllergies = value;

                if (int.TryParse(_dishesWithAllergies, out int result))
                {
                    this.State = AllergyState.Limit;
                    Limit = result;
                }
                else if (_dishesWithAllergies == nameof(AllergyState.Keep))
                {
                    this.State = AllergyState.Keep;
                }
                else
                {
                    this.State = AllergyState.Waste;
                }
            }
        }

    }
}
