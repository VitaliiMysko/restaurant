﻿using System;
using ConsoleRestaurant.Domain.Interfaces;

namespace ConsoleRestaurant.Domain.Entities
{
    public class TipConfig
    {
        private int _maxTipValue;
        public IRnd rnd = new Rnd.Rnd();
        public int MaxTipValue
        {
            //get { return _maxTipValue != 0 ? _maxTipValue : TipDefault.MaxTipValue; }
            get { return _maxTipValue; }

            set { _maxTipValue = value; }
        }

        public bool IsTipped()
        {
            return rnd.Next()%2 == 0;
        }

        public double GetTipAmount(double priceOfDish)
        {
            return Math.Round(priceOfDish * GetTipPercent(), 2);
        }

        public double GetTipAmount(double priceOfDish, double budgetOfCustomer)
        {
            var tip = GetTipAmount(priceOfDish);

            if (budgetOfCustomer < priceOfDish + tip)
            {
                tip = Math.Round(budgetOfCustomer - priceOfDish, 2);
            }

            return tip;
        }

        private double GetTipPercent()
        {
            return rnd.NextDouble() * (double)MaxTipValue / 100;
        }
    }
}
