﻿using ConsoleRestaurant.Domain.Settings;

namespace ConsoleRestaurant.Domain.Entities
{
    public class ChartConfig
    {
        private string _pathToLinePlotChartFile;
        private string _pathToBarGraphChartFile;

        public string PathToLinePlotChartFile
        {
            get
            {
                return string.IsNullOrEmpty(_pathToLinePlotChartFile) ? PathDefault.PathToLinePlotChartFile : _pathToLinePlotChartFile;
            }
            set
            {
                _pathToLinePlotChartFile = value;
            }
        }

        public string PathToBarGraphChartFile
        {
            get
            {
                return string.IsNullOrEmpty(_pathToBarGraphChartFile) ? PathDefault.PathToBarGraphChartFile : _pathToBarGraphChartFile;
            }
            set
            {
                _pathToBarGraphChartFile = value;
            }
        }

        public int ChartVisibility { get; set; }
        public bool BarGraphChartEnable { get; set; }
        public bool LinePlotChartEnable { get; set; }
        public bool ChartShowBeforeClosingApp { get; set; }
    }
}
