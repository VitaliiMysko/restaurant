﻿
namespace ConsoleRestaurant.Domain.Entities
{
    public class WarehouseConfig
    {
        public int TotalMaximum { get; set; }
        public int MaxIngredientType { get; set; }
        public int MaxDishType { get; set; }
        public int MaxTrash { get; set; }
        public double SpoilRate { get; set; }
        public int IngredientVolatility { get; set; }
        public int DishtVolatility { get; set; }
    }
}
