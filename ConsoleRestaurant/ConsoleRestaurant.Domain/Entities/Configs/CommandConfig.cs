﻿using System;

namespace ConsoleRestaurant.Domain.Entities
{
    public class CommandConfig
    {
        public bool Buy { get; set; }
        public bool Budget { get; set; }
        public bool Cleanoff { get; set; }
        public bool Order { get; set; }
        public bool Table { get; set; }
        public bool Audit { get; set; }
        public bool Chart { get; set; }

        public bool HasCommandEnable(string nameCommand)
        {
            try
            {
                return (bool)this.GetType().GetProperty(nameCommand).GetValue(this, null);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
