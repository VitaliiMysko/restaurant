﻿
using ConsoleRestaurant.Domain.Settings;

namespace ConsoleRestaurant.Domain.Entities
{
    public class Appsetting
    {
        private string _pathToAuditFile;

        public string PathToAuditFile
        {
            get
            {
                return string.IsNullOrEmpty(_pathToAuditFile) ? PathDefault.PathToAuditFile : _pathToAuditFile;
            }
            set
            {
                _pathToAuditFile = value;
            }
        }

        public string MainRestaurantName { get; set; }
        public AllergyConfig Allergy { get; set; }
        public CommandConfig Command { get; set; }
        public CouponConfig Coupon { get; set; }
        public OrderConfig Order { get; set; }
        public TaxConfig Tax { get; set; }
        public WarehouseConfig Warehouse { get; set; }
        public TipConfig Tip { get; set; }
        public ChartConfig Chart { get; set; }
        public TimeCostConfig TimeCost { get; set; }
    }
}
