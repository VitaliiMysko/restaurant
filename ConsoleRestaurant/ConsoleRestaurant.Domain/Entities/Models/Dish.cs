﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleRestaurant.Domain.Entities
{
    public class Dish
    {
        public string Name { get; set; }
        public IList<Dish> Dishes { get; set; }
        public IList<Ingredient> Ingredients { get; set; }

        public double GetPrice()
        {
            double price = 0;

            if (Dishes != null)
            {
                foreach (var item in Dishes)
                {
                    price += item.GetPrice();
                }
            }

            if (Ingredients != null)
            {
                foreach (var item in Ingredients)
                {
                    price += item.Price;
                }
            }

            return price;
        }
        public double GetTotalPrice()
        {
            var amount = GetPrice();
            double taxAmount = GetProfitMarginTaxAmount(amount);
            return Math.Round(amount + taxAmount, 2);
        }
        private double GetProfitMarginTaxAmount(double amount)
        {
            return AppConfig.Settings.Tax.GetProfitMarginTaxAmount(amount);
        }
        public bool Contains(string name)
        {
            if (Ingredients != null)
            {
                if (Ingredients.Any(i => i.Name == name))
                {
                    return true;
                }
            }
            if (Dishes != null)
            {
                return Dishes.Any(d => d.Contains(name));
            }
            return false;
        }
    }
}
