﻿
namespace ConsoleRestaurant.Domain.Entities
{
    public class Ingredient
    {
        public string Name { get; set; }
        public double Price { get; set; }
    }
}
