﻿using System;
using System.Collections.Generic;

namespace ConsoleRestaurant.Domain.Entities
{
    public class Customer
    {
        public string FullName { get; set; }
        public IList<Ingredient> Allergies { get; set; }
        public double Budget { get; set; }

        public void ReduceBudget(double correctAmount)
        {
            Budget = (Budget >= correctAmount) ? Math.Round(Budget - correctAmount, 2) : Budget;
        }
    }
}
