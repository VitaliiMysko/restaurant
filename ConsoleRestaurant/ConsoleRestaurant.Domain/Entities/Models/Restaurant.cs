﻿
namespace ConsoleRestaurant.Domain.Entities
{
    public class Restaurant
    {
        public string Name { get; set; }
        public double Budget { get; set; }
        //Tip include tip tax
        public double Tip { get; set; }

        public bool IsBankrupt
        {
            get => Budget <= 0;
        }

    }
}
