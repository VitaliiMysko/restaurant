﻿
namespace ConsoleRestaurant.Domain.Entities
{
    public class ActivityOfBuying
    {
        public Customer Customer { get; set; }
        public int Times { get; set; }
    }
}
