﻿using ConsoleRestaurant.Domain.SetsApp;

namespace ConsoleRestaurant.Domain.Entities
{
    public class BuyReport : Report
    {
        public bool IsAllergy { get; set; }
        public bool IsRecommend { get; set; }
        public AllergyBaseState AllergyBaseState { get; set; }
        public bool AreMoney { get; set; }
        public double PriceOfDish { get; set; }
        public double BudgetOfCustomer { get; set; }
        public double TransactionTaxAmount { get; set; }
        public double Discount { get; set; }
        public double Tip { get; set; }
        public Customer Customer;
        public Dish Dish;
        //public double TimeCostMax { get; set; }
        //public double TimeCostMin { get; set; }

    }
}
