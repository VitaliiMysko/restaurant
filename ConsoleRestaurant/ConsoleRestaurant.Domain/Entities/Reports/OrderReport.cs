﻿
namespace ConsoleRestaurant.Domain.Entities
{
    public class OrderReport : Report
    {
        public double Amount { get; set; }
        public bool Multiple { get; set; }
    }
}
