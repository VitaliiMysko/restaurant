﻿using ConsoleRestaurant.Domain.SetsApp;
using System;
using System.Collections.Generic;

namespace ConsoleRestaurant.Domain.Entities
{
    public class Report
    {
        private string _message;

        public Command Command { get; set; }
        public bool CommandSucceeded { get; set; }
        public string CommandEntered { get; set; }

        public bool IsBankruptcy { get; set; }
        public bool IsPoisoning { get; set; }
        public List<Report> ExtraReports { get; set; } = new List<Report>();

        public double TimeCost { get; set; }

        public string Message { get => _message; }

        public void AddMessage(string message)
        {
            message = message.Trim();

            if (!string.IsNullOrEmpty(message))
            {
                _message += string.IsNullOrEmpty(_message) ? message : "\n" + message;
            }
        }

        public void ClearMessage(string message)
        {
            _message = "";
        }

        public string Result
        {
            get
            {
                var isBankruptcyMessage = IsBankruptcy ? " (Restaurant is bankrupt!)" : "";
                var isPoisonMessage = IsPoisoning ? " (kitchen is poisoning!)" : "";
                if (!String.IsNullOrEmpty(Message))
                {
                    if (IsBankruptcy || IsPoisoning)
                    {
                        return CommandEntered + " => fail" + isBankruptcyMessage + isPoisonMessage;
                    }

                    return CommandEntered + " => " + Message + isBankruptcyMessage + isPoisonMessage;
                }

                if (CommandSucceeded)
                {
                    return CommandEntered + " => success" + isBankruptcyMessage + isPoisonMessage;
                }
                else
                {
                    return CommandEntered + " => fail" + isBankruptcyMessage + isPoisonMessage;
                }

            }
        }

        public void Update(Report extraReport)
        {
            this.ExtraReports.Add(extraReport);
            this.CommandSucceeded = extraReport.CommandSucceeded;
            this.IsBankruptcy = extraReport.IsBankruptcy;
        }

    }
}
