﻿
namespace ConsoleRestaurant.Domain.Entities
{
    public class BudgetReport : Report
    {
        public double Budget { get; set; }
        public double Tip { get; set; }
    }
}
